Official implementation of "Automated Palette Cycling Animations"

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

----

The project was built and tested with VS2022.
Most of the libraries were previously compiled on linux,
I assume one should be able to compile the project on other
platforms without much trouble.

----

To run the project provide a folder path containing the source frames to the executable

$(exec) src\random_0.25fps_650p

The config.json file in the samples directly is not mandatory but can help with controling
the execution. Please refer to `tower_25fps_320p/config.json` for sample inputs.

----

Please note that this project was written during the research and as a result one might have
difficaulties reading the code. I will try to answer questions posted as issues.
