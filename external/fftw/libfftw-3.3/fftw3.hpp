#pragma once
#include "api/fftw3.h"

template <typename precision>
struct fftw_functions {
};

template <> struct fftw_functions<float>
{
#define createAlias(X) constexpr static decltype(&fftwf_##X) X = fftwf_##X
	createAlias(free);
	createAlias(execute);
	createAlias(plan_r2r_3d);
	createAlias(init_threads);
	createAlias(plan_with_nthreads);
	createAlias(cleanup_threads);
	createAlias(malloc);
	createAlias(destroy_plan);
	typedef fftwf_plan plan;
#undef createAlias
};

template <> struct fftw_functions<double>
{
#define createAlias(X) constexpr static decltype(&fftw_##X) X = fftw_##X
	createAlias(free);
	createAlias(execute);
	createAlias(plan_r2r_3d);
	createAlias(init_threads);
	createAlias(plan_with_nthreads);
	createAlias(cleanup_threads);
	createAlias(malloc);
	createAlias(destroy_plan);
	typedef fftw_plan plan;
#undef createAlias
};
