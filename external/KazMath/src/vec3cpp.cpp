#include "Vec3.hpp"

Vec3::Vec3()
{
}

Vec3::Vec3(kmScalar x,kmScalar y,kmScalar z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vec3::Vec3(kmVec3 p) : kmVec3(p)
{

}

kmScalar Vec3::dot(const Vec3 &other) const
{
	return kmVec3Dot(this,&other);
}

void Vec3::normalize()
{
	kmVec3Normalize(this,this);
}

kmScalar Vec3::length() const
{
	return kmVec3Length(this);
}

kmScalar Vec3::lengthsq() const
{
	return kmVec3LengthSq(this);
}
Vec3 Vec3::cross(const Vec3 &other) const
{
	Vec3 res;
	kmVec3Cross(&res, this, &other);
	return res;
}

Vec3 & Vec3::operator+=(const Vec3 & v2)
{
	kmVec3Add(this, this, &v2);
	return *this;
}

Vec3 & Vec3::operator-=(const Vec3 & v2)
{
	kmVec3Subtract(this, this, &v2);
	return *this;
}

Vec3 & Vec3::operator*=(const kmScalar v2)
{
	kmVec3Scale(this, this, v2);
	return *this;
}

Vec3 & Vec3::operator/=(const kmScalar v2)
{
	kmVec3Scale(this, this, 1 / v2);
	return *this;
}

Vec3 Vec3::operator+(const Vec3 & v2) const
{
	Vec3 v;
	kmVec3Add(&v, this, &v2);
	return v;
}

Vec3 Vec3::operator-(const Vec3 & v2) const
{
	Vec3 v;
	kmVec3Subtract(&v, this, &v2);
	return v;
}

Vec3 Vec3::operator*(const kmScalar v2) const
{
	Vec3 v;
	kmVec3Scale(&v, this, v2);
	return v;
}

Vec3 Vec3::operator/(const kmScalar v2) const
{
	Vec3 v;
	kmVec3Scale(&v, this, 1 / v2);
	return v;
}


kmVec3 operator +(const kmVec3& v1, const kmVec3& v2)
{
	kmVec3 res;	
	kmVec3Add(&res,&v1,&v2);
	return res;
}

kmVec3 operator -(const kmVec3& v1, const kmVec3& v2)
{
	kmVec3 res;	
	kmVec3Subtract(&res,&v1,&v2);
	return res;
}

kmVec3 operator *(const kmVec3& v1, const kmScalar v2)
{
	kmVec3 res;
	kmVec3Scale(&res,&v1, v2);
	return res;
}

kmVec3 operator /(const kmVec3& v1, const kmScalar v2)
{
	kmVec3 res;
	kmVec3Scale(&res,&v1, 1/v2);
	return res;
}

kmVec3& operator +=(kmVec3& v1, const kmVec3& v2)
{
	kmVec3Add(&v1,&v1,&v2);
	return v1;
}

kmVec3& operator -=(kmVec3& v1, const kmVec3& v2)
{
	kmVec3Subtract(&v1,&v1,&v2);
	return v1;
}

kmVec3& operator *=(kmVec3& v1, const kmScalar v2)
{
	kmVec3Scale(&v1,&v1, v2);
	return v1;
}

kmVec3& operator /=(kmVec3& v1, const kmScalar v2)
{	
	kmVec3Scale(&v1,&v1, 1/v2);
	return v1;
}
