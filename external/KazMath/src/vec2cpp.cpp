#include "vec2.hpp"
#include <limits>

Vec2::Vec2()
{
}

Vec2::Vec2(kmScalar x,kmScalar y)
{
	this->x = x;
	this->y = y;
}

Vec2::Vec2(kmVec2 p) : kmVec2(p)
{

}

void Vec2::normalize()
{
	kmVec2Normalize(this,this);
}

kmScalar Vec2::length() const
{
	return kmVec2Length(this);
}

kmScalar Vec2::length2() const
{
	return kmVec2LengthSq(this);
}

kmScalar Vec2::dot(const Vec2 &other) const
{
	return kmVec2Dot(this, &other);
}

kmScalar Vec2::cross(const Vec2 &other) const
{
	return x * other.y - y * other.x;
}

Vec2 & Vec2::operator+=(const Vec2 & v2)
{
	kmVec2Add(this, this, &v2);
	return *this;
}

Vec2 & Vec2::operator-=(const Vec2 & v2)
{
	kmVec2Subtract(this, this, &v2);
	return *this;
}

Vec2 & Vec2::operator*=(const kmScalar v2)
{
	kmVec2Scale(this, this, v2);
	return *this;
}

Vec2 & Vec2::operator/=(const kmScalar v2)
{
	kmVec2Scale(this, this, 1/v2);
	return *this;
}

Vec2 Vec2::operator+(const Vec2 & v2) const
{
	Vec2 v;
	kmVec2Add(&v, this, &v2);
	return v;
}

Vec2 Vec2::operator-(const Vec2 & v2) const
{
	Vec2 v;
	kmVec2Subtract(&v, this, &v2);
	return v;
}

Vec2 Vec2::operator*(const kmScalar v2) const
{
	Vec2 v;
	kmVec2Scale(&v, this, v2);
	return v;
}

Vec2 Vec2::operator/(const kmScalar v2) const
{
	Vec2 v;
	kmVec2Scale(&v, this, 1/v2);
	return v;
}

kmVec2 operator +(const kmVec2& v1, const kmVec2& v2)
{
	kmVec2 res;	
	kmVec2Add(&res,&v1,&v2);
	return res;
}

kmVec2 operator -(const kmVec2& v1, const kmVec2& v2)
{
	kmVec2 res;	
	kmVec2Subtract(&res,&v1,&v2);
	return res;
}

kmVec2 operator *(const kmVec2& v1, const kmScalar v2)
{
	kmVec2 res;
	kmVec2Scale(&res,&v1, v2);
	return res;
}

kmVec2 operator /(const kmVec2& v1, const kmScalar v2)
{
	kmVec2 res;
	kmVec2Scale(&res,&v1, 1/v2);
	return res;
}

kmVec2& operator +=(kmVec2& v1, const kmVec2& v2)
{
	kmVec2Add(&v1,&v1,&v2);
	return v1;
}

kmVec2& operator -=(kmVec2& v1, const kmVec2& v2)
{
	kmVec2Subtract(&v1,&v1,&v2);
	return v1;
}

kmVec2& operator *=(kmVec2& v1, const kmScalar v2)
{
	kmVec2Scale(&v1,&v1, v2);
	return v1;
}

kmVec2& operator /=(kmVec2& v1, const kmScalar v2)
{	
	kmVec2Scale(&v1,&v1, 1/v2);
	return v1;
}


bool almost_equal(const Vec2& v1, const Vec2& v2)
{
	return kmAlmostEqual(v1.x, v2.x) && kmAlmostEqual(v1.y, v2.y);
}
