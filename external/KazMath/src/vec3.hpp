#include "Vec3.h"

struct Vec3 : public kmVec3
{
	Vec3();
	Vec3(kmScalar x,kmScalar y,kmScalar z);
	Vec3(kmVec3 p);
	void normalize();
	kmScalar length() const;
	kmScalar lengthsq() const;

	kmScalar dot(const Vec3 &other) const;
	Vec3 cross(const Vec3 &other) const;

	Vec3& operator +=(const Vec3& v2);
	Vec3& operator -=(const Vec3& v2);
	Vec3& operator *=(const kmScalar v2);
	Vec3& operator /=(const kmScalar v2);
	Vec3 operator +(const Vec3& v2) const;
	Vec3 operator -(const Vec3& v2) const;
	Vec3 operator *(const kmScalar v2) const;
	Vec3 operator /(const kmScalar v2) const;
};

kmVec3 operator +(const kmVec3& v1, const kmVec3& v2);
kmVec3 operator -(const kmVec3& v1, const kmVec3& v2);
kmVec3 operator *(const kmVec3& v1, const kmScalar v2);
kmVec3 operator /(const kmVec3& v1, const kmScalar v2);
kmVec3& operator -=(kmVec3& v1, const kmVec3& v2);
kmVec3& operator *=(kmVec3& v1, const kmScalar v2);
kmVec3& operator /=(kmVec3& v1, const kmScalar v2);
