#pragma once 

#include "vec2.h"
#include <ostream>

struct Vec2 : public kmVec2
{
	Vec2();
	Vec2(kmScalar x,kmScalar y);
	Vec2(kmVec2 p);
	void normalize();
	kmScalar length() const;
	kmScalar length2() const;

	kmScalar dot(const Vec2 &other) const;
	kmScalar cross(const Vec2 &other) const;

	Vec2& operator +=(const Vec2& v2);
	Vec2& operator -=(const Vec2& v2);
	Vec2& operator *=(const kmScalar v2);
	Vec2& operator /=(const kmScalar v2);
	Vec2 operator +(const Vec2& v2) const;
	Vec2 operator -(const Vec2& v2) const;
	Vec2 operator *(const kmScalar v2) const;
	Vec2 operator /(const kmScalar v2) const;
};

kmVec2 operator +(const kmVec2& v1, const kmVec2& v2);
kmVec2 operator -(const kmVec2& v1, const kmVec2& v2);
kmVec2 operator *(const kmVec2& v1, const kmScalar v2);
kmVec2 operator /(const kmVec2& v1, const kmScalar v2);
kmVec2& operator +=(kmVec2& v1, const kmVec2& v2);
kmVec2& operator -=(kmVec2& v1, const kmVec2& v2);
kmVec2& operator *=(kmVec2& v1, const kmScalar v2);
kmVec2& operator /=(kmVec2& v1, const kmScalar v2);
bool almost_equal(const Vec2& v1, const Vec2& v2);

inline std::ostream& operator <<(std::ostream &s, const Vec2& v) {
	s << "(" << v.x << ", " << v.y << ")";
	return s;
}