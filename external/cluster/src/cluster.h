/******************************************************************************/
/* The C Clustering Library.
 * Copyright (C) 2002 Michiel Jan Laurens de Hoon.
 *
 * This library was written at the Laboratory of DNA Information Analysis,
 * Human Genome Center, Institute of Medical Science, University of Tokyo,
 * 4-6-1 Shirokanedai, Minato-ku, Tokyo 108-8639, Japan.
 * Contact: michiel.dehoon 'AT' riken.jp
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation with or without modifications and for any purpose and
 * without fee is hereby granted, provided that any copyright notices
 * appear in all copies and that both those copyright notices and this
 * permission notice appear in supporting documentation, and that the
 * names of the contributors or copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific prior permission.
 *
 * THE CONTRIBUTORS AND COPYRIGHT HOLDERS OF THIS SOFTWARE DISCLAIM ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#ifndef CLUSTER_H_
#define CLUSTER_H_
#include <kazmath.h>

#define CLUSTERVERSION "1.59"

#ifdef __cplusplus
extern "C" {
#endif

	/* Chapter 2 */
	kmScalar clusterdistance(int nrows, int ncolumns, kmScalar** data, int** mask,
		kmScalar weight[], int n1, int n2, int index1[], int index2[], char dist,
		char method, int transpose);
	void distancematrix(int ngenes, int ndata, kmScalar** data, int** mask,
		kmScalar* weight, char dist, int transpose, kmScalar** distances);

	/* Chapter 3 */
	int getclustercentroids(int nclusters, int nrows, int ncolumns,
		kmScalar** data, int** mask, int clusterid[], kmScalar** cdata, int** cmask,
		int transpose, char method);
	void getclustermedoids(int nclusters, int nelements, kmScalar** distance,
		int clusterid[], int centroids[], kmScalar errors[]);
	void kcluster(int nclusters, int ngenes, int ndata, kmScalar** data,
		int** mask, kmScalar weight[], int transpose, int npass, char method, char dist,
		int clusterid[], kmScalar* error, int* ifound);
	void kmedoids(int nclusters, int nelements, kmScalar** distance,
		int npass, int clusterid[], kmScalar* error, int* ifound);

	/* Chapter 4 */
	typedef struct { int left; int right; kmScalar distance; } ClusterNode;
	/*
	 * A ClusterNode struct describes a single ClusterNode in a tree created by hierarchical
	 * clustering. The tree can be represented by an array of n ClusterNode structs,
	 * where n is the number of elements minus one. The integers left and right
	 * in each ClusterNode struct refer to the two elements or subClusterNodes that are joined
	 * in this ClusterNode. The original elements are numbered 0..nelements-1, and the
	 * ClusterNodes -1..-(nelements-1). For each ClusterNode, distance contains the distance
	 * between the two subClusterNodes that were joined.
	 */

	ClusterNode* treecluster(int nrows, int ncolumns, kmScalar** data, int** mask,
		kmScalar weight[], int transpose, char dist, char method, kmScalar** distmatrix);
	int sorttree(const int nClusterNodes, ClusterNode* tree, const kmScalar order[], int indices[]);
	int cuttree(int nelements, const ClusterNode* tree, int nclusters, int clusterid[]);

	/* Chapter 5 */
	void somcluster(int nrows, int ncolumns, kmScalar** data, int** mask,
		const kmScalar weight[], int transpose, int nxnodes, int nynodes,
		kmScalar inittau, int niter, char dist, kmScalar*** celldata,
		int clusterid[][2]);

	/* Chapter 6 */
	int pca(int m, int n, kmScalar** u, kmScalar** v, kmScalar* w);

	/* Utility routines, currently undocumented */
	void sort(int n, const kmScalar data[], int index[]);
	kmScalar mean(int n, kmScalar x[]);
	kmScalar median(int n, kmScalar x[]);

	kmScalar* calculate_weights(int nrows, int ncolumns, kmScalar** data, int** mask,
		kmScalar weights[], int transpose, char dist, kmScalar cutoff, kmScalar exponent);
#ifdef __cplusplus
}
#endif


#endif