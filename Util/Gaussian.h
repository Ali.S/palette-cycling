#pragma once
#include "ThreadUtil.h"
#include <kazmath.h>

enum class Dimention {
	X, Y
};

template <typename T>
inline void make_zero(T& v);

template <>
inline void make_zero(kmScalar& v)
{
	v = 0;
}

template <typename Container, typename T = Container::value_type>
Container runGausian(Dimention d, float stdDev, size_t width, size_t height, const Container& data)
{
	constexpr auto kernelCutoff = 0.01;
	auto pdf = [stdDev](float x) -> float {
		static float inv_sqrt_2pi = 0.3989422804014327;
		float a = (x) / stdDev;

		return std::exp(-0.5f * a * a) * inv_sqrt_2pi / stdDev;
	};
	kmScalar red = 1 - pdf(0);
	int kernelSize = 0;
	while (red > kernelCutoff)
		red -= pdf(++kernelSize) * 2;

	std::vector<kmScalar> kernel(kernelSize * 2 + 1);

	for (int i = -kernelSize; i <= kernelSize; i++)
		kernel[i + kernelSize] = pdf(i);

	std::vector<T> ret;
	ret.resize(data.size());
	size_t dx = d == Dimention::X ? 1 : 0;
	size_t dy = d == Dimention::Y ? 1 : 0;
	size_t dimMin = 0, dimMax = d == Dimention::X ? width : height;
	sequal_for_each(data.size(), [&](size_t task_id, size_t thread_id) {
		size_t j = task_id / width;
		size_t i = task_id % width;
		size_t& mover = d == Dimention::X ? i : j;
		int start = static_cast<int>(std::max<int>(mover - kernelSize, 0) - mover);
		int end = static_cast<int>(std::min<int>(mover + kernelSize + 1, dimMax) - mover);
		make_zero(ret[task_id]);
		mover += start;
		kmScalar kSum = 0;
		for (int diff = start; diff < end; diff++, mover++)
		{
			kSum += kernel[diff + kernelSize];
			ret[task_id] += data[i + j * width] * kernel[diff + kernelSize];
		}
		ret[task_id] /= kSum;
		}, nullptr);
	return ret;
}


template <typename Container, typename T = Container::value_type>
Container runBlock(Dimention d, float stdDev, size_t width, size_t height, const Container& data)
{
	constexpr auto kernelCutoff = 0.01;
	auto pdf = [stdDev](float x) -> float {
		return 1 / stdDev;
	};

	kmScalar red = 1 - pdf(0);
	int kernelSize = 0;
	while (red > kernelCutoff)
		red -= pdf(++kernelSize) * 2;

	std::vector<kmScalar> kernel(kernelSize * 2 + 1);

	for (int i = -kernelSize; i <= kernelSize; i++)
		kernel[i + kernelSize] = pdf(i);

	std::vector<T> ret;
	ret.resize(data.size());
	size_t dx = d == Dimention::X ? 1 : 0;
	size_t dy = d == Dimention::Y ? 1 : 0;
	size_t dimMin = 0, dimMax = d == Dimention::X ? width : height;
	sequal_for_each(data.size(), [&](size_t task_id, size_t thread_id) {
		size_t j = task_id / width;
		size_t i = task_id % width;
		size_t& mover = d == Dimention::X ? i : j;
		int start = static_cast<int>(std::max<int>(mover - kernelSize, 0) - mover);
		int end = static_cast<int>(std::min<int>(mover + kernelSize + 1, dimMax) - mover);
		make_zero(ret[task_id]);
		mover += start;
		kmScalar kSum = 0;
		for (int diff = start; diff < end; diff++, mover++)
		{
			kSum += kernel[diff + kernelSize];
			ret[task_id] += data[i + j * width] * kernel[diff + kernelSize];
		}
		ret[task_id] /= kSum;
		}, nullptr);
	return ret;
}
