#pragma once
#include <functional>
#include <vector>
#include <algorithm>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>

void setThreadName(const char* threadName);
// p: range 0..1
// p2:  independent of p range 0..1
void progressBar(int w, float p, float p2 = 0);

struct ThreadPool {
	ThreadPool(size_t threadCount);
	~ThreadPool();

	void runJob(size_t domain, std::function<void(size_t taskId, size_t threadId)> kernel, std::function<void(float)> report);

	size_t getPoolSize();

	static ThreadPool* instance();
	static void recreateIntance(size_t threadCount);
	static void recreateIntance();

private:
	void threadFunc(int tid);

	std::vector<std::thread> _threads;
	size_t _domain;
	std::atomic<size_t> _done;
	size_t _ready; //need separate counters for start and finish
	std::atomic<size_t> _started; //need separate counters for start and finish
	std::mutex _mutex;
	std::condition_variable _newTaskCondition;
	std::function<void(size_t taskId, size_t threadId)> _kernel;
	static ThreadPool* s_instance;

};

void sequal_for_each(size_t domain, std::function<void(size_t taskId, size_t threadId)> kernel, std::function<void(float)> report);
void sequal_for_each(size_t domain, std::function<void(size_t taskId, size_t threadId)> kernel);


template <typename T> inline int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}
