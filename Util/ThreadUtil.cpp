#include "Conf.h"
#include "ThreadUtil.h"
#include <cstdio>
#include <thread>
#include <atomic>
#include <vector>
#include <iostream>
#include <thread>
#include <chrono>
#include <string>


#define VC_EXTRALEAN
#define NOMINMAX
#include <windows.h>  
const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)  
typedef struct tagTHREADNAME_INFO
{
	DWORD dwType; // Must be 0x1000.  
	LPCSTR szName; // Pointer to name (in user addr space).  
	DWORD dwThreadID; // Thread ID (-1=caller thread).  
	DWORD dwFlags; // Reserved for future use, must be zero.  
} THREADNAME_INFO;
#pragma pack(pop)  

void setThreadName(const char * threadName) {
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = threadName;
	info.dwThreadID = GetCurrentThreadId();
	info.dwFlags = 0;
#pragma warning(push)  
#pragma warning(disable: 6320 6322)  
	__try {
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
	}
#pragma warning(pop)  
}

void progressBar(int w, float p, float p2)
{
	printf("\r[");

	for (int i = 1; i < w - 1; i++)
	{
		if (i == w / 2 - 1)
			i += printf("%.0f%%", p * 100 * (i += 2) / i) - 1;
		else
			printf("%c", i <= (p*w) ? '|' :  (i < (p2 * w)?'.':' '));
	}

	printf("]");
}


ThreadPool *ThreadPool::s_instance;

void sequal_for_each(size_t domain, std::function<void(size_t , size_t)> kernel, std::function<void(float)> report)
{
	ThreadPool::instance()->runJob(domain, kernel, report);
}

void sequal_for_each(size_t domain, std::function<void(size_t, size_t)> kernel)
{
	size_t lastPrint = 0;
	sequal_for_each(domain, kernel, [&lastPrint](float p)
	{
		size_t thisPrint = p * 80;
		if (thisPrint != lastPrint) {
			progressBar(80, p);
			lastPrint = thisPrint;
		}
	});
	printf("\n");
}

ThreadPool::ThreadPool(size_t threadCount)
{
	_ready = 0;
	_started = 0;
	_done = 0;
	_domain = 0;
	for (size_t tid = 0; tid < threadCount; tid++)
	{
		_threads.emplace_back([this,tid]() {
			threadFunc(tid);
		}); 
	}
	std::unique_lock<std::mutex> ul(_mutex);
	while (_ready != _threads.size())
	{
		ul.unlock();
		std::this_thread::yield();
		ul.lock();
	}
	ul.unlock();
}

ThreadPool::~ThreadPool()
{
	_started = 0;
	_done = 2;
	_domain = 1;
	_newTaskCondition.notify_all();
	for (auto &t : _threads)
		t.join();
}

void ThreadPool::runJob(size_t domain, std::function<void(size_t taskId, size_t threadId)> kernel, std::function<void(float)> report)
{
	if (report)
		report(0);
	std::unique_lock<std::mutex> ul(_mutex);
	while (_ready != _threads.size())
	{
		ul.unlock();
		std::this_thread::yield();
		ul.lock();
	}
	_ready = 0;
	ul.unlock();
	_domain = domain;
	_kernel = kernel;
	_started = 0;
	_done = 0;
	_newTaskCondition.notify_all();
	
	while (_done < _domain)
	{
		if (report)
			report(_done / static_cast<float>(domain));
		else
			std::this_thread::sleep_for(std::chrono::milliseconds(5));
	}

	if (report)
		report(1);

}

size_t ThreadPool::getPoolSize()
{
	return _threads.size();
}

ThreadPool * ThreadPool::instance()
{
	if (s_instance == nullptr)
	{
		auto threadCount = std::thread::hardware_concurrency();
		if (threadCount == 0)
		{
			printf("Could not detect system thread Count");
			threadCount = 1;
		}
		s_instance = new ThreadPool(threadCount);
	}
	return s_instance;
}

void ThreadPool::recreateIntance(size_t threadCount)
{
	if (s_instance)
		delete s_instance;
	s_instance = new ThreadPool(threadCount);
}

void ThreadPool::recreateIntance()
{
	auto threadCount = std::thread::hardware_concurrency();
	if (threadCount == 0)
	{
		printf("Could not detect system thread Count");
		threadCount = 1;
	}
	recreateIntance(threadCount);
}

void ThreadPool::threadFunc(int tid)
{
	setThreadName(("worker_" + std::to_string(tid)).c_str());
	
	while (true)
	{
		std::unique_lock<std::mutex> ul(_mutex); 
		_ready++;
		_newTaskCondition.wait(ul);
		ul.unlock();
		size_t td = _done;
		size_t ts = _started;
		if (td > ts)
		{
			std::cout << ts << " " << td<< "exited\n";
			break;
		}

		do {
			int task = _started++;
			if (task < _domain)
			{
				_kernel(task, tid);
				if (task == _domain - 1)
				{
					_done++;
					break;
				}
				else
					_done++;
			}
			else
				break;
		} while (true);
	}
}
