#pragma once
#include <vector>
#include <Image.h>
#include <Visualizer.h>

typedef std::vector<Color4F> Palette;

struct Dither {

	Dither(const std::vector<Image> &rawImgs, const size_t paletteSize);

	void initializeRandomShared();

	static constexpr const int neighbors_g0[][2] = {
		{ 0,  0}
	};
	static constexpr const int neighbors_g1[][2] = {
				  { 0, -1},
		{-1,  0}, { 0,  0}, {+1,  0},
				  { 0, +1}
	};
	static constexpr const int neighbors_g2[][2] = {
		{-1, -1}, { 0, -1}, {+1, -1},
		{-1,  0}, { 0,  0}, {+1,  0},
		{-1, +1}, { 0, +1}, {+1, +1}
	};
	static constexpr const int neighbors_g3[][2] = {
						   { 0, -2},
				 {-1, -1}, { 0, -1}, {+1, -1},
		{-2, 0}, {-1,  0}, { 0,  0}, {+1,  0}, {+2, 0},
				 {-1, +1}, { 0, +1}, {+1, +1},
						   { 0, +2},
	};
	static constexpr const int neighbors_g4[][2] = {
				  {-1, -2}, { 0, -2}, {+1, -2},
		{-2, -1}, {-1, -1}, { 0, -1}, {+1, -1}, {+2, -1},
		{-2,  0}, {-1,  0}, { 0,  0}, {+1,  0}, {+2,  0},
		{-2, +1}, {-1, +1}, { 0, +1}, {+1, +1}, {+2, +1},
				  {-1, +2}, { 0, +2}, {+1, +2},
	};
	static constexpr const int neighbors_g5[][2] = {
		           {-2, -3}, {-1, -3},/*overlap*/{+1, -3}, {+2, -3},
		 {-3, -2}, {-2, -2}, {-1, -2}, { 0, -2}, {+1, -2}, {+2, -2}, {+3, -2},
		 {-3, -1}, {-2, -1}, {-1, -1}, { 0, -1}, {+1, -1}, {+2, -1}, {+3, -1},
		/*overlap*/{-2,  0}, {-1,  0}, { 0,  0}, {+1,  0}, {+2,  0},/*overlap*/
		 {-3, +1}, {-2, +1}, {-1, +1}, { 0, +1}, {+1, +1}, {+2, +1}, {+3, +1},
		 {-3, +2}, {-2, +2}, {-1, +2}, { 0, +2}, {+1, +2}, {+2, +2}, {+3, +2},
		           {-2, +3}, {-1, +3},/*overlap*/{+1, +3}, {+2, +3},
	};
	static constexpr const int neighbors_g6[][2] = {
		                     {-2, -4}, {-1, -4}, { 0, -4}, {+1, -4}, {+2, -4},
		          /*overlap*/{-2, -3}, {-1, -3},/*overlap*/{+1, -3}, {+2, -3},/*overlap*/
		 {-4, -2}, {-3, -2}, {-2, -2}, {-1, -2}, { 0, -2}, {+1, -2}, {+2, -2}, {+3, -2}, {+4, -2},
		 {-4, -1}, {-3, -1}, {-2, -1}, {-1, -1}, { 0, -1}, {+1, -1}, {+2, -1}, {+3, -1}, {+4, -1},
		 {-4,  0},/*overlap*/{-2,  0}, {-1,  0}, { 0,  0}, {+1,  0}, {+2,  0},/*overlap*/{+4,  0},
		 {-4, +1}, {-3, +1}, {-2, +1}, {-1, +1}, { 0, +1}, {+1, +1}, {+2, +1}, {+3, +1}, {+4, +1},
		 {-4, +2}, {-3, +2}, {-2, +2}, {-1, +2}, { 0, +2}, {+1, +2}, {+2, +2}, {+3, +2}, {+4, +2},
		          /*overlap*/{-2, +3}, {-1, +3},/*overlap*/{+1, +3}, {+2, +3},/*overlap*/
		                     {-2, +4}, {-1, +4}, { 0, +4}, {+1, +4}, {+2, +4},
	};

	int neighbor_count;
	const int(*neighbors)[2];

	std::vector<std::vector<kmScalar>> ditherWeights;
	std::vector<std::vector<kmScalar>> paletteWeights;
	std::vector<std::vector<Color4F>> imgs;
	std::vector<std::vector<Color4F>> birefCache;
	std::vector<std::vector<Color4F>> gaussCache;
	std::vector<uint32_t> shared;
	std::vector<Palette> palettes;
	size_t width, height;
	size_t freeMemory, totalMemory;
	size_t maxBatchSize;

	enum class KernelWeightOptions {
		Bilateral,
		Linear
	};

	enum class EffFunc {
		Original,
		Quadratic,
		QuadraticFZ,
		Linear
	};

	void updateWeights(int neighborGroup, KernelWeightOptions ditherOption = KernelWeightOptions::Linear, KernelWeightOptions = KernelWeightOptions::Bilateral);

	void updatePalettes();
	void updateShared(int step, int groupId, const size_t target_diff, const EffFunc effFunc);
	void compressPalette(size_t compressedPaletteSize);
	void render(Visualizer &viz, int displayRows, int displayCols, const std::vector<Image> &rawImgs, int frameNum, bool drawJoint);

	struct ErrorMetrics {
		double bilateralError;
		double gaussianError;
	};
	ErrorMetrics computeTotalError(bool calcBilateral = true, bool calcGaussian = true);
	bool skip(int frameNum);

	void sortPalette();

private:
	void makeUseOfFullPalette();
	static double frameCost(double imageCost);
	kmScalar weightBilateral(kmScalar sim, kmScalar dx, kmScalar dy);
	kmScalar weightLinear(kmScalar sim, kmScalar dx, kmScalar dy);
	kmScalar s_dis, s_col;
};
