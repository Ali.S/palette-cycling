#define _USE_MATH_DEFINES
#pragma optimize("t", on)
#include <CommonHeaders.h>
#include <iostream>  
#include <fstream>
#include <SDL.h>
#include <future>
#include <Texture2D.h>
#include <Texture3D.h>
#include <random>
#include <queue>
#include <algorithm>
#include <direct.h>

#include <GarbageCollector.h>
#include <GLGlobals.h>
#include <thread>
#include "Image.h"
#include "Visualizer.h"
#include <Camera.h>
#include "ThreadUtil.h"
#include <array>
#include "GPURenderProgram.h"
#include "RenderTexture.h"
#include <cluster.h>
#include <Node.h>
#include <Mesh.h>
#include <array>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseQR>
#include <cluster.h>
#include <ColorSpace.h>
#include <filesystem>
#include "Dither.h"
#pragma optimize("", on)

using namespace std;

constexpr const char *vShaderSource = R"(
	varying vec2 vTexCoord;

void main(void) {
	gl_Position = getPosition();
	vTexCoord = a_TexCoord;
})";

constexpr const char *fShaderSource = R"(
	uniform float uFrame;
	uniform float uFrameCount;
	uniform float uPaletteSize;
	uniform sampler2D uShared;
	uniform sampler2D uPalette;
	varying vec2 vTexCoord;

	void main() {
		vec4 selectionC = texture2D(uShared, vTexCoord);
		float selection =
			floor(selectionC.r * 255.0 + 0.5) +
			floor(selectionC.g * 255.0 + 0.5) * 256.0 +
			floor(selectionC.b * 255.0 + 0.5) * 256.0 * 256.0;

		float time = (uFrame + 0.5) / uFrameCount;
		selection = (floor(selection) + 0.5) / uPaletteSize;
		gl_FragColor = texture2D(uPalette, vec2(selection, time));
		gl_FragColor.a = 1;
	})";

constexpr size_t frameMax = 230;
std::string outDir = "out/";
std::vector<std::string> userDirs;

volatile bool keypress = false;

bool goNext = false;
size_t to_upload[] = { 10, 20, 25, 40, 60, 120, frameMax };

#pragma optimize("", off)
bool pass(const std::vector<std::string> &inFiles, size_t paletteSize, const JSON& extraData) {
	std::vector<Image> rawImages(inFiles.size());
	std::vector<Dither::ErrorMetrics> frameErrors;
	std::chrono::high_resolution_clock clock;
	struct ClockMetric {
		double kernel, shared, compress, palette, sort, render, error;
		operator JSON() const {
			JSON json;
			json["kernel"] = kernel;
			json["shared"] = shared;
			json["compress"] = compress;
			json["palette"] = palette;
			json["sort"] = sort;
			json["render"] = render;
			json["error"] = error;
			return json;
		}

		ClockMetric() :kernel(0), shared(0), compress(0), palette(0), sort(0), render(0), error(0) {}
		ClockMetric(const JSON& json) {

			kernel = json["kernel"].as<double>();
			shared = json["shared"].as<double>();
			compress = json["compress"].as<double>();
			palette = json["palette"].as<double>();
			sort = json["sort"].as<double>();
			render = json["render"].as<double>();
			error = json["error"].as<double>();
		}
		ClockMetric(double kernel, double shared, double compress, double palette, double sort, double render, double error) :
			kernel(kernel), shared(shared), compress(compress), palette(palette), sort(sort), render(render), error(error) {};
	};
	std::vector<ClockMetric> frameClocks;

	for (size_t i = 0; i < inFiles.size(); i++)
		rawImages[i].load(inFiles[i]);

#ifndef NDEBUG
	for (size_t i = 1; i < inFiles.size(); i++)
		assert(rawImages[i].width() == rawImages[0].width() && rawImages[i].height() == rawImages[0].height());
#endif

	size_t width = rawImages[0].width();
	size_t height = rawImages[0].height();

	kmScalar displayScale = 0;
	size_t displayRows, displayCols;
	for (size_t i = 0; i < sqrt(inFiles.size()) + 1; i++)
	{
		size_t rows = i + 1;
		size_t cols = (inFiles.size() + i) / (i + 1);
		assert(rows * cols >= inFiles.size());
		kmScalar xScale = GLGlobals::getInstance()->getWinWidth() / static_cast<kmScalar>(cols * width + (cols - 1) * 20);
		kmScalar yScale = GLGlobals::getInstance()->getWinHeight() / static_cast<kmScalar>(rows * (height * 2 + 20) + (rows - 1) * 20);
		if (displayScale < min(xScale, yScale))
		{
			displayScale = min(xScale, yScale);
			displayCols = cols;
			displayRows = rows;
		}
		cols = i + 1;
		rows = (inFiles.size() + i) / (i + 1);
		assert(rows * cols >= inFiles.size());
		xScale = GLGlobals::getInstance()->getWinWidth() / static_cast<kmScalar>(cols * width + (cols - 1) * 20);
		yScale = GLGlobals::getInstance()->getWinHeight() / static_cast<kmScalar>(rows * (height * 2 + 20) + (rows - 1) * 20);
		if (displayScale < min(xScale, yScale))
		{
			displayScale = min(xScale, yScale);
			displayCols = cols;
			displayRows = rows;
		}
	}

	auto shader = GPUProgramCache::getInstance()->getProgram("palette");

	if (!shader) {
		shader = new GPURenderProgram(vShaderSource, fShaderSource);
		GPUProgramCache::getInstance()->addProgram("palette", shader);
	}

	Visualizer viz(displayScale);

	Dither dither(rawImages, paletteSize);

	auto parseWeightData = [](const JSON& j, std::string key, Dither::KernelWeightOptions def) {
		if (j.getType() == JSON::JTYPE_NULL ||  j.find(key) == j.objectEnd())
			return def;
		if (j[key].getValue() == "linear")
			return Dither::KernelWeightOptions::Linear;
		if (j[key].getValue() == "bilateral")
			return Dither::KernelWeightOptions::Bilateral;
		return def;
	};

	Dither::KernelWeightOptions ditherOption = parseWeightData(extraData, "weight_dither", Dither::KernelWeightOptions::Bilateral);
	Dither::KernelWeightOptions paletteOption = parseWeightData(extraData, "weight_palette", Dither::KernelWeightOptions::Bilateral);
	std::vector<size_t> kernel_switch;
	if (extraData.getType() != JSON::JTYPE_OBJECT || extraData.find("kernel_switch") == extraData.objectEnd())
	{
		kernel_switch.push_back(20);
		kernel_switch.push_back(60);
		kernel_switch.push_back(110);
		kernel_switch.push_back(170);
		/*
		[20,50,85,125,170,200]
		*/
	}
	else
	{
		kernel_switch.resize(extraData["kernel_switch"].size());
		for (size_t i = 0; i < kernel_switch.size(); i++)
			kernel_switch[i] = extraData["kernel_switch"][i].as<int>();
		std::sort(kernel_switch.begin(), kernel_switch.end());	
	}

	std::vector<size_t> kernel_use(frameMax + 1);
	for (size_t i = 0, j = 0; i <= frameMax; i++)
	{
		while(j < kernel_switch.size() && i == kernel_switch[j])
			j++;
		kernel_use[i] = j;
	}
	static constexpr int first_iter = 0;
	auto updateWeights = [&dither, &kernel_use](int i) {
		if (i == first_iter || kernel_use[i] != kernel_use[i - 1])
			dither.updateWeights(kernel_use[i]);
	};


	int i = first_iter;
	frameErrors.resize(i);
	frameClocks.resize(i);

	JSON cachedErrors;

	try {
		cachedErrors.load((outDir + "errors.json").c_str());
		for (size_t j = i; j < cachedErrors.size(); j++)
		{
			frameErrors.push_back({ 
				cachedErrors[j]["bilateral"].as<float>(), 
				cachedErrors[j]["gauss"].as<float>() });
			frameClocks.emplace_back(cachedErrors[j]["clock"]);
		}
	}
	catch (...) {

	}
	
	updateWeights(i);

	while (dither.skip(i) && frameClocks.size() > i)
	{
		if (i == frameErrors.size())
		{
			auto errorBeg = clock.now();
			frameErrors.push_back(dither.computeTotalError());
			auto errorEnd = clock.now();
			frameClocks[i].error = std::chrono::duration_cast<std::chrono::microseconds>(errorEnd - errorBeg).count() / 1000.0 / 1000.0;
		}
		assert(i <= frameErrors.size());

		i++;
		{
			auto weightBeg = clock.now();
			updateWeights(i);
			auto weightEnd = clock.now();
			frameClocks[i].kernel = std::chrono::duration_cast<std::chrono::microseconds>(weightEnd - weightBeg).count() / 1000.0 / 1000.0;
		}
	}

	if (frameErrors.size() > i)
		frameErrors.resize(i);
	if (frameClocks.size() > i)
		frameClocks.resize(i);


	if (i == first_iter) {
		dither.initializeRandomShared();
		dither.updatePalettes();
	}
	else
	{
		cout << "skipped to iteration: " << i << endl;
	}

	cachedErrors.clear();
	for (size_t j = 0; j < frameErrors.size(); j++)
	{
		cachedErrors[j]["bilateral"] = frameErrors[j].bilateralError;
		cachedErrors[j]["gauss"] = frameErrors[j].gaussianError;
		cachedErrors[j]["clock"] = frameClocks[j];
	}
	cachedErrors.save((outDir + "errors.json").c_str());
	Dither::EffFunc effFunc = Dither::EffFunc::Original;
	if (extraData.getType() == JSON::JTYPE_OBJECT && extraData.find("eff_func") != extraData.objectEnd())
	{
		if (extraData["eff_func"].getValue() == "original")
			;
		else if (extraData["eff_func"].getValue() == "quadratic")
			effFunc = Dither::EffFunc::Quadratic;
		else if (extraData["eff_func"].getValue() == "linear")
			effFunc = Dither::EffFunc::Linear;
		else if (extraData["eff_func"].getValue() == "quadratic_fz")
			effFunc = Dither::EffFunc::QuadraticFZ;
	}

	dither.render(viz, displayRows, displayCols, rawImages, -1, false);
	if (i == frameMax + 1)
		return false;
	const int compressionRate = extraData.getType() == JSON::JTYPE_NULL || extraData.find("compression") == extraData.objectEnd() ? 5 : extraData["compression"].as<int>();

	for (; i <= frameMax; i++)
	{
		frameClocks.emplace_back();
		auto clock_start = clock.now();

		cout << "gen = " << i  << "-> begin" << endl;
		
		updateWeights(i);
		auto clock_weights = clock.now();
		frameClocks.back().kernel = std::chrono::duration_cast<std::chrono::microseconds>(clock_weights - clock_start).count() / 1000.0 / 1000.0;
		size_t target_diff = i * 3 < frameMax * 2 ? 2 : 1;
		if (i + 10 >= frameMax)
			target_diff = 0;
		
		dither.updateShared(i, (i + 3) % 9, target_diff, effFunc);
		dither.updateShared(i, (i + 6) % 9, target_diff, effFunc);
		dither.updateShared(i, (i + 9) % 9, target_diff, effFunc);

		if (extraData.getType() != JSON::JTYPE_NULL && extraData.find("update_all") != extraData.objectEnd())
		{

			dither.updateShared(i, (i + 4) % 9, target_diff, effFunc);
			dither.updateShared(i, (i + 7) % 9, target_diff, effFunc);
			dither.updateShared(i, (i + 10) % 9, target_diff, effFunc);

			dither.updateShared(i, (i + 5) % 9, target_diff, effFunc);
			dither.updateShared(i, (i + 8) % 9, target_diff, effFunc);
			dither.updateShared(i, (i + 11) % 9, target_diff, effFunc);
		}

		auto clock_shared = clock.now();
		frameClocks.back().shared = std::chrono::duration_cast<std::chrono::microseconds>(clock_shared - clock_weights).count() / 1000.0 / 1000.0;
		if (i % compressionRate == 0 && i > 6) {
			size_t compressed_size = paletteSize - ((paletteSize / 4) * (frameMax * 2 - i * 3)) / (frameMax) / 2;
			if (compressed_size < paletteSize)
				dither.compressPalette(compressed_size);
		}
		auto clock_compress = clock.now();
		frameClocks.back().compress = std::chrono::duration_cast<std::chrono::microseconds>(clock_compress - clock_shared).count() / 1000.0 / 1000.0;
		
		dither.updatePalettes();
		auto clock_palette = clock.now();
		frameClocks.back().palette = std::chrono::duration_cast<std::chrono::microseconds>(clock_palette - clock_compress).count() / 1000.0 / 1000.0;

		dither.sortPalette();
		auto clock_sort = clock.now();
		frameClocks.back().sort = std::chrono::duration_cast<std::chrono::microseconds>(clock_sort - clock_palette).count() / 1000.0 / 1000.0;
		
		dither.render(viz, displayRows, displayCols, rawImages, i, (i * 4) / frameMax != ((i - 1) * 4) / frameMax);
		auto clock_render = clock.now();
		frameClocks.back().render = std::chrono::duration_cast<std::chrono::microseconds>(clock_render - clock_sort).count() / 1000.0 / 1000.0;

		cout << "gen = " << i << "-> errors" << endl;
		//frameErrors.push_back(dither.computeTotalError());

		frameErrors.push_back(Dither::ErrorMetrics{});
		auto clock_error = clock.now();		
		frameClocks.back().error = std::chrono::duration_cast<std::chrono::microseconds>(clock_error - clock_render).count() / 1000.0 / 1000.0;
		
		cachedErrors[i]["bilateral"] = frameErrors[i].bilateralError;
		cachedErrors[i]["gauss"] = frameErrors[i].gaussianError;
		cachedErrors[i]["clock"] = frameClocks[i];
		cachedErrors.save((outDir + "errors.json").c_str());

		cout << "gen = " << i << "-> end" << endl;
		cout.flush();
	}
	return true;
}
#pragma optimize("", on)

static bool allDone;

class teebuf : public std::streambuf
{
public:
	// Construct a streambuf which tees output to both input
	// streambufs.
	teebuf(std::streambuf * sb1, std::streambuf * sb2)
		: sb1(sb1)
		, sb2(sb2)
	{
	}
private:
	// This tee buffer has no buffer. So every character "overflows"
	// and can be put directly into the teed buffers.
	virtual int overflow(int c)
	{
		if (c == EOF)
		{
			return !EOF;
		}
		else
		{
			int const r1 = sb1->sputc(c);
			int const r2 = sb2->sputc(c);
			return r1 == EOF || r2 == EOF ? EOF : c;
		}
	}

	// Sync both teed buffers.
	virtual int sync()
	{
		int const r1 = sb1->pubsync();
		int const r2 = sb2->pubsync();
		return r1 == 0 && r2 == 0 ? 0 : -1;
	}
private:
	std::streambuf * sb1;
	std::streambuf * sb2;
};
#pragma optimize("", off)
int work(SDL_Window *extra_window, SDL_GLContext extra_context)
{
	setThreadName("renderer");
	SDL_GL_MakeCurrent(extra_window, extra_context);

	int err = glGetError();
	auto check_gl = glGetString(GL_RENDERER);
	err = glGetError();
	


	std::vector<size_t> defaultPaletteSize = {
		16,
		64,
		256,
		1024,
		4096,
	};
	
	struct Task {
		std::vector<std::string> files;
		std::vector<size_t> paletteSize;
		std::vector<JSON> extraData;
	};

	std::vector<Task> tasks;

	for (const auto & path : userDirs)
	{

		std::cout << "processing path " << path << "\n";
		std::vector<std::string> files;
		std::vector<size_t> paletteSize;
		std::vector<JSON> extraData;
		try {
			std::filesystem::path src_dir(path);
			for (const auto & entry : std::filesystem::directory_iterator(src_dir))
			{
				if (entry.is_regular_file() && entry.path().extension() == ".png")
					files.push_back(entry.path().string());
			}

			if (std::filesystem::exists(src_dir / "config.json"))
			{
				JSON config;
				config.load((src_dir / "config.json").generic_string().c_str());
				for (auto ps = config.arrayBegin(); ps != config.arrayEnd(); ps++)
					if ((*ps)->getType() == JSON::JTYPE_NUMBER) {
						if ((*ps)->as<int>() > 0)
						{
							paletteSize.emplace_back((*ps)->as<int>());
							extraData.emplace_back();
						}

					}
					else
					{
						extraData.push_back(**ps);
						paletteSize.emplace_back((**ps)["palette_size"].as<int>());
					}
			}
			else
			{
				paletteSize = defaultPaletteSize;
			}
		}
		catch (std::filesystem::filesystem_error error) {
			std::cout << error.what() << "\n";
			continue;
		}
		tasks.emplace_back(Task{ files, paletteSize, extraData});
	};
	size_t k = 0;
	while (true)
	{
		bool hasTask = false;
		for (auto & t:tasks)
		{
			if (k >= t.paletteSize.size())
				continue;
			hasTask = true;
			auto & files = t.files;
			auto & paletteSize = t.paletteSize[k];
			auto & extraData = t.extraData[k];

			streambuf *conOut = cout.rdbuf();
			if (extraData.getType() == JSON::JTYPE_NULL || extraData.find("name") == extraData.objectEnd())
				outDir = (std::filesystem::path(files[0]).parent_path() / ("out_" + to_string(paletteSize) + "/")).string();
			else
				outDir = (std::filesystem::path(files[0]).parent_path() / ("out_" + to_string(paletteSize) + "_" + extraData["name"].getValue() + "/")).string();

			std::filesystem::create_directories(outDir);
			ofstream logFile((outDir + "cout.log").c_str());
			teebuf *tee = new teebuf(conOut, logFile.rdbuf());
			cout.rdbuf(tee);
			auto begin = std::chrono::high_resolution_clock::now();
			bool updates = pass(files, paletteSize, extraData) || true;
			
			cout << "finished in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - begin).count() / 1000.0f << "second\n";
			cout << "\n------------------------\n\n";
			delete tee;
			cout.rdbuf(conOut);
		}
		if (!hasTask)
			break;
		else
			k += 1;
	}
	allDone = true;
	return 0;
}

int main(int argc, char *argv[])
{
	if (argc > 1)
		for (int i = 1; i < argc; i++)
			userDirs.push_back(argv[i]);

	allDone = false;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		Log("Error initializing video subsystem: %s\n", SDL_GetError());
		return -1;
	}

	SetProcessDPIAware();

	if (GLGlobals::getInstance()->getSetupResult() == false)
	{
		SDL_Quit();
		return -2;
	}

	auto oldWindow = SDL_GL_GetCurrentWindow();
	auto oldContext = SDL_GL_GetCurrentContext();

	SDL_GL_MakeCurrent(nullptr, nullptr);
	int err = glGetError();
	std::thread backgroundWorkerThread(work, oldWindow, oldContext);

	SDL_Event e;
	int mouseDown = 0;

	while (allDone == false)
	{
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				allDone = true;
			}
			if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				SDL_DestroyWindow(SDL_GetWindowFromID(e.window.windowID));
				allDone = true;
				e.type = SDL_QUIT;
				exit(0);
				break;
			}

			if (e.type == SDL_KEYDOWN)
			{
				keypress = true;
			}
			if (e.type == SDL_KEYUP)
			{
				keypress = false;
			}

			if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
				goNext = true;

		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	backgroundWorkerThread.join();

	SDL_GL_DeleteContext(oldContext);
	SDL_Quit();

	cout << "press enter to continue.";
	cin.get();

	return 0;
}

