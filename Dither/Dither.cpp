#include "Dither.h"
#include "Dither.h"
#include <random>
#include <GarbageCollector.h>
#include <GLGlobals.h>
#include <thread>
#include "Image.h"
#include "Visualizer.h"
#include <Camera.h>
#include "ThreadUtil.h"
#include <array>
#include "GPURenderProgram.h"
#include "RenderTexture.h"
#include <cluster.h>
#include <Node.h>
#include <Mesh.h>
#include <array>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseQR>
#include <cluster.h>
#include <ColorSpace.h>
#include <filesystem>
#include <IteratorUtil.h>
#include <Gaussian.h>

extern std::string outDir;

kmScalar labSimilarity(const Color4F &x, const Color4F &y) {
	kmScalar ld = x.r - y.r;
	kmScalar ad = x.g - y.g;
	kmScalar bd = x.b - y.b;
	kmScalar distance = sqrt(ld * ld + ad * ad + bd * bd);
	distance = distance / 180;// max lab distance, computed by experiment
	return 1 - 2 * distance;
}

Dither::Dither(const std::vector<Image>& rawImgs, const size_t paletteSize)
{
	width = rawImgs[0].width();
	height = rawImgs[0].height();
	shared.resize(width * height);
	palettes.resize(rawImgs.size());
	for (size_t i = 0; i < rawImgs.size(); i++)
		palettes[i].resize(paletteSize);
	imgs.resize(rawImgs.size());
	birefCache.resize(rawImgs.size());
	gaussCache.resize(rawImgs.size());
	sequal_for_each(imgs.size(), [&](size_t k, size_t)
	{
		imgs[k].resize(width * height);
		for (size_t y = 0, i = 0; y < height; y++)
			for (size_t x = 0; x < width; x++, i++)
			{
				Color4B c = rawImgs[k].getPixel(x, y);
				ColorSpace::Rgb rgb;
				rgb.r = c.r;
				rgb.g = c.g;
				rgb.b = c.b;
				ColorSpace::Lab lab;
				rgb.To(&lab);
				imgs[k][i] = Color4F(lab.l, lab.a, lab.b);
			}
	});

	ditherWeights.resize(imgs.size());
	paletteWeights.resize(imgs.size());
	updateWeights(0);
}

void Dither::initializeRandomShared() {
	std::default_random_engine re;
	for (size_t i = 0; i < shared.size(); i++)
		shared[i] = re() % palettes[0].size();
}

#pragma optimize("", on)

kmScalar Dither::weightLinear(kmScalar sim, kmScalar dx, kmScalar dy)
{
	//return weightPalette(sim, dx, dy);
	kmScalar d = sqrt(dx * dx + dy * dy);
	kmScalar c = 1. + sim + s_col;
	
	return c * exp(-d * (0.6 / s_dis));
}

kmScalar Dither::weightBilateral(kmScalar sim, kmScalar dx, kmScalar dy)
{
	//return dx == 0 && dy == 0 ? 1 : 0;
	kmScalar d = sqrt(dx * dx + dy * dy) / (s_dis);
	kmScalar c = (1 - sim) / s_col;
	return exp(-c - d);
}

template<typename R, typename C, typename... Args>
static constexpr std::function<R(Args...)> make_free(R(C::* func)(Args...), C* instance) {
	return [instance, func](Args... args) {
		return (instance->*func)(args...);
	};
}

void Dither::updateWeights(int neighborGroup, KernelWeightOptions ditherOption, KernelWeightOptions paletteOption)
{
	auto weightPalette = paletteOption == KernelWeightOptions::Linear ? make_free(&Dither::weightLinear, this) : make_free(&Dither::weightBilateral, this);
	auto weightDither = ditherOption == KernelWeightOptions::Linear ? make_free(&Dither::weightLinear, this) : make_free(&Dither::weightBilateral, this);

	switch (neighborGroup) {
	case 0: neighbors = neighbors_g0; neighbor_count = std::size(neighbors_g0); s_dis = 0.01;  break;
	case 1: neighbors = neighbors_g1; neighbor_count = std::size(neighbors_g1); s_dis = 0.2; break;
	case 2: neighbors = neighbors_g2; neighbor_count = std::size(neighbors_g2); s_dis = 0.3; break;
	case 3: neighbors = neighbors_g3; neighbor_count = std::size(neighbors_g3); s_dis = 0.4; break;
	case 4: neighbors = neighbors_g4; neighbor_count = std::size(neighbors_g4); s_dis = 0.5; break;
	case 5: neighbors = neighbors_g5; neighbor_count = std::size(neighbors_g5); s_dis = 0.5; break;
	case 6: neighbors = neighbors_g6; neighbor_count = std::size(neighbors_g6); s_dis = 0.5; break;
	default:return;
	}
	std::cout << "weight change: " << neighbor_count << "neighbors\n";
	for (size_t k = 0; k < imgs.size(); k++)
	{
		ditherWeights[k].resize(width * height * neighbor_count);
		paletteWeights[k].resize(width * height * neighbor_count);
	}

	if (neighbor_count == 1)
	{
		for (size_t k = 0; k < imgs.size(); k++)
		{
			std::fill(ditherWeights[k].begin(), ditherWeights[k].end(), 1);
			std::fill(paletteWeights[k].begin(), paletteWeights[k].end(), 1);
		}
		return;
	}


	s_col = 1.5f;
	

	sequal_for_each(imgs[0].size(), [&](size_t pixel, size_t threadId)
	{
		size_t x = pixel % width;
		size_t y = pixel / width;
		for (size_t k = 0; k < ditherWeights.size(); k++) {
			kmScalar totalWeightP = 0, totalWeightD = 0;
			size_t i = pixel * neighbor_count;

			for (size_t j = 0; j < neighbor_count; j++, i++)
			{
				size_t nx = neighbors[j][0] + x;
				size_t ny = neighbors[j][1] + y;
				if (nx < width && ny < height)
				{
					auto sim = labSimilarity(imgs[k][ny * width + nx], imgs[k][pixel]);
					ditherWeights[k][i] = weightDither(sim, neighbors[j][0], neighbors[j][1]);
					paletteWeights[k][i] = weightPalette(sim, neighbors[j][0], neighbors[j][1]);

					totalWeightD += ditherWeights[k][i];
					totalWeightP += paletteWeights[k][i];
				}
				else
				{
					ditherWeights[k][i] = 0;
					paletteWeights[k][i] = 0;
				}
			}
			/*if (x == 130 && y == 155 && neighborGroup == 6)
				__debugbreak();*/
			i = pixel * neighbor_count;
			for (size_t j = 0; j < neighbor_count; j++, i++)
			{
				ditherWeights[k][i] /= totalWeightD;
				paletteWeights[k][i] /= totalWeightP;
			}
		}
	});


	maxBatchSize = -1;
}
#pragma optimize("", on)
void Dither::makeUseOfFullPalette() {
	std::vector<size_t> useCount(palettes[0].size(), 0);
	std::vector<kmScalar> usageError(palettes[0].size(), 0);

	for (auto &selection : shared)
		useCount[selection] ++;

	std::vector<size_t> usedPaletteColors;
	std::vector<size_t> unusedPaletteColors;

	for (size_t i = 0; i < useCount.size(); i++)
		if (useCount[i] > 0)
		{
			usedPaletteColors.push_back(i);
		}
		else
			unusedPaletteColors.push_back(i);

	std::cout << "unused color palettes: " << unusedPaletteColors.size() << std::endl;

	if (unusedPaletteColors.size() > 0)
	{
		std::vector<std::pair<kmScalar, size_t> > costs(shared.size());
		sequal_for_each(shared.size(), [&](size_t task_id, size_t thread_id) {
			size_t x = task_id % width;
			size_t y = task_id / width;
			std::vector<Color4F> sumColor(imgs.size());
			std::vector<Color4F> targetColor(imgs.size());

			std::vector<double> centerWeight(imgs.size());

			for (size_t j = 0; j < imgs.size(); j++)
			{
				targetColor[j].r = targetColor[j].g = targetColor[j].b = 0;
				sumColor[j].r = sumColor[j].g = sumColor[j].b = 0;

				for (size_t k = 0; k < neighbor_count; k++)
				{
					size_t nx = neighbors[k][0] + x;
					size_t ny = neighbors[k][1] + y;
					if (nx < width && ny < height)
					{
						if (neighbors[k][0] == 0 && neighbors[k][1] == 0)
							centerWeight[j] = ditherWeights[j][task_id * neighbor_count + k];
						else
							sumColor[j] += palettes[j][shared[nx + ny * width]] * ditherWeights[j][task_id * neighbor_count + k];
						targetColor[j] += imgs[j][nx + ny * width] * ditherWeights[j][task_id * neighbor_count + k];
					}
				}
			}

			double best_cost = std::numeric_limits<double>::max();

			for (size_t i = 0; i < usedPaletteColors.size(); i++)
			{
				double match_cost = 0;

				for (size_t j = 0; j < imgs.size(); j++)
				{
					auto testAssign = sumColor[j] + palettes[j][usedPaletteColors[i]] * centerWeight[j];
					double imageCost = (1 - labSimilarity(testAssign, targetColor[j]));
					match_cost += frameCost(imageCost);
				}

				if (match_cost < best_cost)
					best_cost = match_cost;
			}
			costs[task_id].first = best_cost;
			costs[task_id].second = task_id;

		});

		sort(costs.rbegin(), costs.rend());
		
		for (size_t i = 0, replacement = 0; i < unusedPaletteColors.size(); i++, replacement ++)
		{/*
			char buffer[1024];
			char* bf = buffer;
			bf += snprintf(bf, size(buffer) + buffer - bf, "%4dx%4d -> ", costs[i].second % width, costs[i].second / width);
			for (size_t j = 0; j < palettes.size(); j++)
				bf += snprintf(bf, size(buffer) + buffer - bf, "%7.2f ", imgs[j][costs[i].second].r);

			std::cout << buffer << "\n";*/
			while (useCount[shared[costs[replacement].second]] <= 2)
				replacement++;

			useCount[shared[costs[replacement].second]] --;
			shared[costs[replacement].second] = unusedPaletteColors[i];
			useCount[shared[costs[replacement].second]] ++;
		}
	}
	std::cout << "palette fully used" << std::endl;
}

double Dither::frameCost(double imageCost)
{
	return imageCost * imageCost;
}
//#pragma optimize("", off)
void Dither::updatePalettes()
{
	makeUseOfFullPalette();

	std::vector<std::default_random_engine> rand_engine(ThreadPool::instance()->getPoolSize());
	std::vector<size_t> useCount(palettes[0].size(), 0);
	std::vector<kmScalar> usageError(palettes[0].size(), 0);

	for (auto &selection : shared)
		useCount[selection] ++;

	for (size_t i = 0; i < useCount.size(); i++)
		assert(useCount[i] > 0);



	if (neighbor_count == 1) // we only need to average so let's not bother GPU
	{
		sequal_for_each(imgs.size(), [&](size_t task_id, size_t) {
			auto &palette = palettes[task_id];
			auto oldPalette = palette;
			auto &img = imgs[task_id];
			for (size_t i = 0; i < palette.size(); i++)
				palette[i].r = palette[i].g = palette[i].b = 0;
			std::vector<kmScalar> sumW(palette.size(), 0);
			for (size_t i = 0; i < img.size(); i++)
			{
				auto c = shared[i];
				auto w = paletteWeights[task_id][i];
				palette[c] += img[i] * w;
				sumW[c] += w;
			}
			for (size_t i = 0; i < palette.size(); i++)
				palette[i] /= sumW[i];
		});
		return;
	}


	const int row = width * height;
	const int col = palettes[0].size();
	std::mutex gpuLock;

	std::vector<int> h_Ac;
	std::vector<int> h_Arp;
	h_Arp.push_back(0);
	for (size_t i = 0; i < imgs[0].size(); i++)
	{
		size_t x = i % width;
		size_t y = i / width;

		std::set<size_t> w;

		for (size_t j = 0; j < neighbor_count; j++)
		{
			size_t nx = neighbors[j][0] + x;
			size_t ny = neighbors[j][1] + y;
			if (nx < width && ny < height)
			{
				uint32_t selection = shared[nx + width * ny];
				assert(selection < palettes[0].size());
				w.insert(selection);
			}
		}
		for (auto &k : w)
		{
			h_Ac.push_back(k);
		}
		h_Arp.push_back(h_Ac.size());
	}

	struct VecArrayContainer {
		std::vector<kmScalar> data[3];
	};
	std::vector <VecArrayContainer> h_x_list(imgs.size());
	std::vector < std::vector<kmScalar>> h_Av_list(imgs.size());
	std::vector <VecArrayContainer> h_b_list(imgs.size());

	std::atomic<int> cpuSolveDirect = 0, cpuSolve40 = 0, cpuSolve24 = 0;
	std::mutex statusMutex;

	enum class FrameStatus {
		NotStarted,
		PreprocessDone,
		InQueue,
		Solved
	};

	std::vector <FrameStatus> status_list(imgs.size(), FrameStatus::NotStarted);

	sequal_for_each(imgs.size(), [&](size_t task_id, size_t threadId) {
		auto &img = imgs[task_id];
		auto &palette = palettes[task_id];
		auto &paletteWeights = this->paletteWeights[task_id];


		std::vector<kmScalar> &h_Av = h_Av_list[task_id];
		h_Av.reserve(h_Ac.size());

		std::vector<kmScalar>(&h_b)[3] = h_b_list[task_id].data;

		h_b[0].resize(row);
		h_b[1].resize(row);
		h_b[2].resize(row);

		for (size_t i = 0; i < img.size(); i++)
		{
			h_b[0][i] = 0;
			h_b[1][i] = 0;
			h_b[2][i] = 0;
			size_t x = i % width;
			size_t y = i / width;

			std::map<size_t, double> w;
			double sumW = 0;
			for (size_t j = 0; j < neighbor_count; j++)
			{
				size_t nx = neighbors[j][0] + x;
				size_t ny = neighbors[j][1] + y;
				if (nx < width && ny < height)
				{
					uint32_t selection = shared[nx + ny * width];
					assert(selection < palette.size());
					size_t ni = i * neighbor_count + j;
					kmScalar nw = paletteWeights[ni];

					if (w.find(selection) == w.end())
						w[selection] = nw;
					else
						w[selection] += nw;
					sumW += nw;

					if (isnan(nw))
						__debugbreak();
					h_b[0][i] += img[nx + ny * width].r * nw;
					h_b[1][i] += img[nx + ny * width].g * nw;
					h_b[2][i] += img[nx + ny * width].b * nw;

				}
			}
			if (isnan(sumW) || kmAlmostEqual(sumW, 0))
				__debugbreak();
			for (auto &k : w)
			{
				h_Av.push_back(k.second / sumW);
			}
		}
		std::vector<kmScalar>(&h_x)[3] = h_x_list[task_id].data;
		h_x[0].resize(palette.size());
		h_x[1].resize(palette.size());
		h_x[2].resize(palette.size());
		for (size_t i = 0; i < col; i++)
		{
			h_x[0][i] = palette[i].r;
			h_x[1][i] = palette[i].g;
			h_x[2][i] = palette[i].b;
		}
		status_list[task_id] = FrameStatus::PreprocessDone;
	});

	std::condition_variable checkList;
	if (imgs.size() > ThreadPool::instance()->getPoolSize())
		omp_set_num_threads(2);
	else
		omp_set_num_threads(ThreadPool::instance()->getPoolSize() / imgs.size() + 1);

	sequal_for_each(std::min(ThreadPool::instance()->getPoolSize(), imgs.size()), [&](size_t task_id, size_t thread_id) {
		int solved = 0, inqueue = 0;
		int nextAvailable = -1;
		std::unique_lock<std::mutex> ul(statusMutex);

		for (size_t i = 0; i < status_list.size(); i++)
		{
			if (status_list[i] == FrameStatus::PreprocessDone)
				nextAvailable = i;
		}

		if (nextAvailable != -1)
			status_list[nextAvailable] = FrameStatus::InQueue;

		ul.unlock();
		do
		{
			if (nextAvailable != -1)
			{
				Eigen::Map<const Eigen::SparseMatrix<kmScalar, Eigen::RowMajor> > mapping(row, col, h_Ac.size(), h_Arp.data(), h_Ac.data(), h_Av_list[nextAvailable].data());
				Eigen::SparseMatrix<double> A = mapping.cast<double>();
				Eigen::SparseQR<decltype(A), Eigen::NaturalOrdering<int> > solverDirect;
				Eigen::LeastSquaresConjugateGradient<decltype(A)> solverInc;
				Eigen::Matrix<double, -1, 3> B(row, 3);
				for (size_t i = 0; i < row; i++)
				{
					B(i, 0) = h_b_list[nextAvailable].data[0][i];
					B(i, 1) = h_b_list[nextAvailable].data[1][i];
					B(i, 2) = h_b_list[nextAvailable].data[2][i];
				}
				solverInc.compute(A);
				solverInc.setTolerance(1.0 / (1ll << 40));
				bool success = false;
				if (solverInc.info() == Eigen::Success)
				{
					Eigen::Matrix<double, -1, -1> X0(col, 3);
					for (size_t i = 0; i < col; i++)
					{
						X0(i, 0) = h_x_list[nextAvailable].data[0][i];
						X0(i, 1) = h_x_list[nextAvailable].data[1][i];
						X0(i, 2) = h_x_list[nextAvailable].data[2][i];
					}
					Eigen::Matrix<double, -1, -1> X = solverInc.solveWithGuess(B, X0);
					if (solverInc.info() == Eigen::Success)
					{
						for (size_t i = 0; i < col; i++)
						{
							h_x_list[nextAvailable].data[0][i] = X(i, 0);
							h_x_list[nextAvailable].data[1][i] = X(i, 1);
							h_x_list[nextAvailable].data[2][i] = X(i, 2);
						}
						success = true;
						cpuSolve40++;
					}
				}
				if (success == false)
				{
					solverInc.setTolerance(1.0 / (1 << 24));
					bool success = false;
					if (solverInc.info() == Eigen::Success)
					{
						Eigen::Matrix<double, -1, -1> X0(col, 3);
						for (size_t i = 0; i < col; i++)
						{
							X0(i, 0) = h_x_list[nextAvailable].data[0][i];
							X0(i, 1) = h_x_list[nextAvailable].data[1][i];
							X0(i, 2) = h_x_list[nextAvailable].data[2][i];
						}
						Eigen::Matrix<double, -1, -1> X = solverInc.solveWithGuess(B, X0);
						if (solverInc.info() == Eigen::Success)
						{
							for (size_t i = 0; i < col; i++)
							{
								h_x_list[nextAvailable].data[0][i] = X(i, 0);
								h_x_list[nextAvailable].data[1][i] = X(i, 1);
								h_x_list[nextAvailable].data[2][i] = X(i, 2);
							}
							success = true;
							cpuSolve24++;
						}
					}
				}
				if (success == false)
				{
					solverDirect.compute(A);
					if (solverDirect.info() == Eigen::Success)
					{
						Eigen::Matrix<double, -1, -1> X = solverDirect.solve(B);
						if (solverInc.info() == Eigen::Success)
						{
							for (size_t i = 0; i < col; i++)
							{
								h_x_list[nextAvailable].data[0][i] = X(i, 0);
								h_x_list[nextAvailable].data[1][i] = X(i, 1);
								h_x_list[nextAvailable].data[2][i] = X(i, 2);
							}
							success = true;
							cpuSolveDirect++;
						}
					}
				}

				if (!success)
				{
					status_list[nextAvailable] = FrameStatus::PreprocessDone;
				}
				else
				{
					status_list[nextAvailable] = FrameStatus::Solved;
				}
			}

			ul.lock();
			checkList.notify_all();

			nextAvailable = -1;
			inqueue = 0;
			solved = 0;
			for (size_t i = 0; i < status_list.size(); i++)
			{
				if (status_list[i] == FrameStatus::Solved)
					solved++;
				if (status_list[i] == FrameStatus::InQueue)
					inqueue++;
				if (status_list[i] == FrameStatus::PreprocessDone)
					nextAvailable = i;
			}


			progressBar(80, static_cast<float>(solved) / imgs.size(), static_cast<float>(inqueue + solved) / imgs.size());
			
			if (nextAvailable != -1)
				status_list[nextAvailable] = FrameStatus::InQueue;
			ul.unlock();
		} while (solved != imgs.size());
	}, nullptr);
	std::cout << "Cpu solved: (" << cpuSolve40 << ", " << cpuSolve24 << ", " << cpuSolveDirect << ")"<< std::endl;

	sequal_for_each(imgs.size(), [&](size_t task_id, size_t threadId) {
		auto &palette = palettes[task_id];
		auto &h_x = h_x_list[task_id].data;
		for (size_t i = 0; i < palette.size(); i++)
		{
			assert(!isnan(h_x[0][i]));
			palette[i].r = h_x[0][i];
			palette[i].g = h_x[1][i];
			palette[i].b = h_x[2][i];
		}
	});

	sequal_for_each(imgs.size(), [&](size_t task_id, size_t threadId) {
		auto &palette = palettes[task_id];
		auto &h_x = h_x_list[task_id].data;
		for (size_t i = 0; i < palette.size(); i++)
		{
			if (palette[i].r < 0 || palette[i].r >= 100.f ||
				palette[i].g < -128 || palette[i].g >= 128.f ||
				palette[i].b < -128 || palette[i].b >= 128.f)
			{
				ColorSpace::Lab lab;
				lab.l = palette[i].r;
				lab.a = palette[i].g;
				lab.b = palette[i].b;
				ColorSpace::Rgb rgb;
				lab.To(&rgb);
				rgb.r -= 127.5f;
				rgb.g -= 127.5f;
				rgb.b -= 127.5f;
				auto mx = std::max({ std::fabs(rgb.r), std::fabs(rgb.g), std::fabs(rgb.b) });
				rgb.r = rgb.r * 127.5 / mx + 127.5;
				rgb.g = rgb.g * 127.5 / mx + 127.5;
				rgb.b = rgb.b * 127.5 / mx + 127.5;
				rgb.To(&lab);
				palette[i].r = lab.l;
				palette[i].g = lab.a;
				palette[i].b = lab.b;

			}

			if (isnan(palette[i].r) || isnan(palette[i].g) || isnan(palette[i].b))
				__debugbreak();
		}
	});
}

#pragma optimize("", on)
void Dither::updateShared(int step, int groupId, const size_t target_diff, const EffFunc effFunc)
{	
	std::vector<std::default_random_engine> re(ThreadPool::instance()->getPoolSize());
	for(size_t i=0; i < re.size(); i++)
		re[i].seed(i);

	std::vector<uint32_t> newShared(shared.size());
	std::atomic<size_t> groupSize = 0;
	std::atomic<double> chance[3] = { 0, 0, 0 };
	double target_eff;
	
	switch (effFunc)
	{
	case EffFunc::Original: target_eff = fabs(25 - step) * pow(step + 10, 1) / 20 + 2; break;
	case EffFunc::Linear: target_eff = step + 10 / 20 + 2; break;
	case EffFunc::Quadratic: target_eff = (step - 25) * (step - 25) / 20 + 2; break;
	case EffFunc::QuadraticFZ: target_eff = step * step / 20 + 2; break;
	}

	static int inspect_pixel_x = 28, inspect_pixel_y = 24;
	
	sequal_for_each(width * height, [&](size_t task_id, size_t threadId) {
		size_t x = task_id % width;
		size_t y = task_id / width;
		size_t group = (x % 3) * 3 + (x + y) % 3;

		newShared[task_id] = shared[task_id];
		if (group != groupId)
			return;

		std::vector<Color4F> sumNeighbors(imgs.size());
		std::vector<Color4F> targetColor(imgs.size());
		std::vector<kmScalar> centerWeight(imgs.size());

		for (size_t j = 0; j < imgs.size(); j++)
		{
			targetColor[j].r = targetColor[j].g = targetColor[j].b = 0;
			sumNeighbors[j].r = sumNeighbors[j].g = sumNeighbors[j].b = 0;
			for (size_t k = 0; k < neighbor_count; k++)
			{
				size_t nx = neighbors[k][0] + x;
				size_t ny = neighbors[k][1] + y;
				auto w = ditherWeights[j][task_id * neighbor_count + k];
				if (nx < width && ny < height)
				{
					if (neighbors[k][0] != 0 || neighbors[k][1] != 0)
						sumNeighbors[j] += palettes[j][shared[nx + ny * width]] * w;
					else
						centerWeight[j] = w;

					targetColor[j] += imgs[j][nx + ny * width] * w;
				}
			}
		}
		/*
		if (x == inspect_pixel_x && y == inspect_pixel_y)
			__debugbreak();*/
		
		auto maxPossibility = palettes[0].size();

		double sumOfAllChances = 0;
		std::vector<std::pair<double, size_t>> chances(maxPossibility);

		for (size_t i = 0; i < maxPossibility; i++)
		{
			double cost = 1e-12; // to avoid division by zero
			for (size_t j = 0; j < imgs.size(); j++)
			{
				Color4F sumColor;
				if (centerWeight[j] < 0.999)
				{
					auto &cw = centerWeight[j];
					const Color4F &sn = sumNeighbors[j];

					const Color4F &sp = palettes[j][i];
					sumColor = (sn + sp * cw);
				}
				else
				{
					sumColor = palettes[j][i];
				}
				double imageCost = (1 - labSimilarity(sumColor, targetColor[j]));
				if (isnan(imageCost))
					__debugbreak();
				cost += frameCost(imageCost);
				
			}
			chances[i].first = sqrt(cost);
			if (isnan(chances[i].first))
				__debugbreak();
			chances[i].second = i;
		}
		sort(chances.begin(), chances.end());
		uint32_t selection;
		if (target_diff > 0)
		{
			size_t drop = target_diff;
			while (drop + 1 < chances.size() && chances[drop].first / chances[0].first < 1.001)// almost equal doesn't matter
				drop += 1;
			groupSize++;
			
			double eff = pow(std::min(log(target_eff) / log(chances[drop].first / chances[0].first), 1e5), 0.8);
			if (eff > 5e4 || isnan(eff) || isinf(eff))
				eff = 5e4;

			const double base_chance = 1 / (chances[drop / 2].first + std::numeric_limits<double>::epsilon() * 16);

			for (size_t i = 0; i < chances.size(); i++)
			{
				auto normalized = pow(chances[i].first * base_chance, eff);
				
				if (kmAlmostEqual(normalized, 0))
				{
					chances[i].first = 1;
					sumOfAllChances += chances[i].first;
					while(++i < chances.size())
						chances[i].first = 0;
					break;
				}
				if (isinf(normalized))
				{
					chances[i].first = 0;
					while (++i < chances.size())
						chances[i].first = 0;
					break;
				}
				normalized = 1024 / normalized;
				if (isnan(normalized) || isinf(normalized))
					__debugbreak();
				chances[i].first = normalized;
				sumOfAllChances += chances[i].first;
			}
			sumOfAllChances *= 1 - (0.1 / target_eff);

			double selectionAccumeChance = double(std::uniform_real_distribution<double>(0, sumOfAllChances)(re[threadId]));

			for (size_t l = 0; l < std::size(chance); l++)
			{
				//for debugging purpose
				double newVal = chances[l].first / sumOfAllChances;

				if (isnan(newVal))
					__debugbreak();

				if (isnan(chance[l].fetch_add(newVal)))
					__debugbreak();
			}

			for (selection = 0; chances[selection].first < selectionAccumeChance; selection++)
				selectionAccumeChance -= chances[selection].first;
		}
		else
			selection = 0;
		/*if (x < 5 && y < 5 && selection == 0)
			__debugbreak();*/
		assert(selection < chances.size());
		newShared[task_id] = chances[selection].second;
	});

	if (groupSize == 0)
	{
		std::cout <<
			"target_eff = " << target_eff << " chance[avg0, avg1, avg2] = " << 100 << ", " << 0 << " ," << 0 << "%\n";
	}
	else
	{
		std::cout <<
			"target_eff = " << target_eff << " chance[avg0, avg1, avg2] = " << 100 * chance[0] / groupSize << ", " << 100 * chance[1] / groupSize << " ," << 100 * chance[2] / groupSize << "%\n";
	}
	sequal_for_each(width * height, [&](size_t task_id, size_t threadId) {
		shared[task_id] = newShared[task_id];
	}, nullptr);
}
#pragma optimize("", on)
void Dither::compressPalette(size_t compressedPaletteSize) {

	std::vector<size_t> useCount(palettes[0].size(), 0);

	for (auto &selection : shared)
		useCount[selection] ++;

	std::vector<size_t> usedPaletteColors, reverseUsedPaletteColors;

	usedPaletteColors.reserve(useCount.size());
	reverseUsedPaletteColors.resize(useCount.size(), -1);
	for (size_t i = 0; i < useCount.size(); i++)
		if (useCount[i])
		{
			reverseUsedPaletteColors[i] = usedPaletteColors.size();
			usedPaletteColors.push_back(i);
		}

	if (usedPaletteColors.size() < compressedPaletteSize)
		return;

	size_t nrows = usedPaletteColors.size();
	size_t ncolomns = palettes.size() * 3;

	std::vector<std::vector<kmScalar> > dataSrc(nrows);
	std::vector<kmScalar *> data(nrows);

	for (size_t i = 0; i < nrows; i++)
	{
		dataSrc[i].resize(ncolomns);
		for (size_t j = 0; j < palettes.size(); j++)
		{
			dataSrc[i][j * 3 + 0] = palettes[j][i].r;
			dataSrc[i][j * 3 + 1] = palettes[j][i].g;
			dataSrc[i][j * 3 + 2] = palettes[j][i].b;
		}
		data[i] = dataSrc[i].data();
	}


	
	std::vector<int> maskTemplate(ncolomns, 1);
	std::vector<int*> mask(nrows, maskTemplate.data());

	std::vector<kmScalar> clusterWeights(ncolomns, 1);

	std::vector<int> clusterIds(usedPaletteColors.size());

	kmScalar error;
	int found;

	kcluster(compressedPaletteSize, nrows, ncolomns, data.data(), mask.data(), clusterWeights.data(), 0/*transpose*/, 50/*iterations*/, 'a', 'e', clusterIds.data(), &error, &found);


	if (found)
	{
		std::map<size_t, std::vector<size_t>> newIds;

		for (size_t i = 0; i < clusterIds.size(); i++)
		{
			newIds[clusterIds[i]].push_back(i);
		}
		
		for (auto &newId :newIds)
			std::sort(newId.second.rbegin(), newId.second.rend(), [&useCount, &usedPaletteColors](size_t a, size_t b) {
				return useCount[usedPaletteColors[a]] < useCount[usedPaletteColors[b]];
			});

		for (size_t i = 0; i < clusterIds.size(); i++)
		{
			clusterIds[i] = newIds[clusterIds[i]][0];
		}
		
		for (size_t i = 0; i < clusterIds.size(); i++)
			if (i == clusterIds[i])
				for (size_t j = 0; j < palettes.size(); j++)
					palettes[j][usedPaletteColors[i]] *= useCount[usedPaletteColors[i]];

		for (size_t i = 0; i < clusterIds.size(); i++)
		{
			if (i != clusterIds[i])
			{
				for (size_t j = 0; j < palettes.size(); j++)
					palettes[j][usedPaletteColors[clusterIds[i]]] += palettes[j][usedPaletteColors[i]] * useCount[usedPaletteColors[i]];
				useCount[usedPaletteColors[clusterIds[i]]] += useCount[usedPaletteColors[i]];
			}
		}

		for (size_t i = 0; i < clusterIds.size(); i++)
		{
			if (i == clusterIds[i])
				for (size_t j = 0; j < palettes.size(); j++)
				{
					palettes[j][usedPaletteColors[i]] /= useCount[usedPaletteColors[i]];
					assert(palettes[j][usedPaletteColors[i]].r >= -1 && palettes[j][usedPaletteColors[i]].r <= 101);
				}
		}

		//std::cout << "--------\n";
		/*for (size_t i = 0; i < reveerseUsedPaletteColors.size(); i++)
		{
			char buffer[1024];
			char* bf = buffer;
			for (size_t j=0; j < palettes.size(); j++)
				bf += snprintf(bf, size(buffer) + buffer - bf, "%7.2f ", palettes[j][i].r);
			bf += snprintf(bf, size(buffer) + buffer - bf, "%d\n", clusterIds[i]);
			std::cout << buffer;
		}*/

		std::cout << "compressed palette to " << compressedPaletteSize << " with error: " << error << "\n";

		for (size_t i = 0; i < shared.size(); i++)
			shared[i] = usedPaletteColors[clusterIds[reverseUsedPaletteColors[shared[i]]]];
	}
	else
	{
		std::cout << "compression failed\n";
	}
}

#pragma optimize("", on)

void Dither::render(Visualizer & viz, int displayRows, int displayCols, const std::vector<Image>& rawImgs, int frameNum, bool drawJoint)
{
	Image temp, pTemp;
	Image paletteImg, jointImg;

	auto shader = GPUProgramCache::getInstance()->getProgram("palette");

	shader->setUniform(shader->getUniformID("uFrameCount"), static_cast<float>(imgs.size()));
	shader->setUniform(shader->getUniformID("uPaletteSize"), static_cast<float>(palettes[0].size()));
	shader->setUniform(shader->getUniformID("uShared"), 0);
	shader->setUniform(shader->getUniformID("uPalette"), 1);

	kmMat4 projection;
	kmMat4OrthographicProjection(&projection, 0, width, 0, height, -1, 1);
	shader->setUniform(shader->getUniformID(Uniform_ProjectionView_Name), projection);

	temp.alloc(rawImgs[0].width(), rawImgs[0].height(), Image::Format::RGBA);
	pTemp.alloc(palettes[0].size(), 1, Image::Format::RGB);
	int row = displayRows - 1;
	int col = 0;
	kmScalar colWidth = rawImgs[0].width() + 20;
	kmScalar rowHeight = rawImgs[0].height() * 2 + 40;

	std::vector<size_t> useCount(palettes[0].size(), 0);
	for (auto &selection : shared)
		useCount[selection] ++;

	paletteImg.alloc(palettes[0].size(), palettes.size(), Image::Format::RGBA);
	for (size_t i = 0; i < palettes.size(); i++)
	{
		for (size_t x = 0; x < palettes[0].size(); x++)
		{
			ColorSpace::Lab lab(palettes[i][x].r, palettes[i][x].g, palettes[i][x].b);
			ColorSpace::Rgb rgb;
			lab.ToRgb(&rgb);
			rgb.r = std::clamp<kmScalar>(rgb.r, 0, 255);
			rgb.g = std::clamp<kmScalar>(rgb.g, 0, 255);
			rgb.b = std::clamp<kmScalar>(rgb.b, 0, 255);
			Color4B c(rgb.r, rgb.g, rgb.b);
			c.a = useCount[x] > 0 ? 255 : 0;
			paletteImg.putPixel(x, i, c);
		}
	}

	Texture2D *paletteTexture = new Texture2D();
	Texture2D *sharedTexture = new Texture2D();
	paletteTexture->bufferData(GL_RGBA, paletteImg.width(), paletteImg.height(), paletteImg.data());
	sharedTexture->bufferData(GL_RGBA, width, height, shared.data());
	sharedTexture->setScaleFilter(GL_NEAREST, GL_NEAREST);
	//RenderTexture *rt = new RenderTexture(width, height, 1, false, false);

	if (drawJoint)
		jointImg.alloc(colWidth * displayCols, rowHeight * displayRows, Image::Format::RGBA);
	for (size_t i = 0; i < rawImgs.size(); i++, col++)
	{
		if (col == displayCols)
		{
			row--;
			col = 0;
		}

		size_t xOff = colWidth * col;
		size_t yOff = rowHeight * row + rawImgs[i].height() + 20;

		viz.setTranslate(Vec2(xOff, yOff));
		if (drawJoint)
		{
			sequal_for_each(width * height, [xOff, yOff, i, &jointImg, &rawImgs, this](size_t task_id, size_t thread_id) {
				size_t x = task_id % width;
				size_t y = task_id / width;
				jointImg.putPixel(x + xOff, y + yOff, rawImgs[i].getPixel(x, y));
			}, nullptr);
		}

		viz.useDefaultShaders(true);
		viz.renderImage(rawImgs[i]);


		yOff = rowHeight * row;

		if (drawJoint)
		{
			sequal_for_each(width * height, [xOff, yOff, i, &temp, &jointImg, &paletteImg, this](size_t task_id, size_t thread_id) {
				size_t x = task_id % width;
				size_t y = task_id / width;
				Color4B c = paletteImg.getPixel(shared[task_id], i);
				temp.putPixel(x, y, c);
				jointImg.putPixel(x + xOff, y + yOff, c);
			}, nullptr);
		}
		viz.useDefaultShaders(false);
		struct {
			Vec2 pos;
			Vec2 tex;
		}rect[] = {
			{ { 0,          0 },{ 0, 0 } },
		{ { 0,     static_cast<kmScalar>(height) },{ 0, 1 } },
		{ { static_cast<kmScalar>(width),      0 },{ 1, 0 } },
		{ { static_cast<kmScalar>(width), static_cast<kmScalar>(height) },{ 1, 1 } }
		};
		uint16_t indices[] = { 0, 1, 2, 2, 1, 3 };
		shader->use();
		shader->setUniform(shader->getUniformID("uFrame"), static_cast<float>(i));
		GLGlobals::getInstance()->bindTexture(sharedTexture, 0);
		GLGlobals::getInstance()->bindTexture(paletteTexture, 1);
		viz.bufferData(rect, 4);
		viz.setTranslate(Vec2(xOff, yOff));
		viz.renderIndexTriangles(&std::remove_reference<decltype(*rect)>::type::pos, &std::remove_reference<decltype(*rect)>::type::tex, indices, 6);
	}
	glClearColor(0, 0, 0, 0);
	viz.display();

	char pathPalette[1024];
	char pathShared[1024];
	char pathJoint[1024];
	sprintf_s(pathPalette, "%s/%03d-n%d-palette.png", outDir.c_str(), frameNum, neighbor_count);
	sprintf_s(pathShared, "%s/%03d-n%d-shared.png", outDir.c_str(), frameNum, neighbor_count);
	sprintf_s(pathJoint, "%s/%03d-n%d-joint.png", outDir.c_str(), frameNum, neighbor_count);

	if (!drawJoint)
		jointImg = viz.screenShot();

	std::thread saveJointThread([&jointImg, &pathJoint]() {
		
	});
	jointImg.save(pathJoint);

	std::thread savePalleteThread([&paletteImg, &pathPalette]() {
		
	});
	paletteImg.save(pathPalette);

	temp.alloc(width, height, Image::Format::RGB);

	sequal_for_each(width * height, [&temp, this](size_t task_id, size_t thread_id) {

		size_t x = task_id % width;
		size_t y = task_id / width;
		size_t selection = shared[task_id];

		Color4B clusterId;
		clusterId.value =
			((selection * 127) & 0xffffff) +
			((selection * 256 * 79) & 0xffffff) +
			((selection * 256 * 256 * 23) & 0xffffff);

		clusterId.a = 255;

		temp.putPixel(x, y, clusterId);
	}, nullptr);


	temp.save(pathShared);

	saveJointThread.join();
	savePalleteThread.join();
}

#pragma region bilateral

static const float sig_d = 100, sig_r = 500;
static constexpr auto kernelCutoff = 0.15;
static const int kernelSize = (std::max<int>(sqrt(-log(kernelCutoff) * sig_d) , 2)) | 1;

static const std::vector<kmScalar> fkernel = []() {
	std::vector<kmScalar> kernel(kernelSize * kernelSize, 0);
	for (int i = 0; i < kernelSize; i++)
		for (int j = 0; j < kernelSize; j++)
		{
			kmScalar dx = j - kernelSize / 2;
			kmScalar dy = i - kernelSize / 2;
			kernel[i * kernelSize + j] = exp(-(dx * dx + dy * dy) / sig_d);
		}
	return kernel;
}();

static void bilateral(std::vector<Color4F>& src, size_t width, size_t height, std::vector<Color4F> &ret)
{
	ret.resize(width * height);

	sequal_for_each(width * height,
		[&](size_t idx, size_t tid)
	{
		size_t x = idx % width;
		size_t y = idx / width;

		Color4F sum(0, 0, 0);
		float sum_w = 0;
		const auto &srcPix = src[idx];

		for (int i = 0; i < kernelSize; i++)
			for (int j = 0; j < kernelSize; j++)
			{
				const int dx = j - kernelSize / 2;
				const int dy = i - kernelSize / 2;

				float w = fkernel[i * kernelSize + j];

				if (static_cast<size_t>(dx + x) < width && static_cast<size_t>(dy + y) < height)
				{

					size_t neiId = (dy + y) * width + (dx + x);
					auto &neiPix = src[neiId];
					float dl = srcPix.r - neiPix.r;
					float da = srcPix.g - neiPix.g;
					float db = srcPix.b - neiPix.b;

					double colorDiff = dl * dl + da * da + db * db;
					w *= std::expl(-colorDiff / sig_r);

					sum.r += neiPix.r * w;
					sum.g += neiPix.g * w;
					sum.b += neiPix.b * w;
					sum_w += w;
				}
			}

		//cout << kernel[(kernelSize / 2) * kernelSize + kernelSize / 2] / sum_w << endl;

		sum.r /= sum_w;
		sum.g /= sum_w;
		sum.b /= sum_w;
		sum.a = 1;

		ret[idx] = sum;
	}, nullptr);
}
#pragma endregion

template <>
void make_zero(Color4F& v)
{
	v.r = v.g = v.b = v.a = 0;
}

Dither::ErrorMetrics Dither::computeTotalError(bool calcBilateral, bool calcGaussian)
{
	Dither::ErrorMetrics err;

	err.bilateralError = -1;
	err.gaussianError = -1;

	for (size_t i = 0; i < imgs.size(); i++)
	{
		std::vector<Color4F> ref = imgs[i];
		std::vector<Color4F> res(imgs[i].size());
		for (size_t j = 0; j < imgs[0].size(); j++)
			res[j] = palettes[i][shared[j]];
		if (calcBilateral)
		{
			std::vector<Color4F> bires;
			std::vector<Color4F>& biref = birefCache[i];
			if (biref.size() == 0)
				bilateral(ref, width, height, biref);
			bilateral(res, width, height, bires);
			for (size_t i = 0; i < imgs[0].size(); i++)
			{
				err.bilateralError += (1 - labSimilarity(biref[i], bires[i])) / 2;
			}
		}
		if (calcGaussian)
		{
			std::vector<Color4F> gaussres;
			std::vector<Color4F>& gaussref = gaussCache[i];
			if (gaussref.size() == 0)
			{
				auto inter = runGausian(Dimention::X, 2, width, height, ref);
				gaussref = runGausian(Dimention::Y, 2, width, height, inter);
			}
			auto inter = runGausian(Dimention::X, 2, width, height, res);
			gaussres = runGausian(Dimention::Y, 2, width, height, inter);
			for (size_t i = 0; i < imgs[0].size(); i++)
			{
				err.gaussianError += (1 - labSimilarity(gaussref[i], gaussres[i])) / 2;
			}
		}
	}

	return err;
}

//#pragma optimize("", off)
bool Dither::skip(int frameNum) {
	char path[1024];
	sprintf_s(path, "%s/%03d-n%d-palette.png", outDir.c_str(), frameNum, neighbor_count);
	Image tempPalette;
	if (!tempPalette.load(path))
		return false;
	if (tempPalette.width() != palettes[0].size())
		return false;
	if (tempPalette.height() != palettes.size())
		return false;
	sprintf_s(path, "%s/%03d-n%d-shared.png", outDir.c_str(), frameNum, neighbor_count);
	std::cout << path << std::endl;
	Image tempShared;
	if (!tempShared.load(path))
		return false;
	if (tempShared.width() != width || tempShared.height() != height)
		return false;

	std::map<uint32_t, uint32_t> hashes;
	for (size_t i = 0; i < palettes[0].size(); i++)
	{
		uint32_t hash =
			((i * 127) & 0xffffff) +
			((i * 256 * 79) & 0xffffff) +
			((i * 256 * 256 * 23) & 0xffffff);
		hash &= 0xffffff;
		hashes[hash] = i;
	}

	for (size_t y = 0; y < tempShared.height(); y++)
	{
		for (size_t x = 0; x < tempShared.width(); x++)
		{

			Color4B selection = tempShared.getPixel(x, y);
			selection.a = 0;
			if (hashes.find(selection.value) == hashes.end())
				return false;
			else
			{
				shared[x + y * width] = hashes[selection.value];
			}
		}
	}

	for (size_t i = 0; i < palettes.size(); i++)
	{
		for (size_t x = 0; x < palettes[0].size(); x++)
		{
			Color4B c = tempPalette.getPixel(x, i);
			c.a = 255;
			ColorSpace::Rgb rgb;
			rgb.r = c.r;
			rgb.g = c.g;
			rgb.b = c.b;
			ColorSpace::Lab lab;
			rgb.To(&lab);
			palettes[i][x] = Color4F(lab.l, lab.a, lab.b);
		}
	}

	return true;
}
//#pragma optimize("", off)
void Dither::sortPalette()
{
	printf("sort-begin\n");
	std::vector<size_t> order(palettes[0].size());
	for (size_t i = 0; i < order.size(); i++)
		order[i] = i;
	/*std::vector<kmScalar> var(order.size(), 0);
	std::vector<Color4F> avg(order.size(), Color4F::Black);*/
	std::vector<std::vector<Color4F>> hsv(order.size());
	std::vector<std::vector<int>> clusterIds(order.size());
	/*for (size_t i = 0; i < palettes.size(); i++)
		for (size_t j = 0; j < order.size(); j++)
			avg[j] += palettes[i][j];
	for (size_t i = 0; i < order.size(); i++)
		avg[i] /= palettes.size();

	for (size_t i = 0; i < palettes.size(); i++)
		for (size_t j = 0; j < order.size(); j++)
		{
			Vec3 diff(palettes[i][j].r - avg[j].r, palettes[i][j].g - avg[j].g, palettes[i][j].b - avg[j].b);
			var[j] += diff.lengthsq();
		}*/


	sequal_for_each(order.size(), [&](size_t i, size_t threadId)
	{
		auto &pclusterIds = clusterIds[i];
		pclusterIds.resize(palettes.size());

		size_t nrows = pclusterIds.size();
		size_t ncolomns = 3;

		std::vector<kmScalar > dataSrc(nrows * 3);
		std::vector<kmScalar *> data(nrows);

		for (size_t j = 0; j < palettes.size(); j++)
		{
			dataSrc[j * 3 + 0] = palettes[j][i].r;
			dataSrc[j * 3 + 1] = palettes[j][i].g;
			dataSrc[j * 3 + 2] = palettes[j][i].b;
			data[j] = &dataSrc[j * 3];
		}
	


		std::vector<int> maskTemplate(ncolomns, 1);
		std::vector<int*> mask(nrows, maskTemplate.data());

		std::vector<kmScalar> clusterWeights(ncolomns, 1);

		kmScalar error;
		int found;
		size_t clusterCount = 1;
		while (true) {
			kcluster(clusterCount, nrows, ncolomns, data.data(), mask.data(), clusterWeights.data(), 0/*transpose*/, 4/*iterations*/, 'a', 'e', pclusterIds.data(), &error, &found);

			if (found <= 0)
				return;

			std::vector<Color4F> means(clusterCount, Color4F(0,0,0,0));
			for (size_t j = 0; j < pclusterIds.size(); j++)
			{
				means[pclusterIds[j]] += palettes[j][i];
				means[pclusterIds[j]].a += 1;
			}
			for (auto& m : means)
				m /= m.a;
			kmScalar max_dist = 0;
			for (size_t j = 0; j < pclusterIds.size(); j++)
			{
				auto dist = (palettes[j][i] - means[pclusterIds[j]]).length();
				if (dist > max_dist)
					max_dist = dist;
			}
			if (max_dist > 10 && clusterCount * 2 < palettes.size() * 3)
				clusterCount++;
			else
				break;
		}
		hsv[i].resize(clusterCount);

			
		std::vector<size_t> reindex(hsv[i].size()),reindex_t(hsv[i].size());
		for (size_t j = palettes.size(); j > 0; j--)
			reindex[pclusterIds[j-1]] = j-1;
		std::sort(reindex.begin(), reindex.end());
		for (size_t j = 0; j < reindex.size(); j++)
			reindex_t[pclusterIds[reindex[j]]] = j;
		for (size_t j = 0; j < pclusterIds.size(); j++)
			pclusterIds[j] = reindex_t[pclusterIds[j]];

		std::vector<Color4F> columnAvg(hsv[i].size(), Color4F(0,0,0,0));
		for (size_t j = 0; j < palettes.size(); j++)
		{
			columnAvg[pclusterIds[j]] += palettes[j][i];
			columnAvg[pclusterIds[j]].a += 1;
		}

		for (size_t j = 0; j < hsv[i].size(); j++)
		{
			columnAvg[j] /= columnAvg[j].a;
			ColorSpace::Lab lab(columnAvg[j].r, columnAvg[j].g, columnAvg[j].b);
			ColorSpace::Hsv hsv_1;
			lab.To(&hsv_1);
			hsv[i][j].r = floor(hsv_1.h / 36);
			hsv[i][j].g = hsv_1.s;
			hsv[i][j].b = floor(hsv_1.v * 2);
		}
	});

	std::sort(order.begin(), order.end(), [&](size_t l, size_t r) {
		if (hsv[l].size() < hsv[r].size())
			return true;
		if (hsv[l].size() > hsv[r].size())
			return false;

		for (size_t i = 0; i < palettes.size(); i++)
		{
			if (hsv[l][clusterIds[l][i]].b < hsv[r][clusterIds[r][i]].b)
				return true;
			if (hsv[l][clusterIds[l][i]].b > hsv[r][clusterIds[r][i]].b)
				return false;

			if (hsv[l][clusterIds[l][i]].r < hsv[r][clusterIds[l][i]].r)
				return true;
			if (hsv[l][clusterIds[l][i]].r > hsv[r][clusterIds[l][i]].r)
				return false;
		}
		return false;
	});

	auto order_backup = order;
	for (size_t i = 0; i < order.size(); i++)
		order[order_backup[i]] = i;

	for (auto& p : palettes)
	{
		reorder(order.begin(), order.end(), p.begin());
	}

	for (size_t i = 0; i < shared.size(); i++)
		shared[i] = order[shared[i]];
	printf("sort-end\n");
}
