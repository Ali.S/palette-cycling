#include "Camera.h"

Camera* Camera::_activeCamera = nullptr;

Camera::Camera()
{
	setOrthogonal(1,1e-2, 1e2);
	kmVec3 eye;
	eye.x = 0; eye.y = 0; eye.z = 1;
	kmVec3 center;
	center.x = 0; center.y = 0; center.z = 0;
	kmVec3 up;
	up.x = 0; up.y = 1; up.z = 0;
	setVeiw(eye,center,up);
	kmMat4Multiply(&_projectionVeiwMatrix,&_projectionMatrix, &_veiwMatrix);
}

Camera::~Camera()
{
	if (_activeCamera == this)
		_activeCamera = nullptr;
}

void Camera::setOrthogonal(float aspect, float near, float far)
{
	kmMat4OrthographicProjection(&_projectionMatrix, -aspect, aspect, -1, 1, near, far);
}

void Camera::setVeiw(const kmVec3& eye, const kmVec3& center, const kmVec3& up)
{
	kmMat4LookAt(&_veiwMatrix, &eye, &center, &up);
}

void Camera::setPerspective(float fovY, float aspect, float near, float far)
{
	kmMat4PerspectiveProjection(&_projectionMatrix, fovY, aspect, near, far);	
}

const kmMat4& Camera::getProjectionViewMatrix()
{
	Camera* temp = _activeCamera;
	_activeCamera = nullptr;
	recalculateWorldTransformationMatrix(true);
	_activeCamera = temp;
	
	kmMat4Inverse(&_projectionVeiwMatrix, &getWorldTransformationMatrix());
	
	kmMat4Multiply(&_projectionVeiwMatrix, &_veiwMatrix, &_projectionVeiwMatrix);
	kmMat4Multiply(&_projectionVeiwMatrix, &_projectionMatrix, &_projectionVeiwMatrix);
	return _projectionVeiwMatrix;
}

const kmMat4 Camera::getViewMatrix()
{
	Camera* temp = _activeCamera;
	_activeCamera = nullptr;
	recalculateWorldTransformationMatrix(true);
	_activeCamera = temp;
	kmMat4 res;
	
	kmMat4Inverse(&res, &getWorldTransformationMatrix());
	
	kmMat4Multiply(&res, &_veiwMatrix, &res);	
	return res;
}

const kmMat4& Camera::getProjectionMatrix()
{
	return _projectionMatrix;
}

void Camera::setAsActive()
{
	_activeCamera = this;
}

Camera* Camera::getActiveCamera()
{
	return _activeCamera;
}