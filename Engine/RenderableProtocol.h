#pragma once

#include "Macros.h"
#include "PositionProtocol.h"

class RenderableProtocol : public PositionProtocol
{
	
public:
	RenderableProtocol();
	virtual void preRender();
	virtual void render() = 0;
	virtual void postRender() = 0;
	PROPERTY(bool, _visible, Visible);
};

