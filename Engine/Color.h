#pragma once
#define Log printf
#include <cstdint>
#include <kazmath.h>

struct Color4B;
struct Color4F;

struct Color4B
{
	Color4B() = default;
	Color4B(const Color4F &ref);
	Color4B(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);

	uint8_t avg() const;

	kmScalar similarity(const Color4B &ref) const;

	union {
		uint32_t value;
		struct {
			uint8_t r, g, b, a;
		};
	};

	static const Color4B Black;
	static const Color4B White;
	static const Color4B Red;
	static const Color4B Blue;
	static const Color4B Green;
};

struct Color4F
{
	Color4F() = default;
	Color4F(const Color4B &ref);
	Color4F(kmScalar r, kmScalar g, kmScalar b, kmScalar a = 1.0f);

	Color4F &operator += (const Color4F& o);
	Color4F &operator -= (const Color4F& o);
	Color4F operator - (const Color4F& o) const;
	Color4F operator + (const Color4F& o) const;
	Color4F &operator *= (const kmScalar& o);
	Color4F operator * (const kmScalar& o) const;
	Color4F &operator /= (const kmScalar& o);
	Color4F operator / (const kmScalar& o) const;
	kmScalar length2() const;
	kmScalar length() const;
	kmScalar avg() const;
	kmScalar similarity(const Color4F &ref) const;

	union
	{
		struct { kmScalar r, g, b, a; };
		kmScalar channel[4];
	};

	static const Color4F Black;
	static const Color4F White;
	static const Color4F Red;
	static const Color4F Blue;
	static const Color4F Green;
};
