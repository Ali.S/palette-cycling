#include "CommonHeaders.h"
#include "Mesh.h"
#include "GPUProgramCache.h"
#include "GPURenderProgram.h"
#include "Texture2D.h"
#include "Camera.h"
#include "GLGlobals.h"
#include <fstream>
#include <tuple>
#include <string>
#include <cstring>
#include <sstream>

using namespace std;

Mesh::Mesh() : _shader(nullptr), _texture(nullptr), _VBOID(nullptr), _usePerVertexColor(false), _autoSelectShader(true), _hasNormals(false)
{
	_uniformColor.inUse = false;
	setRenderProgram(GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION));
}

Mesh::Mesh(const Mesh& ref) : _shader(ref._shader), _texture(ref._texture), _VBOID(ref._VBOID), 
	_usePerVertexColor(ref._usePerVertexColor), _uniformIDs(ref._uniformIDs), _useIndices(ref._useIndices),
	_autoSelectShader(ref._autoSelectShader),
	_hasNormals(ref._hasNormals)
{
	if (_shader)
		_shader->retain();
	if(_texture)
		_texture->retain();
}

Mesh::Mesh(const VertexInfo* vertices, int vertexCount) : _shader(nullptr), _texture(nullptr), _VBOID(nullptr), _usePerVertexColor(false), _autoSelectShader(true), _hasNormals(false)
{
	_uniformColor.inUse = false;
	setRenderProgram(GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION));
	setData(vertices,vertexCount);	
}

Mesh::Mesh(const VertexInfo* vertices, int vertexCount, const short* indices, int indexCount) : _shader(nullptr), _texture(nullptr), _VBOID(nullptr), _usePerVertexColor(false), _autoSelectShader(true), _hasNormals(false)
{
	_uniformColor.inUse = false;
	setRenderProgram(GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_TEXTURE));
	setData(vertices,vertexCount,indices,indexCount);
}

Mesh::~Mesh()
{
	if(_texture)
		_texture->release();
	if (_shader)
		_shader->release();
	if(_VBOID.use_count() == 1)
		glDeleteBuffers(2, reinterpret_cast<GLuint*>(_VBOID.get()));
}

void Mesh::render()
{
	if(!_VBOID || !_shader || !getVisible())
		return;
	_shader->use();
	_shader->setUniform(_uniformIDs.MMatrix,getWorldTransformationMatrix());
	
	if(_uniformColor.inUse)
		_shader->setUniform(_uniformIDs.color, _uniformColor.r, _uniformColor.g, _uniformColor.b,_uniformColor.a);
	GLGlobals::getInstance()->setArrayBuffer(_VBOID->vertices);
	if(_useIndices)
		GLGlobals::getInstance()->setElementArrayBuffer(_VBOID->indices);

	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, _usePerVertexColor);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, _texture != nullptr);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Normal, _hasNormals);
	GLGlobals::getInstance()->applyVertexAttributeChanges();
	glVertexAttribPointer(VertexAttribute_Position, 3, GL_FLOAT, false, _vertexAttributeStride, _vertexAttributeOffsets._positionOffset);
	if (_hasNormals)
		glVertexAttribPointer(VertexAttribute_Normal, 3, GL_FLOAT, true, _vertexAttributeStride, _vertexAttributeOffsets._normalOffset);
	if (_usePerVertexColor)
		glVertexAttribPointer(VertexAttribute_Color, 3, GL_FLOAT, false, _vertexAttributeStride, _vertexAttributeOffsets._colorOffset);
	if(_texture != nullptr)
			glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, false, _vertexAttributeStride, _vertexAttributeOffsets._texCoordOffset);
	if (_texture != nullptr)
		GLGlobals::getInstance()->bindTexture(_texture);
	if(_useIndices)
		glDrawElements(GL_TRIANGLES, _indicesCount, GL_UNSIGNED_SHORT, 0);
	else
		glDrawArrays(GL_TRIANGLES, 0,_indicesCount);
}

void Mesh::postRender()
{

}

GLuint Mesh::getArrayBufferID()
{
	if (_VBOID)
		return _VBOID->vertices;
	else
		return 0;
}

GLuint Mesh::getElementArrayBufferID()
{
	if (_VBOID)
		return _VBOID->indices;
	else
		return 0;
}

void Mesh::load(const char* path)
{
	size_t len = strlen(path);
	for(--len;len>=0;--len)
		if(path[len] =='.')
			break;
	if(strcmp(path+len,".obj") == 0)
		objFileLoader(path);
}

void Mesh::objFileLoader(const char* path)
{
	ifstream inFile(path);
	string line;
	vector<tuple<float,float,float>> positions;
	vector<tuple<float,float>> textureCoord;
	vector<tuple<float,float,float>> normals;
	vector<vector<tuple<int,int,int>>> faces;
	map<int,vector<int>> smooth_group;
	vector<VertexInfo> vertices;
	map<tuple<int,int,int>, int> mergedVertices;
	vector<short> indices;
	int active_smooth_group = 0;
	if (path)
	{
		for(getline(inFile, line); !inFile.eof(); getline(inFile,line))
		{
			stringstream lineStream(line);
			string entryType;
			lineStream >> entryType;
			if(entryType == "v")
			{
				float x,y,z;
				lineStream>>x>>y>>z;
				positions.push_back(make_tuple(x,y,z));
			}
			if(entryType == "vn")
			{
				float x,y,z;
				lineStream>>x>>y>>z;
				normals.push_back(make_tuple(x,y,z));
			}
			if(entryType == "vt")
			{
				float u,v;
				lineStream>>u>>v;
				textureCoord.push_back(make_tuple(u,v));
			}
			if(entryType == "s")
			{
				int id = 0;
				lineStream >> id;
				active_smooth_group = id;
			}
			if(entryType == "f")
			{
				string group;
				int index[3];
				int vID = 0;
				smooth_group[active_smooth_group].push_back(faces.size());
				faces.push_back(vector<tuple<int,int,int>>());				
				for(lineStream>>group;lineStream;lineStream>>group,vID++)
				{
					if(vID == 3)
					{
						index[1] = index[2];
						vID --;
					}
					int attribs[3] = {0,0,0};
					int fID = 0;
					for(unsigned i = 0 ;i < group.size();i++)
						switch (group[i])
					{
						case '0': case '1': case '2':case '3': case '4': case '5':case '6': case '7': case '8':case '9':
							attribs[fID] = attribs[fID] * 10 + group[i] - '0';
							break;
						case '/':
							fID++;
							break;
						default : return;
					}
					auto t = make_tuple(attribs[0],attribs[1],attribs[2]);					
					faces.back().push_back(t);					
				}
			}
		}		
		for(auto &group:smooth_group)
		{
			vector<int> cnt(positions.size(),0);
			vector<float> sx(positions.size(),0),sy(positions.size(),0),sz(positions.size(),0);
			if(group.first != 0)
			{
			
				float nx,ny,nz,d;
				for(auto& i:group.second)
					for(unsigned j=0;j<faces[i].size();j++)
					{
						cnt[get<0>(faces[i][j]) - 1] ++;
#define pos(j,a) get<a>(positions[get<0>(faces[i][(j+faces[i].size()) % faces[i].size()]) - 1])
#define dif(x,y,a) (pos(y,a) - pos(x,a))
						float dx1,dx2,dy1,dy2,dz1,dz2;
						dx1 = dif(j,j+1,0);
						dx2 = dif(j,j-1,0);
						dy1 = dif(j,j+1,1);
						dy2 = dif(j,j-1,1);
						dz1 = dif(j,j+1,2);
						dz2 = dif(j,j-1,2);
						
						nx = dy1 * dz2 - dz1 * dy2;
						ny = dz1 * dx2 - dx1 * dz2;
						nz = dx1 * dy2 - dy1 * dx2;
						d = sqrt(nx * nx + ny * ny + nz * nz);
						sx[get<0>(faces[i][j]) - 1] += nx / d;
						sy[get<0>(faces[i][j]) - 1] += ny / d;
						sz[get<0>(faces[i][j]) - 1] += nz / d;
#undef pos
#undef dif
					}
			
				for(unsigned i=0;i<positions.size();i++)
				{
					sx[i] /= cnt[i];
					sy[i] /= cnt[i];
					sz[i] /= cnt[i];				
				}			
			}
			for(auto& i:group.second)
				for(unsigned j=0;j<faces[i].size();j++)
				{
					if (group.first != 0 && get<2>(faces[i][j]) == 0)
						get<2>(faces[i][j]) = -group.first;
					auto id = mergedVertices.find(faces[i][j]);
					int attribs[3],index[3];
					if(j > 2)
					{
						index[1] = index[2];
					}
					attribs[0] = get<0>(faces[i][j]);
					attribs[1] = get<1>(faces[i][j]);
					attribs[2] = get<2>(faces[i][j]);
					
					if(id == mergedVertices.end())
					{
						VertexInfo val;
						if(attribs[0] > 0)
						{
							val.x = get<0>(positions[attribs[0] - 1]);
							val.y = get<1>(positions[attribs[0] - 1]);
							val.z = get<2>(positions[attribs[0] - 1]);
						}
						if(attribs[1] > 0)
						{
							val.u = get<0>(textureCoord[attribs[1] - 1]);
							val.v = get<1>(textureCoord[attribs[1] - 1]);							
						}
						if(attribs[2] > 0)
						{
							val.nx = get<0>(normals[attribs[2] - 1]);
							val.ny = get<1>(normals[attribs[2] - 1]);
							val.nz = get<2>(normals[attribs[2] - 1]);
						}
						else
							if(attribs[2] < 0)
							{
								val.nx = sx[attribs[0] - 1];
								val.ny = sy[attribs[0] - 1];
								val.nz = sz[attribs[0] - 1];
							}
							else
							{
								val.nx = val.ny = val.nz = 0;
							}
						index [j>2?2:j] = mergedVertices[faces[i][j]] = vertices.size();
						vertices.push_back(val);
					}
					else
						index [j>2?2:j] = id->second;
					if(j >= 2)
					{
						indices.push_back(index[0]);
						indices.push_back(index[1]);
						indices.push_back(index[2]);
					}
				}
		}

		setData(vertices.data(), vertices.size(), indices.data(), indices.size());		
		setHasNormals(true);
	}
}

void Mesh::disposeData()
{
	if(_VBOID.use_count() == 1)
	{
		glDeleteBuffers(2, reinterpret_cast<GLuint*>(_VBOID.get()));
		_VBOID = nullptr;
	}
}

void Mesh::setTexture(Texture* const&texture)
{
	if (_texture == texture)
		return;
	bool usedTexture = _texture != nullptr;
	bool nowTexture = texture != nullptr;
	if (_texture)
		_texture->release();
	_texture = texture;
	if (_texture)
		_texture->retain();
	if (nowTexture != usedTexture && _autoSelectShader)
	{
		int shaderID = (_texture?1:0) | (_usePerVertexColor?2:0) | (_uniformColor.inUse?4:0);
		const char* shaderName;
		switch (shaderID)
		{
			case 0: shaderName = SHADERNAME_POSITION;break;
			case 1: shaderName = SHADERNAME_POSITION_TEXTURE;break;
			case 2: shaderName = SHADERNAME_POSITION_COLOR;break;
			case 3: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR;break;
			case 4: shaderName = SHADERNAME_POSITION_U_COLOR;break;
			case 5: shaderName = SHADERNAME_POSITION_TEXTURE_U_COLOR;break;
			case 6: shaderName = SHADERNAME_POSITION_COLOR_U_COLOR;break;			
			case 7: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR;break;
			default: shaderName = nullptr;
		}
		setRenderProgram(GPUProgramCache::getInstance()->getRenderProgram(shaderName));		
	}	
}

Texture* const & Mesh::getTexture() const
{
	return _texture;
}

void Mesh::setUsePerVertexColor(const bool &status)
{
	if (_usePerVertexColor != status && _autoSelectShader)
	{
		int shaderID = (_texture?1:0) | (status?2:0) | (_uniformColor.inUse?4:0);
		const char* shaderName;
		switch (shaderID)
		{
			case 0: shaderName = SHADERNAME_POSITION;break;
			case 1: shaderName = SHADERNAME_POSITION_TEXTURE;break;
			case 2: shaderName = SHADERNAME_POSITION_COLOR;break;
			case 3: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR;break;
			case 4: shaderName = SHADERNAME_POSITION_U_COLOR;break;
			case 5: shaderName = SHADERNAME_POSITION_TEXTURE_U_COLOR;break;
			case 6: shaderName = SHADERNAME_POSITION_COLOR_U_COLOR;break;			
			case 7: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR;break;
			default: shaderName = nullptr;
		}
		setRenderProgram(GPUProgramCache::getInstance()->getRenderProgram(shaderName));		
	}
	_usePerVertexColor = status;
}

const bool &Mesh::getUsePerVertexColor() const
{
	return _usePerVertexColor;
}

void Mesh::setAutoSelectShader(const bool &value)
{
	_autoSelectShader = value;
}

const bool &Mesh::getAutoSelectShader() const
{
	return _autoSelectShader;
}

void Mesh::setHasNormals(const bool &value)
{
	_hasNormals = value;
}

const bool &Mesh::getHasNormals() const
{
	return _hasNormals ;
}

void Mesh::setUniformColor(float r, float g, float b, float a)
{
	_uniformColor.r = r;
	_uniformColor.g = g;
	_uniformColor.b = b;
	_uniformColor.a = a;
	
	if (_uniformColor.inUse || !_autoSelectShader)
		return;
	_uniformColor.inUse = true;
	
	int shaderID = (_texture?1:0) | (_usePerVertexColor?2:0) | 4;
	const char* shaderName;
	switch (shaderID)
	{
		case 4: shaderName = SHADERNAME_POSITION_U_COLOR;break;
		case 5: shaderName = SHADERNAME_POSITION_TEXTURE_U_COLOR;break;
		case 6: shaderName = SHADERNAME_POSITION_COLOR_U_COLOR;break;			
		case 7: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR;break;
		default: shaderName = nullptr;
	}
	setRenderProgram(GPUProgramCache::getInstance()->getRenderProgram(shaderName));				
}

void Mesh::removeUniformColor()
{
	if (_uniformColor.inUse && _autoSelectShader)
	{		
		int shaderID = (_texture?1:0) | (_usePerVertexColor?2:0);
		const char* shaderName;
		switch (shaderID)
		{
			case 0: shaderName = SHADERNAME_POSITION;break;
			case 1: shaderName = SHADERNAME_POSITION_TEXTURE;break;
			case 2: shaderName = SHADERNAME_POSITION_COLOR;break;
			case 3: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR;break;
			default: shaderName = nullptr;
		}
		setRenderProgram(GPUProgramCache::getInstance()->getRenderProgram(shaderName));		
	}	
	_uniformColor.inUse = false;
}

void Mesh::setRenderProgram(GPURenderProgram* shader)
{
	if (!shader)
		return;
	if (_shader)
		_shader->release();
	_shader = shader;
	_shader->retain();	
	_uniformIDs.MMatrix = _shader->getUniformID(Uniform_Model_Name);
	_uniformIDs.color = _shader->getUniformID(Uniform_Color_Name);	
}

GPURenderProgram* Mesh::getRenderProgram()
{
	return _shader;
}