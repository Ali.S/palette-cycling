#ifndef JSON_H
#define JSON_H

#include <string>
#include <sstream>
#include <vector>
#include <map>

class JSON
{
public:
    enum Type{
		JTYPE_NULL,
        JTYPE_OBJECT,
        JTYPE_ARRAY,
        JTYPE_STRING,
        JTYPE_NUMBER,
		JTYPE_BOOLEAN
    };

	enum ConflictPolicy {
		CP_Mine,
		CP_Theirs
	};

    JSON();
    JSON(const JSON& json);
    JSON(const char* path);
    virtual ~JSON();

    void load(const char* path);
    void save(const char* path) const;
	void save(std::string& output) const;
    void parse(const std::string& text);
	void parse(const char* textBegin, const char* textEnd);
	void merge(const JSON& other, ConflictPolicy policy);
    void clear();

    typedef std::map<std::string, JSON*>::iterator ObjectIterator;
    typedef std::map<std::string, JSON*>::const_iterator ConstObjectIterator;
    typedef std::vector<JSON*>::iterator ArrayIterator;
    typedef std::vector<JSON*>::const_iterator ConstArrayIterator;
    inline ObjectIterator objectBegin();
    inline ConstObjectIterator objectBegin() const;
    inline ObjectIterator objectEnd();
    inline ConstObjectIterator objectEnd() const;
    inline ArrayIterator arrayBegin();
    inline ConstArrayIterator arrayBegin() const;
    inline ArrayIterator arrayEnd();
    inline ConstArrayIterator arrayEnd() const;

    inline Type getType() const;
    inline void setType(const Type type);
	inline size_t size() const;
	inline void resize(unsigned int newSize);

	// array access
	inline JSON& at(unsigned index) { return (*this)[index]; }
	inline const JSON& at(unsigned index) const { return (*this)[index]; }

    inline JSON& operator [](unsigned index);
    inline const JSON& operator [](unsigned index) const;
	// object access
	inline JSON& at(const std::string& key) { return (*this)[key]; }
	inline const JSON& at(const std::string& key) const { return (*this)[key]; }
	
	inline ConstObjectIterator find(const std::string& key) const;
	inline JSON& operator [](const std::string& key);
	inline const JSON& operator [](const std::string& key) const;

	inline JSON& operator =(const JSON& value);
	inline JSON& operator =(const std::string& value);
	inline JSON& operator =(const int& value);
	inline JSON& operator =(const long& value);
	inline JSON& operator =(const long long& value);
	inline JSON& operator =(const unsigned& value);
	inline JSON& operator =(const unsigned long & value);
	inline JSON& operator =(const unsigned long long& value);
	inline JSON& operator =(const float& value);
	inline JSON& operator =(const double& value);
	inline JSON& operator =(const bool& value);
	//inline JSON& operator =(const JSON& json);
    template <class T> T as() const;
	inline const std::string& getValue()const;

protected:
    Type type_;
    std::map<std::string, JSON*> _children;
    std::vector<JSON*> array_;
    std::string value_;
    
    int rec_parse(const char* const text,const char* const end);
    int read_string(const char* const text, std::string& name);
	void rec_write(std::ostream& f, int tab = 0) const;
};

// inline implementation
JSON::ObjectIterator JSON::objectBegin()
{
    return _children.begin();
}

JSON::ConstObjectIterator JSON::objectBegin() const
{
    return _children.begin();
}

JSON::ObjectIterator JSON::objectEnd()
{
    return _children.end();
}

JSON::ConstObjectIterator JSON::objectEnd() const
{
    return _children.end();
}

JSON::ArrayIterator JSON::arrayBegin()
{
    return array_.begin();
}

JSON::ConstArrayIterator JSON::arrayBegin() const
{
    return array_.begin();
}

JSON::ArrayIterator JSON::arrayEnd()
{
    return array_.end();
}

JSON::ConstArrayIterator JSON::arrayEnd() const
{
    return array_.end();
}

JSON::Type JSON::getType() const
{
    return type_;
}

void JSON::setType(const JSON::Type type)
{
    if (type_ != type)
    {
        clear();
        type_ = type;
    }
}

size_t JSON::size() const
{
	switch (type_)
	{
	case JTYPE_ARRAY:
		return array_.size();
	case JTYPE_OBJECT:
		return _children.size();
	case JTYPE_NULL:
		return 0;
	default:
		return 1;
	}
}

void JSON::resize(unsigned newSize)
{
	if (type_ == JTYPE_ARRAY)
	{
		size_t lastSize = array_.size();		 
		for(size_t i=newSize;i<lastSize;i++)
			delete array_[i];
		array_.resize(newSize);		
		for(size_t i=lastSize;i<newSize;i++)
			array_[i] = new JSON();
		
	}
}

JSON& JSON::operator[](unsigned int index)
{
    if (type_ != JSON::JTYPE_ARRAY)
	{
		clear();
		type_ = JSON::JTYPE_ARRAY;
	}
	if (index >= array_.size())
	{
		size_t last_size = array_.size();
		array_.resize(index + 1);
		for(size_t i=last_size;i<array_.size();i++)
			array_[i] = new JSON;
	}
    return *array_[index];
}

const JSON& JSON::operator[](unsigned int index) const
{
    if (type_ != JSON::JTYPE_ARRAY)
        throw "This JSON value is not an array"; // FIXME: should throw an appropriate exception
    return *array_[index];
}

JSON::ConstObjectIterator JSON::find(const std::string& key) const
{
	if (type_ != JSON::JTYPE_OBJECT)
		throw "This JSON value is not an object"; // FIXME: should throw an appropriate exception
	return _children.find(key);
}

JSON& JSON::operator[](const std::string& key)
{
    if (type_ != JSON::JTYPE_OBJECT)
	{
		clear();
		type_ = JSON::JTYPE_OBJECT;
	}
	if (_children.find(key) == _children.end())
		_children[key] = new JSON();
	return *_children[key];
}

const JSON& JSON::operator[](const std::string& key) const
{
    if (type_ != JSON::JTYPE_OBJECT)
        throw "This JSON value is not an object"; // FIXME: should throw an appropriate exception
	return *_children.find(key)->second;
}

JSON& JSON::operator=(const JSON& json)
{
    clear();
    type_ = json.type_;
    for (std::map<std::string, JSON *>::const_iterator i = json._children.begin(); i != json._children.end(); i ++)
        _children.insert(std::pair<std::string, JSON *>(i->first, new JSON(*i->second)));
    for (std::vector<JSON *>::const_iterator i = json.array_.begin(); i != json.array_.end(); i ++)
        array_.push_back(new JSON(**i));
    value_ = json.value_;
    return (*this);
}

JSON& JSON::operator=(const std::string& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();
	value_ = value;
	type_ = JSON::JTYPE_STRING;
	return *this;
}

JSON& JSON::operator=(const float& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();	
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const int& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const double& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();	
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const long long& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const long& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();	
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const unsigned& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const unsigned long& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();	
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const unsigned long long& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();
	std::stringstream ss;
	ss << value;
	value_ = ss.str();
	type_ = JSON::JTYPE_NUMBER;
	return *this;
}

JSON& JSON::operator=(const bool& value)
{
	if (type_ == JSON::JTYPE_ARRAY || type_ == JSON::JTYPE_OBJECT)
		clear();
	value_ = value?"true":"false";
	type_ = JSON::JTYPE_BOOLEAN;
	return *this;
}

const std::string& JSON::getValue() const
{
	return value_;
}

template <class T> T JSON::as() const
{
	if (type_ != JSON::JTYPE_STRING && type_ != JSON::JTYPE_BOOLEAN && type_ != JSON::JTYPE_NUMBER)
        throw "Cannot convert object or array types"; // FIXME: should throw an appropriate exception
	std::stringstream ss;
	ss << value_;
	T r;
	ss >> r;
    return r;
}

template <> std::string JSON::as() const;
template <> bool JSON::as() const;

#endif // JSON_H
