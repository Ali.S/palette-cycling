#include "Image.h"
#include <iostream>
#include <vector>
#include <fstream>

#define BI_RGB        0L
#define BI_RLE8       1L
#define BI_RLE4       2L
#define BI_BITFIELDS  3L
#define BI_JPEG       4L
#define BI_PNG        5L

bool Image::loadBMP(std::istream & src)
{
	return false;
}

#pragma pack(push)
#pragma pack(1)
struct BitmapHeader {
	uint32_t	biSize;
	uint32_t	biWidth;
	uint32_t	biHeight;
	int16_t		biPlanes;
	int16_t		biBitCount;
	uint32_t	biCompression;
	uint32_t	biSizeImage;
	int32_t		biXPelsPerMeter;
	int32_t		biYPelsPerMeter;
	uint32_t	biClrUsed;
	uint32_t	biClrImportant;
};

struct BitmapFileHeader {
	int16_t		bfType;
	int32_t		bfSize;
	int16_t		bfReserved1;
	int16_t		bfReserved2;
	int32_t		bfOffBits;
};
#pragma pack(pop)

bool Image::saveBMP(std::ostream &dst) const{
	if (!dst) {
		return false;
	}

	BitmapHeader bmih;
	bmih.biSize = sizeof(BitmapHeader);
	bmih.biWidth = _width;
	bmih.biHeight = _height;
	bmih.biPlanes = 1;
	switch (_format)
	{
	case Image::Format::I:
		bmih.biBitCount = 8;
		break;
	case Image::Format::RGB:
		bmih.biBitCount = 24;
		break;
	case Image::Format::RGBA:
		bmih.biBitCount = 32;
		break;
	default:
		return false;
		break;
	}
	bmih.biCompression = BI_RGB;
	
	bmih.biSizeImage = _width * _height * bmih.biBitCount / 8;

	BitmapFileHeader bmfh;
	int nBitsOffset = sizeof(BitmapFileHeader) + bmih.biSize;
	uint32_t lImageSize = bmih.biSizeImage;
	uint32_t lFileSize = nBitsOffset + lImageSize;
	bmfh.bfType = 'B' + ('M' << 8);
	bmfh.bfOffBits = nBitsOffset;
	bmfh.bfSize = lFileSize;
	bmfh.bfReserved1 = bmfh.bfReserved2 = 0;

	// Write the bitmap file header
	dst.write((const char*)&bmfh, sizeof(BitmapFileHeader));
	uint32_t nWrittenFileHeaderSize = dst.tellp();

	// And then the bitmap info header
	dst.write((const char*)&bmih, sizeof(BitmapHeader));
	uint32_t nWrittenInfoHeaderSize = dst.tellp();

	// Finally, write the image data itself
	//-- the data represents our drawing
	switch (_format)
	{
	case Image::Format::I:
		for (unsigned i = 0; i < _data.size(); i ++)
			dst.write(reinterpret_cast<const char*>(_data.data()) + i, 1);
		break;
	case Image::Format::RGB:
		for (unsigned i = 0; i < _data.size(); i += 3)
		{
			dst.write(reinterpret_cast<const char*>(_data.data()) + i + 2, 1);
			dst.write(reinterpret_cast<const char*>(_data.data()) + i + 1, 1);
			dst.write(reinterpret_cast<const char*>(_data.data()) + i + 0, 1);
		}
		break;
	case Image::Format::RGBA:
		for (unsigned i = 0; i < _data.size(); i += 4)
		{
			dst.write(reinterpret_cast<const char*>(_data.data()) + i + 2, 1);
			dst.write(reinterpret_cast<const char*>(_data.data()) + i + 1, 1);
			dst.write(reinterpret_cast<const char*>(_data.data()) + i + 0, 1);
			dst.write(reinterpret_cast<const char*>(_data.data()) + i + 3, 1);
		}
		break;
	default:
		return false;
		break;
	}
	
	return true;
}