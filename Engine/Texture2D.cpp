#include "CommonHeaders.h"
#include "Texture2D.h"
#include <iostream>
#include "GLGlobals.h"

#ifndef GL_BGRA 
#define GL_BGRA 0x80E1
#endif
#ifndef GL_BGR
#define GL_BGR 0x80E0
#endif
#ifndef GL_UNSIGNED_INT_8_8_8_8_REV
#define GL_UNSIGNED_INT_8_8_8_8_REV 0x8367
#endif

Texture2D::Texture2D()
{
	GLGlobals::getInstance()->bindTexture(this);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	minifyFilter_ = magnifyFilter_ = GL_LINEAR;
	wrapModeS_ = wrapModeT_ = GL_CLAMP_TO_EDGE;
}

Texture2D::~Texture2D()
{	
}

void Texture2D::setWrapMode(GLenum wrapModeS, GLenum wrapModeT)
{
	if(wrapModeS != wrapModeS_ || wrapModeT != wrapModeT_)
		GLGlobals::getInstance()->bindTexture(this);
	if(wrapModeS != wrapModeS_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapModeS);
	if(wrapModeT != wrapModeT_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapModeT);
	wrapModeS_ = wrapModeS;
	wrapModeT_ = wrapModeT;
}

void Texture2D::setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter)
{
	if(minifyFilter != minifyFilter_ || magnifyFilter != magnifyFilter_)
		GLGlobals::getInstance()->bindTexture(this);
	if(minifyFilter != minifyFilter_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minifyFilter);
	if(magnifyFilter != magnifyFilter_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magnifyFilter);
	minifyFilter_ = minifyFilter;
	magnifyFilter_ = magnifyFilter_;
}

unsigned int Texture2D::getWidth()
{
	return width_;
}

unsigned int Texture2D::getHeight()
{
	return height_;
}

bool Texture2D::bufferData(int mode, unsigned width, unsigned height, void* data, bool reverse)
{
	GLGlobals::getInstance()->bindTexture(this,GLGlobals::getInstance()->getActiveTextureUnit());
	
	{
		switch (mode)
		{
		case GL_LUMINANCE: 
		{
			char* duplication = new char[width * height * 3];
			for (size_t i = 0; i < width * height; i++)
				duplication[i * 3] = duplication[i * 3 + 1] = duplication[i * 3 + 2] = ((char*)data)[i];
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, duplication);
			delete[]duplication;
			break;
		}
		case GL_STENCIL_INDEX: glTexImage2D(GL_TEXTURE_2D, 0, GL_STENCIL_INDEX8, width, height, 0, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, data); break;
		case GL_RGB: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data); break;
		case GL_BGR: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data); break;
		case GL_RGBA: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, reverse ? GL_UNSIGNED_INT_8_8_8_8_REV : GL_UNSIGNED_BYTE, data); break;
		case GL_BGRA: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, reverse ? GL_UNSIGNED_INT_8_8_8_8_REV : GL_UNSIGNED_BYTE, data); break;
		case GL_DEPTH_COMPONENT:glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, data); break;
			
		default: return false;
		}
	}
	width_ = width;
	height_ = height;	
	return true;
}

GLenum Texture2D::getType()
{
	return GL_TEXTURE_2D;
}