#include "CommonHeaders.h"
#include "Texture.h"

Texture::Texture() : Object()
{
	glGenTextures(1, &_textureID);
}

Texture::~Texture()
{
	glDeleteTextures(1, &_textureID);
}

GLuint Texture::getTextureID()
{
	return _textureID;
}