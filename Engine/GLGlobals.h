#pragma once
#include "Object.h"
#include "Color.h"

class Texture;
struct SDL_Window;
typedef void *GLContext;
class GPUProgram;
class RenderTexture;

class GLGlobals
{
public :
	~GLGlobals();
	static GLGlobals* createInstance(size_t width, size_t height, const char* windowTitle = nullptr);
	static GLGlobals* getInstance();
	void swapBuffers();

	unsigned getWinWidth();
	unsigned getWinHeight();

	void setArrayBuffer(int bufferID);
	void setElementArrayBuffer(int bufferID);
	void setTransformFeedbackBuffer(int bufferID);
	void applyVertexAttributeChanges();
	void resetVertexAttributeEnabled();
	void setVertexAttributeEnabled(int id, bool status);
	void bindTexture(Texture* texture, int textureUnit = 0);
	void useGPUProgram(GPUProgram* shader);
	void setRenderTarget(RenderTexture* renderTarget);


	GPUProgram* getInUseGPUProgram();
	RenderTexture* getRenderTarget();
	int getActiveTextureUnit();
private:
	GLGlobals(size_t width, size_t height, const char* windowTitle);
	static GLGlobals* _sharedGlobals;
	bool setupEnviorments(size_t width, size_t height, const char* windowTitle);
	SYNTHESIZE_READONLY(bool, setupResult_,SetupResult);

	GPUProgram *_inUseGPUProgram;
	RenderTexture *_renderTarget;
	int _arrayBuffer, _elementArrayBuffer, _transformFeedbackBuffer;
	long long _vertexAttributeEnabledStatus, _newVertexAttributeEnabledStatus;
	Texture** _textureIDCache;
	int _textureUnitCount;
	int _activeTextureUnit;
	

	SDL_Window *window_;
	GLContext glContext_;
};