#pragma once
#include "Object.h"
#include <GL/GL.h>

#define VertexAttribute_Position 1
#define VertexAttribute_Normal 2
#define VertexAttribute_Color 3
#define VertexAttribute_TextureCoordinates 4

#define VertexAttribute_Position_Name "a_Position"
#define VertexAttribute_Color_Name "a_Color"
#define VertexAttribute_TextureCoordinates_Name "a_TexCoord"
#define VertexAttribute_Normal_Name "a_Normal"

#define Uniform_ProjectionView_Name "u_projectionview"
#define Uniform_Model_Name "u_model"
#define Uniform_Color_Name "u_color"
#define Uniform_Texture0_name "u_texture0"


class GPUProgramBase : public Object
{
public:
	enum class ShaderType {
		Vertex,
		Geomtry,
		Framgent
	};
	virtual void use();
	virtual bool inUse();
	virtual bool isUsable();
	bool attachShader(ShaderType type, const GLchar* sources);
	bool linkProgram();
	static GPUProgramBase* getInUseGPUProgram();
	int getUniformID(const char* name);
	int getAttribitueID(const char* name);
	void setUniform(int ID, float v0);
	void setUniform(int ID, float v0, float v1);
	void setUniform(int ID, float v0, float v1, float v2);
	void setUniform(int ID, float v0, float v1, float v2, float v3);
	void setUniform(int ID, int v0);
	void setUniform(int ID, int v0, int v1);
	void setUniform(int ID, int v0, int v1, int v2);
	void setUniform(int ID, int v0, int v1, int v2, int v3);		
	void setUniform(int ID, const kmMat3 &v0);
	void setUniform(int ID, const kmMat4 &v0);
protected:
	GPUProgramBase();
	~GPUProgramBase();
	GLuint _programID;
	static GPUProgramBase* _inUseGPUProgram;
	char* logForOpenGLObject(GLuint object, void(__stdcall *infoFunc)(GLuint, GLenum, GLint*), void(__stdcall *logFunc)(GLuint, GLsizei, GLsizei*, GLchar*));
};