#include "CommonHeaders.h"
#include "Texture3D.h"
#include <iostream>
#include "GLGlobals.h"

#ifndef GL_BGRA 
#define GL_BGRA 0x80E1
#endif
#ifndef GL_BGR
#define GL_BGR 0x80E0
#endif
#ifndef GL_UNSIGNED_INT_8_8_8_8_REV
#define GL_UNSIGNED_INT_8_8_8_8_REV 0x8367
#endif

Texture3D::Texture3D()
{
	GLGlobals::getInstance()->bindTexture(this);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	minifyFilter_ = magnifyFilter_ = GL_LINEAR;
	wrapModeS_ = wrapModeT_ = GL_CLAMP_TO_EDGE;
}

Texture3D::~Texture3D()
{
}

void Texture3D::setWrapMode(GLenum wrapModeS, GLenum wrapModeT, GLenum wrapModeR)
{
	if (wrapModeS != wrapModeS_ || wrapModeT != wrapModeT_ || wrapModeR != wrapModeR_)
		GLGlobals::getInstance()->bindTexture(this);
	else
		return;
	if (wrapModeS != wrapModeS_)
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, wrapModeS);
	if (wrapModeT != wrapModeT_)
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, wrapModeT);
	if (wrapModeR != wrapModeR_)
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, wrapModeR);
	wrapModeS_ = wrapModeS;
	wrapModeT_ = wrapModeT;
	wrapModeR_ = wrapModeR;
}

void Texture3D::setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter)
{
	if (minifyFilter != minifyFilter_ || magnifyFilter != magnifyFilter_)
		GLGlobals::getInstance()->bindTexture(this);
	if (minifyFilter != minifyFilter_)
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, minifyFilter);
	if (magnifyFilter != magnifyFilter_)
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, magnifyFilter);
	minifyFilter_ = minifyFilter;
	magnifyFilter_ = magnifyFilter_;
}

unsigned int Texture3D::getWidth()
{
	return width_;
}

unsigned int Texture3D::getHeight()
{
	return height_;
}

unsigned int Texture3D::getDepth()
{
	return depth_;
}

bool Texture3D::bufferData(int mode, unsigned width, unsigned height, unsigned depth, void* data, bool reverse)
{
	GLGlobals::getInstance()->bindTexture(this, GLGlobals::getInstance()->getActiveTextureUnit());

	switch (mode)
	{
	case GL_LUMINANCE: glTexImage3D(GL_TEXTURE_3D, 0, GL_LUMINANCE, width, height, depth, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, data); break;
	case GL_RGB: glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, width, height, depth, 0, GL_RGB, GL_UNSIGNED_BYTE, data); break;
	case GL_BGR: glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, width, height, depth, 0, GL_BGR, GL_UNSIGNED_BYTE, data); break;
	case GL_RGBA: glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_RGBA, reverse ? GL_UNSIGNED_INT_8_8_8_8_REV : GL_UNSIGNED_BYTE, data); break;
	case GL_BGRA: glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_BGRA, reverse ? GL_UNSIGNED_INT_8_8_8_8_REV : GL_UNSIGNED_BYTE, data); break;
	case GL_DEPTH_COMPONENT:glTexImage3D(GL_TEXTURE_3D, 0, GL_DEPTH_COMPONENT16, width, height, depth, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, data); break;
	default: return false;
	}

	width_ = width;
	height_ = height;
	depth_ = depth;
	return true;
}

GLenum Texture3D::getType()
{
	return GL_TEXTURE_3D;
}