#pragma once
#include "Object.h"
#include "Macros.h"
#include "RenderableProtocol.h"
#include <gl/GL.h>
#include <memory>
#include "GLGlobals.h"
#include <vector>

class Texture2D;
class GPURenderProgram;

class Mesh : public Object, public RenderableProtocol
{
public:
	struct VertexInfo
	{
		union{
			struct {
				float u,v;
			};
			struct {
				float u,v;
			} textureCoordination;
		};
		union {
			struct {
				float r,g,b,a;
			};
			struct {
				float r,g,b,a;
			} color;
		};
		union{
			struct{
				float nx,ny,nz;
			};
			struct {
				float x,y,z;
			} normal;
		};
		union {
			struct {
				float x,y,z;
			};
			struct {
				float x,y,z;
			}position;
		};		
	};

	Mesh();
	Mesh(const Mesh&);
	Mesh(const VertexInfo* vertices, int vertexCount);
	Mesh(const VertexInfo* vertices, int vertexCount, const short* indices, int indexCount);

	~Mesh();
	
	virtual void render();
	virtual void postRender();
	void setRenderProgram(GPURenderProgram* shader);
	GPURenderProgram* getRenderProgram();
	template<class T> void setData(const T* vertices, int vertexCount);
	template<class T> void setData(const T* vertices, int vertexCount, const short* indices, int indexCount);	
	GLuint getArrayBufferID();
	GLuint getElementArrayBufferID();
	virtual void load(const char* path);
	void setUniformColor(float r,float g,float b, float a = 1.0f);
	void disposeData();
	void removeUniformColor();
	
	PROPERTY(Texture*, _texture, Texture);
	PROPERTY(bool, _usePerVertexColor, UsePerVertexColor);
	PROPERTY(bool, _autoSelectShader, AutoSelectShader);
	PROPERTY(bool, _hasNormals, HasNormals);

protected:	
	bool _useIndices;	
	struct {
		float r,g,b,a;
		bool inUse;
	} _uniformColor;
	unsigned _indicesCount;	
	GPURenderProgram* _shader;
	struct VBOData
	{
		GLuint vertices;
		GLuint indices;
	};	
	std::shared_ptr<VBOData> _VBOID;
	struct
	{
		void* _positionOffset;
		void* _colorOffset;
		void* _texCoordOffset;
		void* _normalOffset;
	} _vertexAttributeOffsets;
	struct
	{
		int VMatrix, PMatrix, MMatrix, texture,color;
	} _uniformIDs;
	
	int _vertexAttributeStride;
	void objFileLoader(const char* path);
};

template<class T> void Mesh::setData(const T* vertices, int vertexCount, const short* indices, int indexCount)
{
	if (!_VBOID)
	{
		_VBOID = std::make_shared<VBOData>();
		glGenBuffers(2, reinterpret_cast<GLuint*>(_VBOID.get()));		
	}	
	GLGlobals::getInstance()->setArrayBuffer(_VBOID->vertices);
	GLGlobals::getInstance()->setElementArrayBuffer(_VBOID->indices);
	glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(T), vertices, GL_STATIC_DRAW);	
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(short), indices, GL_STATIC_DRAW);
	_useIndices = true;
	_indicesCount = indexCount;
	_vertexAttributeStride = sizeof(T);	
	_vertexAttributeOffsets._colorOffset = &(reinterpret_cast<T*>(0)->color);
	_vertexAttributeOffsets._positionOffset= &(reinterpret_cast<T*>(0)->position);
	_vertexAttributeOffsets._normalOffset = &(reinterpret_cast<T*>(0)->normal);
	_vertexAttributeOffsets._texCoordOffset= &(reinterpret_cast<T*>(0)->textureCoordination);
}

template<class T> void Mesh::setData(const T* vertices, int vertexCount)
{
	if (!_VBOID)
	{
		_VBOID = std::make_shared<VBOData>(VBOData());
		glGenBuffers(2, reinterpret_cast<GLuint*>(_VBOID.get()));		
	}	
	GLGlobals::getInstance()->setArrayBuffer(_VBOID->vertices);	
	glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(T), vertices, GL_STATIC_DRAW);
	_useIndices = false;
	_indicesCount = vertexCount;
	_vertexAttributeStride = sizeof(T);	
	_vertexAttributeOffsets._colorOffset = &(reinterpret_cast<T*>(0)->color);
	_vertexAttributeOffsets._positionOffset= &(reinterpret_cast<T*>(0)->position);
	_vertexAttributeOffsets._normalOffset = &(reinterpret_cast<T*>(0)->normal);
	_vertexAttributeOffsets._texCoordOffset= &(reinterpret_cast<T*>(0)->textureCoordination);
}
