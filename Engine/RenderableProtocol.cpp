#include "RenderableProtocol.h"

RenderableProtocol::RenderableProtocol() : _visible(true)
{
}

void RenderableProtocol::preRender()
{
	recalculateWorldTransformationMatrix();
}

void RenderableProtocol::setVisible(const bool &value)
{
	_visible = value;
}

const bool &RenderableProtocol::getVisible() const
{
	return _visible;
}