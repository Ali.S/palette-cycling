#pragma once

static const char* textureCopierShaderV = R"(	
	#ifdef GL_ES												
	varying highp vec2 v_TexCoord;							
	#else														
	varying vec2 v_TexCoord;								
	#endif													
															
	void main()												
	{			
		gl_Position = getPosition();										
		v_TexCoord = )" VertexAttribute_TextureCoordinates_Name R"(;								
	}														
)";

static const char* textureCopierShaderF = R"(
	varying vec2 v_TexCoord;								
	uniform sampler2D )" Uniform_Texture0_name R"(;							
															
	void main()												
	{													    
		gl_FragColor = 
			texture2D()" Uniform_Texture0_name R"(,v_TexCoord);				
	}														
)";

static const char* positionColorShaderV = R"(
	varying vec4 v_color;									
															
	void main()												
	{			
		gl_Position = getPosition();										
		v_color = a_Color;										
	}														
)";

static const char* positionColorShaderF = R"(
	varying vec4 v_color;									
	
	void main()												
	{ 
	gl_FragColor = v_color;
	}														
)";

static const char* positionTextureColorShaderV = R"(
#ifdef GL_ES													
	varying highp vec2 v_TexCoord;							
	varying lowp vec4 v_color;									
#else															
	varying vec2 v_TexCoord;								
	varying vec4 v_color;									
#endif														
	
	void main()												
	{
		gl_Position = getPosition();						
		v_TexCoord = )" VertexAttribute_TextureCoordinates_Name R"(;							
	}														
)";

static const char* positionTextureColorShaderF = R"(
	varying vec4 v_color;									
	varying vec2 v_TexCoord;								
	uniform sampler2D )" Uniform_Texture0_name R"(;							
	
	void main()												
	{ 
	gl_FragColor =
	texture2D()" Uniform_Texture0_name R"(, v_TexCoord) * v_color;						

	}														
)";


const char* ShaderPosition_v = R"(
															
	void main()												
	{														
		gl_Position = getPosition();						
	}
)";

const char* ShaderPosition_f = R"(
															
	void main()												
	{														
		gl_FragColor = vec4(1,1,1,1);						
	}
)";

const char* ShaderPosition_uColor_f = R"(
	uniform vec4 )" Uniform_Color_Name R"(;									
															
	void main()												
	{														
		gl_FragColor = )" Uniform_Color_Name R"(;								
	}
)";