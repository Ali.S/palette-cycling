#include "CommonHeaders.h"
#include "Color.h"

const Color4B Color4B::Black(0, 0, 0);
const Color4B Color4B::White(255, 255, 255);
const Color4B Color4B::Red(255, 0, 0);
const Color4B Color4B::Green(0, 255, 0);
const Color4B Color4B::Blue(0, 0, 255);

const Color4F Color4F::Black(0, 0, 0);
const Color4F Color4F::White(1, 1, 1);
const Color4F Color4F::Red(1, 0, 0);
const Color4F Color4F::Green(0, 1, 0);
const Color4F Color4F::Blue(0, 0, 1);

Color4B::Color4B(const Color4F & ref) :
	r(ref.r < 0 ? 0 : ref.r > 1 ? 255 : static_cast<uint8_t>(ref.r * 255)),
	g(ref.g < 0 ? 0 : ref.g > 1 ? 255 : static_cast<uint8_t>(ref.g * 255)),
	b(ref.b < 0 ? 0 : ref.b > 1 ? 255 : static_cast<uint8_t>(ref.b * 255)),
	a(ref.a < 0 ? 0 : ref.a > 1 ? 255 : static_cast<uint8_t>(ref.a * 255))
{
}

Color4B::Color4B(uint8_t r, uint8_t g, uint8_t b, uint8_t a) :
	r(r), g(g), b(b), a(a)
{
}

uint8_t Color4B::avg() const
{
	int t = r;
	t += g;
	t += b;
	return static_cast<uint8_t>(t / 3);
}

kmScalar Color4B::similarity(const Color4B & ref) const
{
	return Color4F(*this).similarity(ref);
}

Color4F::Color4F(const Color4B & ref):
	r(ref.r / 255.0), g(ref.g / 255.0), b(ref.b / 255.0), a(ref.a / 255.0)
{
}

Color4F::Color4F(kmScalar r, kmScalar g, kmScalar b, kmScalar a) :
	r(r), g(g), b(b), a(a)
{
}

Color4F &Color4F::operator += (const Color4F& o)
{
	r += o.r;
	g += o.g;
	b += o.b;
	return *this;
}

Color4F &Color4F::operator -= (const Color4F& o)
{
	r -= o.r;
	g -= o.g;
	b -= o.b;
	return *this;
}

Color4F Color4F::operator - (const Color4F& o) const
{
	Color4F r(*this);
	return r -= o;
}

Color4F Color4F::operator + (const Color4F& o) const
{
	Color4F r(*this);
	return r += o;
}

Color4F &Color4F::operator *= (const kmScalar& o)
{
	r *= o;
	g *= o;
	b *= o;
	return *this;
}

Color4F Color4F::operator * (const kmScalar& o) const
{
	Color4F r(*this);
	return r *= o;
}

Color4F &Color4F::operator /= (const kmScalar& o)
{
	r /= o;
	g /= o;
	b /= o;
	return *this;
}

Color4F Color4F::operator / (const kmScalar& o) const
{
	Color4F r(*this);
	return r /= o;
}

kmScalar Color4F::length2() const
{
	return r * r + g * g + b * b;
}

kmScalar Color4F::length() const
{
	return sqrt(length2());
}

kmScalar Color4F::avg() const
{
	return static_cast<kmScalar>((r + g + b) / 3.0);
}

kmScalar Color4F::similarity(const Color4F &ref) const
{
	kmScalar rdif = fabs(ref.r - r);
	kmScalar gdif = fabs(ref.g - g);
	kmScalar bdif = fabs(ref.b - b);
	kmScalar diff = (sqrt(rdif) + sqrt(gdif) + sqrt(bdif)) / static_cast<kmScalar>(3);
	return (0.5 - diff) * 2;
}
