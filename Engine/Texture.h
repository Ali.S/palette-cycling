#pragma once
#include "Object.h"

class Texture: public Object
{
public:
	Texture();
	~Texture();
	GLuint getTextureID();
	virtual GLenum getType() = 0;
private:
	GLuint _textureID;
};