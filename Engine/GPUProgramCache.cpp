#include "CommonHeaders.h"
#include "GPUProgramCache.h"
#include "GPURenderProgram.h"

#include "TextureCopierShader.inl"

GPUProgramCache* GPUProgramCache::_instance = nullptr;

GPUProgramCache::GPUProgramCache()
{
	loadDefaultShaders();
}

GPUProgramCache* GPUProgramCache::getInstance()
{
	if (!_instance)
		_instance = new GPUProgramCache();
	return _instance;
}

void GPUProgramCache::purgeSharedShaderCache()
{
	delete _instance;
}

void GPUProgramCache::addProgram(const std::string &name, GPUProgramBase* shader)
{
	shader->retain();
	if (_programLibrary.find(name) != _programLibrary.end())
		_programLibrary[name]->release();
	_programLibrary[name] = shader;
}

void GPUProgramCache::removeProgram(const std::string &name)
{
	auto shader = _programLibrary.find(name);
	if (shader!= _programLibrary.end())
	{
		_programLibrary.erase(shader);
		shader->second->release();
	}
}

GPUProgramBase * GPUProgramCache::getProgram(const std::string & name)
{
	auto shader = _programLibrary.find(name);
	if (shader != _programLibrary.end())
		return shader->second;
	else
		return nullptr;
}

GPURenderProgram* GPUProgramCache::getRenderProgram(const std::string &name)
{
	return dynamic_cast<GPURenderProgram*>(getProgram(name));
}

void GPUProgramCache::loadDefaultShaders()
{
	addProgram(SHADERNAME_POSITION_TEXTURE, new GPURenderProgram(textureCopierShaderV, textureCopierShaderF));
	addProgram(SHADERNAME_POSITION_TEXTURE_COLOR, new GPURenderProgram(positionTextureColorShaderV, positionTextureColorShaderF));
	addProgram(SHADERNAME_POSITION_COLOR, new GPURenderProgram(positionColorShaderV, positionColorShaderF));
	addProgram(SHADERNAME_POSITION, new GPURenderProgram(ShaderPosition_v, ShaderPosition_f));
	addProgram(SHADERNAME_POSITION_U_COLOR, new GPURenderProgram(ShaderPosition_v, ShaderPosition_uColor_f));
}


void GPUProgramCache::updateRenderProgramsBuiltinUniforms(const kmMat4 &projection)
{
	kmMat4 id;
	kmMat4Identity(&id);
	GPURenderProgram* s;
	for(auto i : _programLibrary)
		if (s = dynamic_cast<GPURenderProgram *>(i.second))
		{
			s->use();
			s->setUniform(s->getPVLocation(), projection);
			s->setUniform(s->getMLocation(), id);
		}
}