#pragma once
#include "macros.h"
#include <kazmath.h>
#include <memory>

class Node;

class PositionProtocol {
	kmMat4 _transformMatrix, _worldTransformationMatrix;
	bool _transformDirty;
	PositionProtocol* _parent;
	friend class Node;
public:
	PositionProtocol();
	virtual ~PositionProtocol();	
	void recalculateWorldTransformationMatrix(bool recurse = false);
	const kmMat4& getTransformationMatrix();
	const kmMat4& getWorldTransformationMatrix();
	
	PROPERTY(kmVec3, _position, Position);
	PROPERTY(kmVec3, _rotation, Rotation);
	PROPERTY(kmVec3, _scale, Scale);

	virtual void setPosition(kmScalar x, kmScalar y, kmScalar z);
	virtual void setRotation(kmScalar x, kmScalar y, kmScalar z);
	virtual void setScale(kmScalar x, kmScalar y, kmScalar z);

	PositionProtocol* getParent();
};