#include "CommonHeaders.h"
#include "GPUProgramBase.h"
#include <iostream>

GPUProgramBase* GPUProgramBase::_inUseGPUProgram = nullptr;

GPUProgramBase::GPUProgramBase()
{
	_programID = glCreateProgram();
}

GPUProgramBase::~GPUProgramBase()
{
	glDeleteProgram(_programID);
}

bool GPUProgramBase::attachShader(GPUProgramBase::ShaderType type, const GLchar* source)
{
    GLint status;
	int shaderID = 0;
    if (!source)    
        return false;

	const GLchar** sources;
	typedef const GLchar* GLStringArray[];
	int glShaderType;
	switch (type)
	{
	case ShaderType::Vertex:
		sources = GLStringArray{
#if TARGET_PLATFORM == PLATFORM_WIN32
			"#version 130																		\n"
#endif	
			"#ifdef GL_ES																		\n"
			"precision lowp float;																\n"
			"#endif																				\n"
			"attribute vec4 " VertexAttribute_Position_Name ";									\n"
			"attribute vec2 " VertexAttribute_TextureCoordinates_Name ";						\n"
			"attribute vec4 " VertexAttribute_Color_Name ";										\n"
			"attribute vec3 " VertexAttribute_Normal_Name ";									\n"
			"uniform mat4 " Uniform_ProjectionView_Name ";										\n"
			"uniform mat4 " Uniform_Model_Name ";												\n"
			"vec4 getPosition()																	\n"
			"{																					\n"
			"	vec4 position;																	\n"
			"	position = " Uniform_ProjectionView_Name " * "
			Uniform_Model_Name " * "
			VertexAttribute_Position_Name ";\n"
			"	return position;																\n"
			"}																					\n",
			source
		};
		glShaderType = GL_VERTEX_SHADER;
		break;
	case ShaderType::Geomtry:
		sources = GLStringArray{
			"#version 150",
			source
		};
		glShaderType = GL_GEOMETRY_SHADER;
		break;
	case ShaderType::Framgent:
		sources = GLStringArray{
#if TARGET_PLATFORM == PLATFORM_WIN32
			"#version 130												\n"
#endif
			"#ifdef GL_ES												\n"
			"precision lowp float;										\n"
			"#endif														\n",
			source
		};
		glShaderType = GL_FRAGMENT_SHADER;
		break;
	default:
		return false;
	}

		
    shaderID = glCreateShader(glShaderType);
	if (shaderID == 0)
	{
		return false;
	}
	glShaderSource(shaderID, 2, sources, nullptr);
    glCompileShader(shaderID);

    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);

    if (!status)
    {
        GLsizei length;
		glGetShaderiv(shaderID, GL_SHADER_SOURCE_LENGTH, &length);
		GLchar* src = (GLchar*)malloc(sizeof(GLchar) * length);
		
		glGetShaderSource(shaderID, length, NULL, src);
		Log("ERROR: Failed to compile shader:%s\n",src);
		free(src);
        
        src = logForOpenGLObject(shaderID, glGetShaderiv, glGetShaderInfoLog);
		Log("ERROR: Compiler output:%s\n", src);
		free (src);
		glDeleteShader(shaderID);
    }
	else
	{
		glAttachShader(_programID, shaderID);
	}
	return status;
}

bool GPUProgramBase::linkProgram()
{
	int status;
	glGetProgramiv(_programID, GL_LINK_STATUS, &status);
	if (status)
		return false;
	
	glLinkProgram(_programID);
	glGetProgramiv(_programID, GL_LINK_STATUS, &status);
	if (!status)
	{
		GLchar *src;
		Log("ERROR: linking shader failed.\n");
		src = logForOpenGLObject(_programID, glGetProgramiv, glGetProgramInfoLog);
		Log("ERROR: linker output %s \n", src);
		free(src);
		_programID = 0;
	}

	return status;
}




char* GPUProgramBase::logForOpenGLObject(GLuint object, void(__stdcall *infoFunc)(GLuint, GLenum, GLint*), void(__stdcall *logFunc)(GLuint, GLsizei, GLsizei*, GLchar*))
{
    GLint logLength = 0, charsWritten = 0;

    infoFunc(object, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength < 1)
        return 0;

    char *logBytes = (char*)malloc(logLength);
    logFunc(object, logLength, &charsWritten, logBytes);
    

	return logBytes;
}

void GPUProgramBase::use()
{
	if(_inUseGPUProgram != this)
	{
		glUseProgram(_programID);
		_inUseGPUProgram = this;
	}
}

bool GPUProgramBase::inUse()
{
	return _inUseGPUProgram == this;
}

bool GPUProgramBase::isUsable()
{
	return _programID != 0;
}

GPUProgramBase* GPUProgramBase::getInUseGPUProgram()
{
	return _inUseGPUProgram;
}

int GPUProgramBase::getUniformID(const char* name)
{
	int res;
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		res = glGetUniformLocation(_programID, name);
		temp->use();
	}
	else
		res = glGetUniformLocation(_programID, name);
	return res;
}

int GPUProgramBase::getAttribitueID(const char* name)
{
	int res;
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		res = glGetAttribLocation(_programID, name);
		temp->use();
	}
	else
		res = glGetAttribLocation(_programID, name);
	return res;
}

void GPUProgramBase::setUniform(int ID, float v0)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform1f(ID, v0);
		temp->use();
	}
	else
		glUniform1f(ID, v0);
}

void GPUProgramBase::setUniform(int ID, float v0, float v1)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform2f(ID, v0, v1);
		temp->use();
	}
	else
		glUniform2f(ID, v0, v1);
}

void GPUProgramBase::setUniform(int ID, float v0, float v1, float v2)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform3f(ID, v0, v1, v2);
		temp->use();
	}
	else
		glUniform3f(ID, v0, v1, v2);
}

void GPUProgramBase::setUniform(int ID, float v0, float v1, float v2, float v3)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform4f(ID, v0, v1, v2, v3);
		temp->use();
	}
	else
		glUniform4f(ID, v0, v1, v2, v3);
}

void GPUProgramBase::setUniform(int ID, int v0)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform1i(ID, v0);
		temp->use();
	}
	else
		glUniform1i(ID, v0);
}

void GPUProgramBase::setUniform(int ID, int v0, int v1)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform2i(ID, v0, v1);
		temp->use();
	}
	else
		glUniform2i(ID, v0, v1);
}

void GPUProgramBase::setUniform(int ID, int v0, int v1, int v2)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform3i(ID, v0, v1, v2);
		temp->use();
	}
	else
		glUniform3i(ID, v0, v1, v2);
}

void GPUProgramBase::setUniform(int ID, int v0, int v1, int v2, int v3)
{
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniform4i(ID, v0, v1, v2, v3);
		temp->use();
	}
	else
		glUniform4i(ID, v0, v1, v2, v3);
}

void GPUProgramBase::setUniform(int ID, const kmMat3& v0)
{
	GLfloat fl[9];
	for (int i = 0; i < 9; i++)
		fl[i] = v0.mat[i];
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniformMatrix3fv(ID, 1, GL_FALSE, fl);
		temp->use();
	}
	else
		glUniformMatrix3fv(ID, 1, GL_FALSE, fl);
}

void GPUProgramBase::setUniform(int ID, const kmMat4& v0)
{
	GLfloat fl[16];
	for (int i = 0; i < 16; i++)
		fl[i] = v0.mat[i];
	if (!inUse())
	{
		auto temp = _inUseGPUProgram;
		use();
		glUniformMatrix4fv(ID, 1, GL_FALSE, fl);
		temp->use();
	}
	else
		glUniformMatrix4fv(ID, 1, GL_FALSE, fl);
}
