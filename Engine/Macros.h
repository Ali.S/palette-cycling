#pragma once
#ifndef WIN32
#define __stdcall
#endif

#define SAFE_RELEASE(X) if(X)(X)->release()

#define PROPERTY(Type, Name, FunName)\
	private:\
	Type Name;\
	public:\
		Type const& get##FunName()const;\
		void set##FunName(Type const& FunName);

#define SYNTHESIZE(Type, Name, FunName)\
	public:\
	Type const& get##FunName()const{return Name;}\
	void set##FunName(Type const&value){Name = value;}\
	private:\
	Type Name;
	
#define SYNTHESIZE_READONLY(Type, Name, FunName)\
	private:\
	Type Name;\
	public:\
	Type const& get##FunName()const{return Name;}
