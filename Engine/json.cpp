#include "CommonHeaders.h"
#include "json.h"
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <filesystem>

using namespace std;

JSON::JSON()
{
	type_ = JSON::JTYPE_NULL;
}

JSON::JSON(const JSON& json)
{
    type_ = json.type_;
    for (std::map<std::string, JSON *>::const_iterator i = json._children.begin(); i != json._children.end(); i ++)
        _children.insert(std::pair<std::string, JSON *>(i->first, new JSON(*i->second)));
    for (std::vector<JSON *>::const_iterator i = json.array_.begin(); i != json.array_.end(); i ++)
        array_.push_back(new JSON(**i));
    value_ = json.value_;
}

JSON::JSON(const char* path)
{
    load(path);
}

JSON::~JSON()
{
    for (std::map<std::string, JSON *>::iterator i = _children.begin(); i != _children.end(); i ++)
        delete i->second;
    for (std::vector<JSON *>::iterator i = array_.begin(); i != array_.end(); i ++)
        delete *i;
}

void JSON::load(const char* path)
{	
    clear();
	ifstream in(path);

	auto fsize = std::filesystem::file_size(path);

	char *string = (char*)malloc(fsize + 1);
	in.read(string, fsize);
	in.close();

	string[fsize] = 0;
	rec_parse(string,string + fsize);
	free(string);
}

void JSON::save(const char* path) const
{
	ofstream f(path);
    rec_write(f);
    f.close();
}

void JSON::save(std::string &output) const
{
	stringstream ss;
	rec_write(ss, 0);
    output = ss.str();
}

void JSON::parse(const std::string& text)
{
	rec_parse(text.c_str(), text.c_str() + text.length());
}

void JSON::parse(const char* textBegin, const char* textEnd)
{
	rec_parse(textBegin, textEnd);
}

void JSON::merge(const JSON & other, ConflictPolicy policy)
{
	if (getType() == other.getType())
	{
		switch (getType()) {
		case JTYPE_ARRAY:
		{
			size_t minSize = size() < other.size() ? size() : other.size();
			for (size_t i = 0; i < minSize; i++)
				(*this)[i].merge(other[i], policy);
			for (size_t i = 0; i < other.size(); i++)
				(*this)[i] = other[i];
			resize(other.size());
			break;
		}
		case JTYPE_OBJECT:
		{
			for (auto child = other.objectBegin(); child != other.objectEnd(); child++)
				(*this)[child->first].merge(*child->second, policy);

			break;
		}
		default:
			if (policy == CP_Theirs)
				*this = other;
		}
	}
	else
		if (policy == CP_Theirs)
			*this = other;
}

void JSON::clear()
{
	type_ = JSON::JTYPE_NULL;
    for (std::map<std::string, JSON *>::iterator i = _children.begin(); i != _children.end(); i ++)
        delete i->second;
    _children.clear();
    for (std::vector<JSON *>::iterator i = array_.begin(); i != array_.end(); i ++)
        delete *i;
    array_.clear();
    value_.clear();
}

int JSON::rec_parse(const char* const text,const char* const end)
{
#define SKEEP_SPACES \
    while (*p == ' ' || *p == '\t' || *p == '\n' || *p == '\r') \
        p ++
    
    const char *p = text;
    SKEEP_SPACES;
    if (p == end)
        return p - text;
    if (*p == '{')
    {
        type_ = JSON::JTYPE_OBJECT;
		while (*p != '}')
        {
			p ++;
			SKEEP_SPACES;
			// Watch out for an empty object
			if (*p == '}')
				break;
			if (*p != '"')
                throw "JSON object member name is not a string"; // FIXME
            string name;
            p += read_string(p, name);
            p ++;
            SKEEP_SPACES;
            if (*p != ':')
                throw "JSON object member name does not followed by a colon"; // FIXME
            p ++;
            SKEEP_SPACES;
            JSON *child = new JSON;
            p += child->rec_parse(p,end);
            p ++;
            SKEEP_SPACES;
            if (*p != ',' && *p != '}')
                throw ("\"}\" or \",\" expected after member \"" + name + "\"").c_str(); // FIXME
            _children.insert(pair<string, JSON*>(name, child));
		}
    }
    else if (*p == '[')
    {
        type_ = JSON::JTYPE_ARRAY;
		while (*p != ']')
        {
			p ++;
			SKEEP_SPACES;
			// Watch out for an empty object
			if (*p == ']')
				break;
			JSON *child = new JSON;
            p += child->rec_parse(p, end);
            p ++;
            SKEEP_SPACES;
            if (*p != ',' && *p != ']')
                throw "\"]\" or \",\" expected"; // FIXME
            array_.push_back(child);
		}
    }
    else if (*p == '"')
    {
        type_ = JSON::JTYPE_STRING;
        p += read_string(p, value_);
    }
    else
    {
        for (; *p != ' ' && *p != '\t' && *p != '\n' && *p != '\r' && *p != ',' && *p != ':' && *p != '[' && *p != ']' && *p != '{' && *p != '}'; p ++)
            value_.push_back(*p);
		if (value_ == "null")
			type_ = JTYPE_NULL;
		else if (value_ == "true" || value_ == "false")
			type_ = JTYPE_BOOLEAN;
        else
            type_ = JTYPE_NUMBER;
        p --;
    }
    return p - text;
#undef SKEEP_SPACES
}

int JSON::read_string(const char* const text, string& name)
{
    const char *p = text;
    for (p ++; *p && *p != '"'; p ++)
    {
        if (*p == '\\')
        {
            p ++;
            switch (*p)
            {
            case 'b':
                name.push_back('\b');
                break;
            case 'f':
                name.push_back('\f');
                break;
            case 'n':
                name.push_back('\n');
                break;
            case 'r':
                name.push_back('\r');
                break;
            case 't':
                name.push_back('\t');
                break;
            case 'u':
            {
                int num = 0;
                int i;
                for (p ++, i = 0; i < 4 && ((*p >= '0' && *p <= '9') || (tolower(*p) >= 'a' && tolower(*p) <= 'f')); p ++)
                    num = num * 16 + (*p >= 'A' ? tolower(*p) - 'a' + 10 : *p - '0');
                break;
            }
            default:
                name.push_back(*p);
            }
        }
        else
            name.push_back(*p);
    }
    return p - text;
}

void JSON::rec_write(ostream& f, int tab) const
{
#define INSERT_TABS \
    for (int j = 0; j < tab; j ++) \
        f << '\t'
    
    switch (type_)
    {
    case JSON::JTYPE_OBJECT:
    {
        f << "{\n";
        map<string, JSON*>::const_iterator i = _children.begin();
        if (i != _children.end())
        {
            INSERT_TABS;
            f << "\t\"" << i->first << "\" : ";
            i->second->rec_write(f, tab+1);
        }
        for (i ++; i != _children.end(); i ++)
        {
            f << ",\n";
            INSERT_TABS;
            f << "\t\"" << i->first << "\" : ";
            i->second->rec_write(f, tab+1);
        }
        f << '\n';
        INSERT_TABS;
        f << '}';
        break;
    }
    case JSON::JTYPE_ARRAY:
    {
        f << "[\n";
        vector<JSON*>::const_iterator i = array_.begin();
        if (i != array_.end())
        {
            INSERT_TABS;
            f << '\t';
            (*i)->rec_write(f, tab+1);
			for (i ++; i != array_.end(); i ++)
			{
				f << ",\n";
				INSERT_TABS;
				f << '\t';
				(*i)->rec_write(f, tab+1);
			}
		}
        f << '\n';
        INSERT_TABS;
        f << ']';
        break;
    }
	case JSON::JTYPE_NULL:
		f << "null";
		break;
    case JSON::JTYPE_STRING:
		f << '"' << value_ << '"';
        break;
    case JSON::JTYPE_NUMBER:
	case JSON::JTYPE_BOOLEAN:
        f << value_;
        break;
    default:
        break;
    }
}

template <> std::string JSON::as<std::string>() const
{
	if (type_ != JSON::JTYPE_STRING && type_ != JSON::JTYPE_BOOLEAN && type_ != JSON::JTYPE_NUMBER)
		throw "Cannot convert object or array types"; // FIXME: should throw an appropriate exception
	return value_;
}

template <> bool JSON::as<bool>() const
{
	if (type_ != JSON::JTYPE_STRING && type_ != JSON::JTYPE_BOOLEAN && type_ != JSON::JTYPE_NUMBER)
		throw "Cannot convert object or array types"; // FIXME: should throw an appropriate exception
	return value_[0] == 't';
};