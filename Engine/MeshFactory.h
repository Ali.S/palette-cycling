#pragma once
#include "Object.h"

class Mesh;

class MeshFactory
{
public:
	static Mesh* createSphere(unsigned vSlice, unsigned hSlice);
	static Mesh* assignSphere(Mesh* target, unsigned vSlice, unsigned hSlice);
	static Mesh* createCapsule(unsigned vSlice, unsigned hSlice, kmScalar height);
	static Mesh* assignCapsule(Mesh* target, unsigned vSlice, unsigned hSlice, kmScalar Height);
};