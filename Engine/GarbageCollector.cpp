#include "CommonHeaders.h"
#include "GarbageCollector.h"
#include "Object.h"

GarbageCollector * GarbageCollector::sharedGarbageCollector_ = nullptr;

GarbageCollector::GarbageCollector()
{
}

GarbageCollector::~GarbageCollector()
{
	for(auto i:Objects_)
		i->release();
}

GarbageCollector* GarbageCollector::sharedGarbageCollector()
{
	if (sharedGarbageCollector_ == nullptr)
		sharedGarbageCollector_ = new GarbageCollector();
	return sharedGarbageCollector_;
}

void GarbageCollector::purgeGarbageCollector()
{
	delete sharedGarbageCollector_;
}

void GarbageCollector::pushObject(Object* obj)
{
	Objects_.push_back(obj);
	obj->retain();
}

void GarbageCollector::cleanUp()
{	
	for(auto i:Objects_)
		i->release();
	Objects_.clear();	
}