#pragma once
#include "GPUProgramBase.h"
#include <kazmath.h>
#include <memory>
#include <string>
#include <unordered_map>

#define SHADERNAME_POSITION_U_COLOR "SH_PUC"
#define SHADERNAME_POSITION_TEXTURE_U_COLOR "SH_PTUC"
#define SHADERNAME_POSITION_COLOR_U_COLOR "SH_PCUC"
#define SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR "SH_PTCUC"
#define SHADERNAME_POSITION "SH_PUW"
#define SHADERNAME_POSITION_TEXTURE "SH_PTUW"
#define SHADERNAME_POSITION_COLOR "SH_PCUW"
#define SHADERNAME_POSITION_TEXTURE_COLOR "SH_PTCUW"

class GPURenderProgram;

class GPUProgramCache
{
public: 
	GPUProgramCache();
	static GPUProgramCache* getInstance();
	static void purgeSharedShaderCache();
	void addProgram(const std::string &name, GPUProgramBase* shader);
	void removeProgram(const std::string &name);
	GPUProgramBase* getProgram(const std::string &name);
	GPURenderProgram* getRenderProgram(const std::string &name);
	void updateRenderProgramsBuiltinUniforms(const kmMat4 &projection);
private:
	void loadDefaultShaders();
	std::unordered_map<std::string, GPUProgramBase*> _programLibrary;
	static GPUProgramCache* _instance;
};