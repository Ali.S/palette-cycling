#pragma once
#include "CommonHeaders.h"
#include "GLGlobals.h"
#include "Image.h"
#include "Texture2D.h"
#include "GPUProgramCache.h"
#include "GPURenderProgram.h"
#include <vec2.hpp>
#include "simple_svg_1.0.0.hpp"

class Texture2D;
class Visualizer
{
public:
	Visualizer(kmScalar scale = 1, bool dumpSVG = false);
	~Visualizer();

	void bindImage(const Image &image, int unit = 0);
	void renderImage(const Image &image, int unit = 0);
	void renderTexture(float width, float height, Texture2D *texture, int textureUnit = 0);
	void renderTextureUnit(float width, float height, int textureUnit = 0);

	void useDefaultShaders(bool use);

	void setTranslate(Vec2 off);
	template <typename T> void bufferData(const T* arr, GLsizei count);
	template <typename T> void renderLineStrip(const T *arr, const Vec2 T::* positionOffset, const Color4F  T::*colorOffset, GLsizei count);
	template <typename T> void renderLines(const T *arr, const Vec2 T::* positionOffset, const Color4F  T::*colorOffset, GLsizei count);
	template <typename T, typename idx> void renderIndexedLineLoop(const Vec2 T::* positionOffset, const Color4F T::*colorOffset, const idx *indices, GLsizei count);
	template <typename T, typename idx> void renderIndexedLines(const Vec2 T::* positionOffset, const Color4F T::*colorOffset, const idx *indices, GLsizei count);
	template <typename T, typename idx> void renderIndexedLineStrip(const Vec2 T::* positionOffset, const Color4F T::*colorOffset, const idx *indices, GLsizei count);
	template <typename T> void renderPointList(const T *arr, const Vec2 T::* positionOffset, const Color4F T::* colorOffset, GLsizei count);
	template <typename T> void renderPointList(const T *arr, const Vec2 T::* positionOffset, const Color4F &color, GLsizei count);
	template <typename T> void renderTriangles(const T *arr, const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const Vec2 T::* texcoordOffset, GLsizei count);
	template <typename T> void renderTriangles(const T *arr, const Vec2 T::* positionOffset, const Color4F T::* colorOffset, GLsizei count);
	template <typename T, typename idx> void renderIndexTriangles(const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const idx* indices, GLsizei count);
	template <typename T, typename idx> void renderIndexTriangles(const Vec3 T::* normalOffset, const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const idx* indices, GLsizei count);
	template <typename T, typename idx> void renderIndexTriangles(const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const Vec2 T::* texcoordOffset, const idx* indices, GLsizei count);
	template <typename T, typename idx> void renderIndexTriangles(const Vec2 T::* positionOffset, const Vec2 T::* texcoordOffset, const idx* indices, GLsizei count);

	template<typename idx> struct idx_gl_helper;
	template<>
	struct idx_gl_helper<uint8_t> {
		static constexpr GLenum type = GL_UNSIGNED_BYTE;
	};
	template<>
	struct idx_gl_helper<uint16_t> {
		static constexpr GLenum type = GL_UNSIGNED_SHORT;
	};
	template<>
	struct idx_gl_helper<uint32_t> {
		static constexpr GLenum type = GL_UNSIGNED_INT;
	};

	void display(bool clear = true, bool clearBackBuffer = false);
	void display(const std::string &svgDumpPath, bool clear = true, bool clearBackBuffer = false);

	Vec2 project(Vec3 point);
	inline Vec2 project(Vec2 point) {
		project(Vec3(point.x, point.y, 0));
	}

	Vec2 unproject(Vec2 point);
	
	Image screenShot();

protected:

	void drawRect(int unit);
	union {
		struct { GLuint _rectBuffer, _arrayBuffer, _elementBuffer; };
		GLuint _glBuffers[3];
	};
	std::vector<uint8_t> _arrayData;

	bool _useDefaultShaders;
	std::vector<Texture2D *>_texture;
	kmScalar _scale;
	Vec2 _translate;
	kmMat4 _transform, _projection;

	std::unique_ptr<svg::Document> _svgDoc;
};

template <class T>
constexpr GLenum getGLType();

template <>
constexpr GLenum getGLType<float>()
{
	return GL_FLOAT;
}

template <>
constexpr GLenum getGLType<double>()
{
	return GL_DOUBLE;
}
template<typename T>
inline void Visualizer::bufferData(const T * arr, GLsizei count)
{
	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(T), arr, GL_STREAM_DRAW);
	if (_svgDoc)
	{
		_arrayData.resize(count * sizeof(T));
		memcpy(_arrayData.data(), arr, count * sizeof(T));
	}
}

template<typename T>
inline void Visualizer::renderLineStrip(const T * arr, const Vec2  T::*positionOffset, const Color4F T::*colorOffset, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();
	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}


	bufferData(arr, count);
	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, false);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	
	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, getGLType<kmScalar>(), 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, getGLType<kmScalar>(), 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawArrays(GL_LINE_STRIP, 0, count);

	if (_svgDoc)
	{
		for (size_t i = 1; i < count; i++)
		{
			svg::Line line(
				reinterpret_cast<T*>(_arrayData.data())[i - 1].*positionOffset,
				reinterpret_cast<T*>(_arrayData.data())[i].*positionOffset,
				svg::Stroke(1, (reinterpret_cast<T*>(_arrayData.data())[i - 1].*colorOffset + reinterpret_cast<T*>(_arrayData.data())[i].*colorOffset) / 2)
			);
			line.offset(_translate);
			*_svgDoc << line;
		}
	}
}

template<typename T>
inline void Visualizer::renderLines(const T * arr, const Vec2  T::*positionOffset, const Color4F T::*colorOffset, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();
	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());

	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	bufferData(arr, count);
	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, false);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();


	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, getGLType<kmScalar>(), 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, getGLType<kmScalar>(), 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawArrays(GL_LINES, 0, count);

	if (_svgDoc)
	{
		for (size_t i = 1; i < count; i+=2)
		{
			svg::Line line(
				reinterpret_cast<T*>(_arrayData.data())[i - 1].*positionOffset,
				reinterpret_cast<T*>(_arrayData.data())[i].*positionOffset,
				svg::Stroke(1, (reinterpret_cast<T*>(_arrayData.data())[i - 1].*colorOffset + reinterpret_cast<T*>(_arrayData.data())[i].*colorOffset) / 2)
			);
			line.offset(_translate);
			*_svgDoc << line;
		}
	}
}


template<typename T, typename idx_t>
inline void Visualizer::renderIndexedLineLoop(const Vec2  T::*positionOffset, const Color4F T::*colorOffset, const idx_t* indices, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());

	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}
	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setElementArrayBuffer(_elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(*indices), indices, GL_STREAM_DRAW);

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, getGLType<kmScalar>(), 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, getGLType<kmScalar>(), 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawElements(GL_LINE_LOOP, count, idx_gl_helper<idx_t>::type, 0);

	if (_svgDoc)
	{
		for (size_t i = 0; i < count; i++)
		{
			int i0 = indices[i];
			int i1 = indices[(i + 1) % count];
			svg::Line line(
				reinterpret_cast<T*>(_arrayData.data())[i0].*positionOffset,
				reinterpret_cast<T*>(_arrayData.data())[i1].*positionOffset,
				svg::Stroke(1, (reinterpret_cast<T*>(_arrayData.data())[i0].*colorOffset + reinterpret_cast<T*>(_arrayData.data())[i1].*colorOffset) / 2)
			);
			line.offset(_translate);
			*_svgDoc << line;
		}
	}
}

template<typename T, typename idx_t>
inline void Visualizer::renderIndexedLines(const Vec2  T::*positionOffset, const Color4F T::*colorOffset, const idx_t* indices, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());

	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}
	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, false);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setElementArrayBuffer(_elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(*indices), indices, GL_STREAM_DRAW);

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawElements(GL_LINES, count, idx_gl_helper<idx_t>::type, 0);
	
	if (_svgDoc)
	{
		for (size_t i = 1; i < count; i += 2)
		{
			int i0 = indices[i-1];
			int i1 = indices[i];
			svg::Line line(
				reinterpret_cast<T*>(_arrayData.data())[i0].*positionOffset,
				reinterpret_cast<T*>(_arrayData.data())[i1].*positionOffset,
				svg::Stroke(1, (reinterpret_cast<T*>(_arrayData.data())[i0].*colorOffset + reinterpret_cast<T*>(_arrayData.data())[i1].*colorOffset) / 2)
			);
			line.offset(_translate);
			*_svgDoc << line;
		}
	}
}

template<typename T, typename idx_t>
inline void Visualizer::renderIndexedLineStrip(const Vec2  T::*positionOffset, const Color4F T::*colorOffset, const idx_t* indices, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());

	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, false);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setElementArrayBuffer(_elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(*indices), indices, GL_STREAM_DRAW);

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawElements(GL_LINE_STRIP, count, idx_gl_helper<idx_t>::type, 0);

	if (_svgDoc)
	{
		for (size_t i = 1; i < count; i ++)
		{
			int i0 = indices[i - 1];
			int i1 = indices[i];
			svg::Line line(
				reinterpret_cast<T*>(_arrayData.data())[i0].*positionOffset,
				reinterpret_cast<T*>(_arrayData.data())[i1].*positionOffset,
				svg::Stroke(1, (reinterpret_cast<T*>(_arrayData.data())[i0].*colorOffset + reinterpret_cast<T*>(_arrayData.data())[i1].*colorOffset) / 2)
			);
			line.offset(_translate);
			*_svgDoc << line;
		}
	}
}


template<typename T>
inline void Visualizer::renderPointList(const T* arr, const Vec2 T::*positionOffset, const Color4F T::*colorOffset, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());

	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	bufferData(arr, count);

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawArrays(GL_POINTS, 0, count);

	if (_svgDoc)
	{
		for (size_t i = 0; i < count; i++)
		{
			svg::Circle cir(
				reinterpret_cast<T*>(_arrayData.data())[i].*positionOffset,
				4,
				svg::Fill(svg::Color(reinterpret_cast<T*>(_arrayData.data())[i].*colorOffset))
			);
			cir.offset(_translate);
			*_svgDoc << cir;
		}
	}
}

template<typename T>
inline void Visualizer::renderPointList(const T* arr, const Vec2 T::*positionOffset, const Color4F &color, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_U_COLOR)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());

	if (shader)
	{
		shader->setUniform(shader->getPVLocation(), _projection);
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getUniformID(Uniform_Color_Name), color.r, color.g, color.b, color.a);
	}

	bufferData(arr, count);

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);

	glDrawArrays(GL_POINTS, 0, count);

	if (_svgDoc)
	{
		auto fill = svg::Fill(svg::Color(color));
		for (size_t i = 0; i < count; i++)
		{
			svg::Circle cir(
				reinterpret_cast<T*>(_arrayData.data())[i].*positionOffset,
				4,
				fill
			);
			cir.offset(_translate);
			*_svgDoc << cir;
		}
	}
}

template<typename T>
inline void Visualizer::renderTriangles(const T *arr, const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const Vec2 T::* texcoordOffset, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_TEXTURE)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	bufferData(arr, count);

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_TextureCoordinates, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*texcoordOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawArrays(GL_TRIANGLES, 0, count);

	if (_svgDoc)
	{
		for (size_t i = 2; i < count; i+=3)
		{
			Color4F color((
				reinterpret_cast<T*>(_arrayData.data())[i - 2].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i - 1].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i].*colorOffset) / 3);
			svg::Polygon tri(svg::Stroke(-1), svg::Fill(color));
			tri.points.resize(3);
			tri.point[0] = reinterpret_cast<T*>(_arrayData.data())[i - 2].*positionOffset;
			tri.point[1] = reinterpret_cast<T*>(_arrayData.data())[i - 1].*positionOffset;
			tri.point[2] = reinterpret_cast<T*>(_arrayData.data())[i].*positionOffset;
			tri.offset(_translate);
			*_svgDoc << tri;
		}
	}
}

template<typename T>
inline void Visualizer::renderTriangles(const T *arr, const Vec2 T::* positionOffset, const Color4F T::* colorOffset, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	bufferData(arr, count);

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawArrays(GL_TRIANGLES, 0, count);

	if (_svgDoc)
	{
		for (size_t i = 2; i < count; i += 3)
		{
			Color4F color{
				(
				reinterpret_cast<T*>(_arrayData.data())[i - 2].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i - 1].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i].*colorOffset) / 3
			};
			svg::Polygon tri = svg::Polygon(svg::Fill(svg::Color(color)));
			tri << reinterpret_cast<T*>(_arrayData.data())[i - 2].*positionOffset;
			tri << reinterpret_cast<T*>(_arrayData.data())[i - 1].*positionOffset;
			tri << reinterpret_cast<T*>(_arrayData.data())[i].*positionOffset;
			tri.offset(_translate);
			*_svgDoc << tri;
		}
	}
}

template <typename T, typename idx_t>
void Visualizer::renderIndexTriangles(const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const Vec2 T::* texcoordOffset, const idx_t* indices, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_TEXTURE_COLOR)->use();
	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setElementArrayBuffer(_elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(*indices), indices, GL_STREAM_DRAW);

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_TextureCoordinates, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*texcoordOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawElements(GL_TRIANGLES, count, idx_gl_helper<idx_t>::type, 0);

	if (_svgDoc)
	{
		for (size_t i = 2; i < count; i += 3)
		{
			auto i0 = indices[i - 2];
			auto i1 = indices[i - 1];
			auto i2 = indices[i - 0];
			Color4F color((
				reinterpret_cast<T*>(_arrayData.data())[i0].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i1].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i2].*colorOffset) / 3);
			svg::Polygon tri(svg::Fill(color), svg::Stroke(-1));
			tri << reinterpret_cast<T*>(_arrayData.data())[i0].*positionOffset;
			tri << reinterpret_cast<T*>(_arrayData.data())[i1].*positionOffset;
			tri << reinterpret_cast<T*>(_arrayData.data())[i2].*positionOffset;
			tri.offset(_translate);
			*_svgDoc << tri;
		}
	}
}

template <typename T, typename idx_t>
void Visualizer::renderIndexTriangles(const Vec2 T::* positionOffset, const Vec2 T::* texcoordOffset, const idx_t* indices, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_TEXTURE)->use();
	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setElementArrayBuffer(_elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(*indices), indices, GL_STREAM_DRAW);

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_TextureCoordinates, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*texcoordOffset).x);

	glDrawElements(GL_TRIANGLES, count, idx_gl_helper<idx_t>::type, 0);
	
	if (_svgDoc)
	{
		for (size_t i = 2; i < count; i += 3)
		{
			auto i0 = indices[i - 2];
			auto i1 = indices[i - 1];
			auto i2 = indices[i - 0];
			svg::Polygon tri = svg::Polygon(svg::Stroke(svg::Color::Black));
			tri << reinterpret_cast<T*>(_arrayData.data())[i0].*positionOffset;
			tri << reinterpret_cast<T*>(_arrayData.data())[i1].*positionOffset;
			tri<< reinterpret_cast<T*>(_arrayData.data())[i2].*positionOffset;
			tri.offset(_translate);
			*_svgDoc << tri;
		}
	}
}

template <typename T, typename idx_t>
void Visualizer::renderIndexTriangles(const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const idx_t* indices, GLsizei count)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_COLOR)->use();
	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setElementArrayBuffer(_elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(*indices), indices, GL_STREAM_DRAW);

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawElements(GL_TRIANGLES, count, idx_gl_helper<idx_t>::type, 0);

	if (_svgDoc)
	{
		for (size_t i = 2; i < count; i += 3)
		{
			auto i0 = indices[i - 2];
			auto i1 = indices[i - 1];
			auto i2 = indices[i - 0];
			Color4F color((
				reinterpret_cast<T*>(_arrayData.data())[i0].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i1].*colorOffset +
				reinterpret_cast<T*>(_arrayData.data())[i2].*colorOffset) / 3);

			svg::Polygon tri = svg::Polygon(svg::Fill(svg::Color(color)));
			tri << reinterpret_cast<T*>(_arrayData.data())[i0].*positionOffset;
			tri << reinterpret_cast<T*>(_arrayData.data())[i1].*positionOffset;
			tri << reinterpret_cast<T*>(_arrayData.data())[i2].*positionOffset;
			tri.offset(_translate);
			*_svgDoc << tri;
		}
	}
}

template <typename T, typename idx_t>
void Visualizer::renderIndexTriangles(const Vec3 T::* normalOffset, const Vec2 T::* positionOffset, const Color4F T::* colorOffset, const idx_t* indices, GLsizei count)
{
	if (_useDefaultShaders)
		return;
	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getPVLocation(), _projection);
	}


	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Normal, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Color, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	GLGlobals::getInstance()->setElementArrayBuffer(_elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(*indices), indices, GL_STREAM_DRAW);

	GLGlobals::getInstance()->setArrayBuffer(_arrayBuffer);
	glVertexAttribPointer(VertexAttribute_Normal, 3, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*normalOffset).x);
	glVertexAttribPointer(VertexAttribute_Position, 2, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*positionOffset).x);
	glVertexAttribPointer(VertexAttribute_Color, 4, GL_FLOAT, 0, sizeof(T), &(reinterpret_cast<T*>(0)->*colorOffset).channel);

	glDrawElements(GL_TRIANGLES, count, idx_gl_helper<idx_t>::type, 0);
}