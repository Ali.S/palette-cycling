#pragma once
#include "GPUProgramBase.h"

class GPURenderProgram : public GPUProgramBase
{	
public:
	GPURenderProgram();
	GPURenderProgram(const char* vertexShader, const char* fragmentShader);
	~GPURenderProgram();
	bool compile(const char* vertexShader, const char* fragmentShader);
private:
	SYNTHESIZE_READONLY(int, _PVLocation, PVLocation);
	SYNTHESIZE_READONLY(int, _MLocation, MLocation);
	SYNTHESIZE_READONLY(int, _TextureLocation, TextureLocation);
};