#include "Image.h"
#include <png.h>
#include <fstream>

/* Check for the older version of libpng */
#if (PNG_LIBPNG_VER_MAJOR == 1) 
#if (PNG_LIBPNG_VER_MINOR < 4)
#define LIBPNG_VERSION_12
typedef png_bytep png_const_bytep;
#endif
#if (PNG_LIBPNG_VER_MINOR < 6)
typedef png_structp png_const_structrp;
typedef png_infop png_const_inforp;
#endif
#endif

/* See if an image is contained in a data source */
int IMG_isPNG(std::ifstream file)
{
	std::streampos start;
	int is_PNG;
	char magic[4];

	if (!file) {
		return 0;
	}

	start = file.tellg();
	is_PNG = 0;
	if (file.readsome(magic, sizeof(magic)) == sizeof(magic)) {
		if (magic[0] == 0x89 &&
			magic[1] == 'P' &&
			magic[2] == 'N' &&
			magic[3] == 'G') {
			is_PNG = 1;
		}
	}
	file.seekg(start);
	return is_PNG;
}

/* Load a PNG type image from an SDL datasource */
static void image_png_read_data(png_structp ctx, png_bytep area, png_size_t size)
{
	std::istream *src;

	src = reinterpret_cast<std::istream*>(png_get_io_ptr(ctx));
	src->read(reinterpret_cast<char*>(area), size);
	
}

bool Image::loadPNG(std::istream &src)
{

	uint64_t start;
	const char *error;
	
	png_structp png_ptr;
	png_infop info_ptr;
	png_uint_32 width, height;
	int bit_depth, color_type, interlace_type, num_channels, depth;
	
	png_bytep *volatile row_pointers;
	int row;
	int ckey = -1;
	png_color_16 *transv;

	if (!src) {
		/* The error message has been set in SDL_RWFromFile */
		return false;
	}

	start = src.tellg();

	/* Initialize the data we will clean up when we're done */
	error = NULL;
	png_ptr = NULL; info_ptr = NULL; row_pointers = NULL;

	/* Create the PNG loading context structure */
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
		NULL, NULL, NULL);
	if (png_ptr == NULL) {
		error = "Couldn't allocate memory for PNG file or incompatible PNG dll";
		goto done;
	}

	/* Allocate/initialize the memory for image information.  REQUIRED. */
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		error = "Couldn't create image information for PNG file";
		goto done;
	}

	/* Set error handling if you are using setjmp/longjmp method (this is
	* the normal method of doing things with libpng).  REQUIRED unless you
	* set up your own error handlers in png_create_read_struct() earlier.
	**/

#ifndef LIBPNG_VERSION_12
	if (setjmp(*png_set_longjmp_fn(png_ptr, longjmp, sizeof(jmp_buf))))
#else
	if (setjmp(png_ptr->jmpbuf))
#endif

	{
		error = "Error reading the PNG file.";
		goto done;
	}

	/* Set up the input control */
	png_set_read_fn(png_ptr, &src, image_png_read_data);

	/* Read PNG header info */
	png_read_info(png_ptr, info_ptr);
	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
		&color_type, &interlace_type, NULL, NULL);

	/* tell libpng to strip 16 bit/color files down to 8 bits/color */
	png_set_scale_16(png_ptr);

	/* Extract multiple pixels with bit depths of 1, 2, and 4 from a single
	* byte into separate bytes (useful for paletted and grayscale images).
	*/
	png_set_packing(png_ptr);

	/* scale greyscale values to the range 0..255 */
	if (color_type == PNG_COLOR_TYPE_GRAY)
		png_set_expand_gray_1_2_4_to_8(png_ptr);

	/* For images with a single "transparent colour", set colour key;
	if more than one index has transparency, or if partially transparent
	entries exist, use full alpha channel */
	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) {
		int num_trans;
		png_bytep trans;
		png_get_tRNS(png_ptr, info_ptr, &trans, &num_trans, &transv);
		if (color_type == PNG_COLOR_TYPE_PALETTE) {
			error = "Can not load pallet PNG files";
			goto done;
		}
		else {
			ckey = 0; /* actual value will be set later */
		}
	}

	/*if (color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png_ptr);*/
	if (color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb(png_ptr);


	png_read_update_info(png_ptr, info_ptr);

	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
		&color_type, &interlace_type, NULL, NULL);

	/* Allocate the SDL surface to hold the image */

	num_channels = png_get_channels(png_ptr, info_ptr);
	depth = png_get_bit_depth(png_ptr, info_ptr);

	Format format;
	switch (num_channels)
	{
	case 1:
		format = Format::I;	
		break;
	case 3:
		format = Format::RGB;
		break;
	case 4:
		format = Format::RGBA;
		break;
	default:
		error = "unknown number of channels";
		break;
	}
	alloc(width, height, format);
	if (_data.size() == 0)
	{
		error = "Out of memory";
		goto done;
	}

	/* Create the array of pointers to image data */
	row_pointers = new png_bytep[height];
	if ((row_pointers == NULL)) {
		error = "Out of memory";
		goto done;
	}
	for (row = 0; row < (int)height; row++) {
		row_pointers[row] = reinterpret_cast<png_bytep>(_data.data() + (height - row - 1) * _width * num_channels);
	}

	/* Read the entire image in one go */
	png_read_image(png_ptr, row_pointers);
	
	/* and we're done!  (png_read_end() can be omitted if no processing of
	* post-IDAT text/time/etc. is desired)
	* In some cases it can't read PNG's created by some popular programs (ACDSEE),
	* we do not want to process comments, so we omit png_read_end

	lib.png_read_end(png_ptr, info_ptr);
	*/

done:   /* Clean up and return */
	if (png_ptr) {
		png_destroy_read_struct(&png_ptr,
			info_ptr ? &info_ptr : (png_infopp)0,
			(png_infopp)0);
	}
	if (row_pointers) {
		delete []row_pointers;
	}
	if (error) {
		src.seekg(start);
		if (_data.size())
			_data.clear();
		return false;
	}
	return true;
}

static void image_png_write_data(png_structp ctx, png_bytep area, png_size_t size)
{
	std::ostream &out = *reinterpret_cast<std::ostream*>(png_get_io_ptr(ctx));
	out.write(reinterpret_cast<char*>(area), size);
}

static void image_png_flush(png_structp ctx)
{
	std::ostream &out = *reinterpret_cast<std::ostream*>(png_get_io_ptr(ctx));
	out.flush();
}

bool Image::savePNG(std::ostream &stream) const
{
	png_bytepp row_pointers = NULL;
	png_structp png = NULL;
	png_infop info = NULL;
	png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png) goto done;

	info = png_create_info_struct(png);
	if (!info) goto done;

#ifndef LIBPNG_VERSION_12
	if (setjmp(*png_set_longjmp_fn(png, longjmp, sizeof(jmp_buf))))
#else
	if (setjmp(png_ptr->jmpbuf))
#endif
	{
		goto done;
	}


	png_set_write_fn(png, &stream, image_png_write_data, image_png_flush);

	// Output is 8bit depth, RGBA format.
	int pngColorType, num_channels;
	switch (_format)
	{
	case Format::I: pngColorType = PNG_COLOR_TYPE_GRAY; num_channels = 1; break;
	case Format::RGB: pngColorType = PNG_COLOR_TYPE_RGB; num_channels = 3; break;
	case Format::RGBA: pngColorType = PNG_COLOR_TYPE_RGBA; num_channels = 4; break;
	default: goto done;
	}
	png_set_IHDR(
		png,
		info,
		_width, _height,
		8,
		pngColorType,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT
	);
	png_write_info(png, info);
	
	row_pointers = new png_bytep[_height];
	
	for (int row = 0; row < (int)_height; row++) {
		row_pointers[row] = (png_bytep)(_data.data() + (_height - row - 1) * _width * num_channels);
	}

	png_write_image(png, const_cast<png_bytepp>(row_pointers));

done:

	if (png) {
		png_destroy_write_struct(&png,
			info ? &info : (png_infopp)0);
	}

	free(row_pointers);
	return true;
}