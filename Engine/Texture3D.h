#pragma once
#include "Object.h"
#include "Texture.h"

class Texture3D : public Texture
{
public:
	Texture3D();
	~Texture3D();
	void setWrapMode(GLenum wrapModeS, GLenum wrapModeT, GLenum wrapModeR);
	void setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter);
	unsigned getWidth();
	unsigned getHeight();
	unsigned getDepth();
	bool bufferData(int mode, unsigned width, unsigned height, unsigned depth, void* data, bool reverse = false);
	GLenum getType();
private:
	unsigned width_, height_, depth_;
	GLenum minifyFilter_, magnifyFilter_, wrapModeS_, wrapModeT_, wrapModeR_;
};