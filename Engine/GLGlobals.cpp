#include "CommonHeaders.h"
#include "GLGlobals.h"
#include "GPUProgramBase.h"
#include "RenderTexture.h"
#include "Texture.h"
#include <thread>
#ifdef WIN32
#include "GL/wglew.h"
#endif

GLGlobals* GLGlobals::_sharedGlobals = nullptr;

GLGlobals::GLGlobals(size_t width, size_t height, const char* windowTitle)
{
	setupResult_ = setupEnviorments(width, height, windowTitle);
	_arrayBuffer = 0;
	_elementArrayBuffer = 0;
	_transformFeedbackBuffer = 0;
	_vertexAttributeEnabledStatus = 0;
	_newVertexAttributeEnabledStatus = 0;
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &_textureUnitCount);
	_textureIDCache = new Texture*[_textureUnitCount];
	for(int i=0;i<_textureUnitCount;i++)
		_textureIDCache[i] = nullptr;
	_activeTextureUnit = 0;
	_inUseGPUProgram = nullptr;
	_renderTarget = nullptr;
}

GLGlobals::~GLGlobals()
{
	delete []_textureIDCache;
}

GLGlobals * GLGlobals::createInstance(size_t width, size_t height, const char * windowTitle)
{
	assert(_sharedGlobals == nullptr);
	_sharedGlobals = new GLGlobals(width, height, windowTitle?windowTitle : "Renderer");
	return _sharedGlobals;
}

GLGlobals* GLGlobals::getInstance()
{
	if (_sharedGlobals == nullptr)
		_sharedGlobals = new GLGlobals(1920, 1080, "Renderer");
	return _sharedGlobals;
}

bool GLGlobals::setupEnviorments(size_t width, size_t height, const char* windowTitle)
{
	int step = 0,res = 0;
	while(true)
	{
#ifdef WIN32
		res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
        res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2 );
		//res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
		res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
#else
		res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
		res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
#endif
		res += SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		res += SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		res += SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		res += SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		res += SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);

		if(res < 0)
		{
			Log("Error setting SDL_GL_Attributes: %s\n", SDL_GetError());
			break;
		}
		
		window_ = SDL_CreateWindow( 
			"Renderer",         
			100,  
			100,  
			width,                      
			height,                      
			SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL //|SDL_WINDOW_BORDERLESS
		);

		if(window_ == nullptr)
		{   
			Log("Could not create window: %s\n", SDL_GetError());		
			break;
		} 
		else
			step++;
			

		glContext_ = SDL_GL_CreateContext(window_);

		if (glContext_ ==nullptr)
		{
			Log("Could not create GLContext: %s\n", SDL_GetError());			
			break;
		}
		else
			step++;
		
		int major,minor,profile;
		res += SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &major);
		res += SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &minor);
		res += SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &profile);		
		if(res < 0)
		{
			Log("Error getting SDL_GL_Attributes: %s\n", SDL_GetError());
			break;
		}
		Log("created GLContext with version %d.%d\n",major,minor);
		if (profile == SDL_GL_CONTEXT_PROFILE_CORE)
			Log("GLContext was created with profile: %s\n","Core");
		if (profile == SDL_GL_CONTEXT_PROFILE_COMPATIBILITY)
			Log("GLContext was created with profile: %s\n","Compatibility");
		if (profile == SDL_GL_CONTEXT_PROFILE_ES)
			Log("GLContext was created with profile: %s\n","GLES");
#if TARGET_PLATFORM == PLATFORM_WIN32
		glewExperimental = GL_TRUE;
		int glewresult = glewInit();
		if(glewresult != GLEW_OK)
		{
			Log("Error initializing GLEW: %s\n", glewGetErrorString(glewresult));
			break;
		}
		else
#endif
			step++;
		break;
	}
	if (step == 3)
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glDepthMask(true);
/*#ifdef WIN32
		wglSwapIntervalEXT(0);
#endif*/
		glClear(GL_DEPTH_BUFFER_BIT);
		GLuint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		return true;
	}
	if (step > 1)
		SDL_GL_DeleteContext(glContext_);
	if (step > 0)
		SDL_DestroyWindow(window_);
	
	return false;
}

void GLGlobals::swapBuffers()
{
	SDL_GL_SwapWindow(window_);
}

unsigned GLGlobals::getWinWidth()
{
	int w, h;
	SDL_GetWindowSize(window_, &w, &h);
	return w;
}

unsigned GLGlobals::getWinHeight()
{
	int w, h;
	SDL_GetWindowSize(window_, &w, &h);
	return h;
}
void GLGlobals::setArrayBuffer(int buffer)
{
	//if (buffer != _arrayBuffer) this condition can cause crash if gl driver decides to reuse same id after deletion
	{
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		_arrayBuffer = buffer;
	}
}

void GLGlobals::setElementArrayBuffer(int buffer)
{
	//if (buffer != _elementArrayBuffer) this condition can cause crash if gl driver decides to reuse same id after deletion
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
		_elementArrayBuffer = buffer;
	}
}

#if TARGET_PLATFORM == PLATFORM_WIN32
void GLGlobals::setTransformFeedbackBuffer(int buffer)
{
	if (buffer != _transformFeedbackBuffer)
	{
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffer);
		_transformFeedbackBuffer = buffer;
	}
}
#endif

void GLGlobals::setVertexAttributeEnabled(int id, bool status)
{
	if(id < 0)
		return;

	assert(id<64 && "only 64 fields are supported");	
	if(status)
		_newVertexAttributeEnabledStatus |= 1LL<<id;
	else
		_newVertexAttributeEnabledStatus &= ~(1LL<<id);
	
}

void GLGlobals::applyVertexAttributeChanges()
{
	long long bit=1;
	long long changes = _vertexAttributeEnabledStatus ^ _newVertexAttributeEnabledStatus;
	for(unsigned i=0;i<64;i++,bit<<=1)
		if (changes & bit)
			if (_newVertexAttributeEnabledStatus & bit)
				glEnableVertexAttribArray(i);
			else
				glDisableVertexAttribArray(i);
	_vertexAttributeEnabledStatus = _newVertexAttributeEnabledStatus;
	_newVertexAttributeEnabledStatus = 0;
}

void GLGlobals::resetVertexAttributeEnabled()
{
	_newVertexAttributeEnabledStatus = 0;
}

void GLGlobals::bindTexture(Texture* texture, int textureUnit)
{
	if (_textureUnitCount < 0 || textureUnit > _textureUnitCount)
		return;
	if (_textureIDCache[textureUnit] != texture)
	{
		
		if (textureUnit != _activeTextureUnit)
		{
			_activeTextureUnit = textureUnit;
			glActiveTexture(GL_TEXTURE0 + textureUnit);
		}
		if (texture)
			glBindTexture(texture->getType(), texture->getTextureID());
		_textureIDCache[textureUnit] = texture;
	}
}

void GLGlobals::setRenderTarget(RenderTexture* target)
{
	if(target != _renderTarget)
	{		
		if(target)
		{
			glViewport(0, 0,target->getWidth(), target->getHeight());
			glBindFramebuffer(GL_FRAMEBUFFER, target->getFrameBufferID());
		}
		else
		{
			glViewport(0, 0, getWinWidth(), getWinHeight());
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
		_renderTarget = target;
	}
}

RenderTexture* GLGlobals::getRenderTarget()
{
	return _renderTarget;
}

int GLGlobals::getActiveTextureUnit()
{
	return _activeTextureUnit;
}
