#pragma once

#include <vector>

class Object;

class GarbageCollector
{
public:
	GarbageCollector();
	~GarbageCollector();
	static GarbageCollector* sharedGarbageCollector();
	static void purgeGarbageCollector();
	void cleanUp();
private:
	static GarbageCollector* sharedGarbageCollector_;
	friend class Object;
	void pushObject(Object* obj);
	std::vector<Object*> Objects_;
};