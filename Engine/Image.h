#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <ostream>
#include "Color.h"

class Image
{
public:
	enum class Format
	{
		I,
		RGB,
		RGBA
	};

	Image();
	~Image();

	bool load(const std::string &path);
	bool save(const std::string &path) const;

	uint8_t * data();
	const uint8_t * data() const;
	Format format() const;
	size_t width() const;
	size_t height() const;

	inline Color4B getPixel(size_t x, size_t y) const { 
		if (x >= _width || y >= _height)
			return Color4B();
		return (this->*_getPixel)(x, y); 
	}
	inline void putPixel(size_t x, size_t y, const Color4B &c) { 
		if (x >= _width || y >= _height)
			return ;
		(this->*_putPixel)(x, y, c);
	}

	void SetData(size_t width, size_t height, Format format, uint8_t *data);
	void alloc(size_t width, size_t height, Format format);

	void clear(const Color4B &c);

protected:

	Color4B getPixel_RGBA(size_t x, size_t y) const;
	void putPixel_RGBA(size_t x, size_t y, const Color4B &c);
	Color4B getPixel_RGB(size_t x, size_t y) const;
	void putPixel_RGB(size_t x, size_t y, const Color4B &c);
	Color4B getPixel_I(size_t x, size_t y) const;
	void putPixel_I(size_t x, size_t y, const Color4B &c);

	Color4B (Image::*_getPixel)(size_t x, size_t y) const;
	void (Image::*_putPixel)(size_t x, size_t y, const Color4B &c);

	bool loadPNG(std::istream &src);
	bool savePNG(std::ostream &dst) const;

	bool loadBMP(std::istream &src);
	bool saveBMP(std::ostream &dst) const;

	std::vector<uint8_t> _data;
	size_t _width, _height;
	Format _format;

};
