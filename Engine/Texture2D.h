#pragma once
#include "Object.h"
#include "Texture.h"

class Texture2D : public Texture
{
public:
	Texture2D();
	~Texture2D();
	void setWrapMode(GLenum wrapModeS, GLenum wrapModeT);
	void setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter);
	unsigned getWidth();
	unsigned getHeight();	
	bool bufferData(int mode, unsigned width, unsigned height, void* data, bool reverse = false);
	GLenum getType();
private:	
	unsigned width_,height_;
	GLenum minifyFilter_, magnifyFilter_, wrapModeS_, wrapModeT_;
};