#include "Image.h"
#include <fstream>


Image::Image() : _width(0), _height(0), _getPixel(nullptr), _putPixel(nullptr)
{

}


Image::~Image()
{

}

bool Image::load(const std::string & path)
{
	if (path.substr(path.length() - 4) == ".png")
	{
		std::ifstream stream(path.c_str(), std::ios::binary);
		if (!stream)
		{
			std::cout << path << "... does not exist!\n";
			return false;
		}
		return loadPNG(stream);
	}
	if (path.substr(path.length() - 4) == ".bmp")
	{
		std::ifstream stream(path.c_str(), std::ios::binary);
		return loadBMP(stream);
	}
	return false;
}

bool Image::save(const std::string & path) const
{
	if (path.substr(path.length() - 4) == ".bmp")
	{
		std::ofstream stream(path.c_str(), std::ios::binary);
		return saveBMP(stream);
	}
	if (path.substr(path.length() - 4) == ".png")
	{
		std::ofstream stream(path.c_str(), std::ios::binary);
		return savePNG(stream);
	}
	return false;
}

uint8_t * Image::data()
{
	if (_data.size())
		return _data.data();
	else
		return nullptr;
}

const uint8_t * Image::data() const
{
	if (_data.size())
		return _data.data();
	else
		return nullptr;
}

Image::Format Image::format() const
{
	return _format;
}

size_t Image::width() const
{
	return _width;
}

size_t Image::height() const
{
	return _height;
}

void Image::SetData(size_t width, size_t height, Image::Format format, uint8_t * data)
{
	alloc(width, height, format);
	if (_data.size() > 0)
		memcpy(_data.data(), data, _data.size());
}

void Image::alloc(size_t width, size_t height, Image::Format format)
{
	_width = width;
	_height = height;
	_format = format;
	int bpp = 0;
	switch (format)
	{
	case Image::Format::I:
		_getPixel = &Image::getPixel_I;
		_putPixel = &Image::putPixel_I;
		bpp = 1;
		break;
	case Image::Format::RGB:
		_getPixel = &Image::getPixel_RGB;
		_putPixel = &Image::putPixel_RGB;
		bpp = 3;
		break;
	case Image::Format::RGBA:
		_getPixel = &Image::getPixel_RGBA;
		_putPixel = &Image::putPixel_RGBA;
		bpp = 4;
		break;
	default:
		break;
	}
	if (_width * _height * bpp > 0)
		_data.resize(width * height * bpp);
	else
		_data.resize(0);
}

void Image::clear(const Color4B & c)
{
	for (size_t i = 0; i < _width; i++)
		for (size_t j = 0; j < _height; j++)
			(this->*_putPixel)(i, j, c);
}

Color4B Image::getPixel_RGBA(size_t x, size_t y) const
{
	Color4B c;
	
	c.r = _data[(x + y * _width) * 4 + 0];
	c.g = _data[(x + y * _width) * 4 + 1];
	c.b = _data[(x + y * _width) * 4 + 2];
	c.a = _data[(x + y * _width) * 4 + 3];
	return c;
}

void Image::putPixel_RGBA(size_t x, size_t y, const Color4B & c)
{
	_data[(x + y * _width) * 4 + 0] = static_cast<uint8_t>(c.r);
	_data[(x + y * _width) * 4 + 1] = static_cast<uint8_t>(c.g);
	_data[(x + y * _width) * 4 + 2] = static_cast<uint8_t>(c.b);
	_data[(x + y * _width) * 4 + 3] = static_cast<uint8_t>(c.a);
}

Color4B Image::getPixel_RGB(size_t x, size_t y) const
{
	Color4B c;

	c.r = _data[(x + y * _width) * 3 + 0];
	c.g = _data[(x + y * _width) * 3 + 1];
	c.b = _data[(x + y * _width) * 3 + 2];
	c.a = 255;
	return c;
}

void Image::putPixel_RGB(size_t x, size_t y, const Color4B & c)
{
	_data[(x + y * _width) * 3 + 0] = static_cast<uint8_t>(c.r);
	_data[(x + y * _width) * 3 + 1] = static_cast<uint8_t>(c.g);
	_data[(x + y * _width) * 3 + 2] = static_cast<uint8_t>(c.b);
}

Color4B Image::getPixel_I(size_t x, size_t y) const
{
	Color4B c;

	c.r = _data[(x + y * _width)];
	c.g = _data[(x + y * _width)];
	c.b = _data[(x + y * _width)];
	c.a = 1.0;
	return c;
}

void Image::putPixel_I(size_t x, size_t y, const Color4B & c)
{
	_data[(x + y * _width) + 0] = c.avg();
}
