#include "CommonHeaders.h"
#include "Object.h"
#include "GarbageCollector.h"
#include <fstream>
#include <typeinfo>

Object::Object() : _retainCount (0)
{
	GarbageCollector::sharedGarbageCollector()->pushObject(this);
}

Object::~Object()
{
	
}

void Object::retain()
{
	_retainCount ++;
}

void Object::release()
{
	if(--_retainCount == 0)
		delete this;
}

