#pragma once 
#include "Macros.h"

class Object 
{
public:
	Object();
	virtual ~Object();
	void retain();
	void release();
	SYNTHESIZE_READONLY(unsigned, _retainCount, RetainCount);	
};