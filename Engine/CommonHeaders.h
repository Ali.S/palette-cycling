#pragma once

#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <numbers>

#ifdef _WIN32
#define VC_EXTRALEAN
#define NOMINMAX

#include <windows.h>
#include <tchar.h>
#endif

#include <SDL.h>
#include "json.h"
#include <kazmath.h>

#include "Macros.h"
#include "Color.h"

#define PLATFORM_WIN32 1
#define PLATFORM_ANDROID 2
#ifdef _WIN32
#define TARGET_PLATFORM PLATFORM_WIN32
#if _DEBUG
#define DEBUG_LEVEL 1
#else
#define DEBUG_LEVEL 0
#endif
#elif defined(__ANDROID__)
#define TARGET_PLATFORM PLATFORM_ANDROID
#if NDEBUG
#define DEBUG_LEVEL 0
#else
#define DEBUG_LEVEL 1
#endif
#endif



#if TARGET_PLATFORM == PLATFORM_WIN32
#include <GL/glew.h>
#include <GL/GL.h>
#elif TARGET_PLATFORM == PLATFORM_ANDROID
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif
