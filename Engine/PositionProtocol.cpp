#include "PositionProtocol.h"
#include "Camera.h"

PositionProtocol::PositionProtocol() : _transformDirty(true), _parent(nullptr)
{
	_position.x = 0;
	_position.y = 0;
	_position.z = 0;
	_rotation.x = 0;
	_rotation.y = 0;
	_rotation.z = 0;
	_scale.x = 1;
	_scale.y = 1;
	_scale.z = 1;
}

PositionProtocol::~PositionProtocol()
{
	_parent = nullptr;
}

void PositionProtocol::recalculateWorldTransformationMatrix(bool recurse)
{
	if (_parent)
	{
		if (recurse)
			_parent->recalculateWorldTransformationMatrix(true);
		kmMat4Multiply(&_worldTransformationMatrix, &_parent->_worldTransformationMatrix, &getTransformationMatrix());
	}
	else
		kmMat4Assign(&_worldTransformationMatrix, &getTransformationMatrix());
}

const kmMat4& PositionProtocol::getTransformationMatrix()
{
	if (_transformDirty)
	{
		kmMat4 temp;
		kmMat4Identity(&_transformMatrix);
		kmMat4Translation(&temp, _position.x, _position.y, _position.z);
		kmMat4Multiply(&_transformMatrix, &_transformMatrix, &temp);
		kmMat4RotationX(&temp, _rotation.x);
		kmMat4Multiply(&_transformMatrix, &_transformMatrix, &temp);
		kmMat4RotationY(&temp, _rotation.y);
		kmMat4Multiply(&_transformMatrix, &_transformMatrix, &temp);
		kmMat4RotationZ(&temp, _rotation.z);
		kmMat4Multiply(&_transformMatrix, &_transformMatrix, &temp);
		kmMat4Scaling(&temp, _scale.x, _scale.y, _scale.z);
		kmMat4Multiply(&_transformMatrix, &_transformMatrix, &temp);
	}
	_transformDirty = false;
	return _transformMatrix;
}

const kmMat4& PositionProtocol::getWorldTransformationMatrix()
{
	return _worldTransformationMatrix;
}

void PositionProtocol::setPosition(const kmVec3 &position)
{
	setPosition(position.x, position.y, position.z);
}

const kmVec3 &PositionProtocol::getPosition() const
{
	return _position;
}

void PositionProtocol::setRotation(const kmVec3 &Rotation)
{
	setRotation(Rotation.x, Rotation.y, Rotation.z);
}

const kmVec3 &PositionProtocol::getRotation() const
{
	return _rotation;
}

void PositionProtocol::setScale(const kmVec3 &Scale)
{
	setScale(Scale.x, Scale.y, Scale.y);
}

const kmVec3 &PositionProtocol::getScale() const
{
	return _scale;
}

void PositionProtocol::setPosition(kmScalar x, kmScalar y, kmScalar z)
{
	_transformDirty = true;
	_position.x = x;
	_position.y = y;
	_position.z = z;
}

void PositionProtocol::setRotation(kmScalar x, kmScalar y, kmScalar z)
{
	_transformDirty = true;
	_rotation.x = x;
	_rotation.y = y;
	_rotation.z = z;
}

void PositionProtocol::setScale(kmScalar x, kmScalar y, kmScalar z)
{
	_transformDirty = true;
	_scale.x = x;
	_scale.y = y;
	_scale.z = z;
}

PositionProtocol* PositionProtocol::getParent()
{
	return _parent;
}

