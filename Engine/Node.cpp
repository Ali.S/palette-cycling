#include "Node.h"


Node::Node()
{
}


Node::~Node()
{
	for(auto child:_children)
	{
		child->_parent = nullptr;
		dynamic_cast<Object*>(child)->release();
	}
}

void Node::preRender()
{
	RenderableProtocol::preRender();
	for(unsigned i=0;i<_children.size();i++)
		if (dynamic_cast<RenderableProtocol*> (_children[i]))
			dynamic_cast<RenderableProtocol*> (_children[i])->preRender();
}

void Node::render()
{
	if(!getVisible())
		return;
	for(unsigned i=0;i<_children.size();i++)
		if (dynamic_cast<RenderableProtocol*> (_children[i]))
			dynamic_cast<RenderableProtocol*> (_children[i])->render();
}

void Node::postRender()
{
	for(unsigned i=0;i<_children.size();i++)
		if (dynamic_cast<RenderableProtocol*> (_children[i]))
			dynamic_cast<RenderableProtocol*> (_children[i])->postRender();
}

void Node::addChild(Node::ChildArrayType::value_type child)
{
	dynamic_cast<Object*>(child)->retain();
	child->_parent = this;
	_children.push_back(child);
}

void Node::removeChild(Node::ChildArrayType::iterator child)
{	
	dynamic_cast<Object*>(*child)->release();
	(*child)->_parent = nullptr;
	_children.erase(child);	
}

const Node::ChildArrayType& Node::getChildren()
{
	return _children;
}

Node::ChildArrayType::iterator Node::childrenBegin()
{
	return _children.begin();
}

Node::ChildArrayType::iterator Node::childrenEnd()
{
	return _children.end();
}

Node::ChildArrayType::const_iterator Node::childrenBegin() const
{
	return _children.begin();
}

Node::ChildArrayType::const_iterator Node::childrenEnd() const
{
	return _children.end();
}