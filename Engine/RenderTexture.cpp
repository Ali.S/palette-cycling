#include "CommonHeaders.h"
#include "RenderTexture.h"
#include "GLGlobals.h"
#define checkGL_ERROR() do{int err = glGetError(); assert(err == GL_NO_ERROR);}while(false)
RenderTexture::RenderTexture() : depthTexture_(nullptr), stencilTexture_(nullptr)
{
	glGenFramebuffers(1, &frameBufferID_);
}

RenderTexture::RenderTexture(unsigned width, unsigned height, int colorAttachments, bool depthAttachment, bool stencilAttachment) : depthTexture_(nullptr), stencilTexture_(nullptr)
{
	glGenFramebuffers(1, &frameBufferID_);
	init(width,height,colorAttachments,depthAttachment,stencilAttachment);
}

RenderTexture::~RenderTexture()
{
	for (size_t i = 0; i < colorTextures_.size(); i++)
		colorTextures_[i]->release();
	if (depthTexture_)
		depthTexture_->release();
	if (stencilTexture_)
		stencilTexture_->release();
	glDeleteFramebuffers(1, &frameBufferID_);
}
bool RenderTexture::init(unsigned width, unsigned height, unsigned colorAttachments, bool depthAttachment, bool stencilAttachment)
{
	if((colorAttachments > 0 || depthAttachment || stencilAttachment) == false)
	{
		width_ = 0;
		height_ = 0;
		return false;
	}
	
	checkGL_ERROR();

	width_ = width;
	height_ = height;
	RenderTexture* prevRenderTarget = GLGlobals::getInstance()->getRenderTarget();
	GLGlobals::getInstance()->setRenderTarget(this);
	unsigned oldTextureCount = colorTextures_.size();
	bool oldDepthAttachment = depthTexture_ != nullptr?true:false;
	bool oldStencilAttachment = stencilTexture_ != nullptr?true:false;
	if(colorTextures_.size() > colorAttachments)
	{
		for(unsigned i=colorAttachments;i<colorTextures_.size();i++)
		{
			colorTextures_[i]->release();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, 0,0);
		}
		colorTextures_.resize(colorAttachments);
	}
	if(colorTextures_.size() < colorAttachments)
	{
		colorTextures_.resize(colorAttachments,nullptr);
		for(unsigned i=colorAttachments - 1;i < colorTextures_.size();i--)
			if(colorTextures_[i])
				break;
			else
			{
				colorTextures_[i] = new Texture2D;
				colorTextures_[i]->retain();			
			}
	}
	
	if(depthAttachment && !depthTexture_)
	{
		depthTexture_ = new Texture2D;
		depthTexture_->retain();		
	}
	
	if(!depthAttachment && depthTexture_)
	{
		depthTexture_->release();
		delete depthTexture_;
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0,0);
	}
	
	if(stencilAttachment && !stencilTexture_)
	{
		stencilTexture_ = new Texture2D;
		stencilTexture_->retain();		
	}
	
	if(!stencilAttachment && stencilTexture_)
	{
		stencilTexture_->release();
		delete stencilTexture_;
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, 0,0);
	}

	checkGL_ERROR();
	for(unsigned i=0;i<colorTextures_.size();i++)
		colorTextures_[i]->bufferData(GL_RGBA, width, height, nullptr);		
	checkGL_ERROR();
	if(depthTexture_)
		depthTexture_->bufferData(GL_DEPTH_COMPONENT, width, height, nullptr);
	checkGL_ERROR();
	if(stencilTexture_)
		stencilTexture_->bufferData(GL_STENCIL_INDEX, width, height, nullptr);	
	checkGL_ERROR();

	for(unsigned i=oldTextureCount;i<colorTextures_.size();i++)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorTextures_[i]->getTextureID(), 0);	
	checkGL_ERROR();
	if (!oldDepthAttachment && depthAttachment)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture_->getTextureID(), 0);	
	checkGL_ERROR();
	if (!oldStencilAttachment && stencilAttachment)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, stencilTexture_->getTextureID(), 0);	
	int completionStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);	
	
	
	checkGL_ERROR();
	assert(completionStatus == GL_FRAMEBUFFER_COMPLETE);
	GLGlobals::getInstance()->setRenderTarget(prevRenderTarget);	
	
	return true;
}
	
Texture2D* RenderTexture::getColorTexture(unsigned id)
{
	return id < colorTextures_.size()?colorTextures_[id]:nullptr;
}

Texture2D* RenderTexture::getDepthTexture()
{
	return depthTexture_;
}

Texture2D* RenderTexture::getStencilTexture()
{
	return stencilTexture_;
}

unsigned RenderTexture::getFrameBufferID()
{
	return frameBufferID_;
}