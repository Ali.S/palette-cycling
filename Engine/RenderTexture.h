#pragma once
#include "Texture2D.h"
#include <vector>
#include "Object.h"

class RenderTexture : public Object
{
public:
	RenderTexture();
	RenderTexture(unsigned width, unsigned height, int colorAttachments, bool depthAttachment, bool stencilAttachment);
	~RenderTexture();
	bool init(unsigned width, unsigned height, unsigned colorAttachments, bool depthAttachment, bool stencilAttachment);
	
	Texture2D* getColorTexture(unsigned id=0);
	Texture2D* getDepthTexture();
	Texture2D* getStencilTexture();
	unsigned getFrameBufferID();

private:
	SYNTHESIZE_READONLY(unsigned, width_, Width);
	SYNTHESIZE_READONLY(unsigned, height_, Height);
	unsigned frameBufferID_;
	std::vector<Texture2D*> colorTextures_;
	Texture2D *depthTexture_, *stencilTexture_  ;
};