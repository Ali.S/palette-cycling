#include "Visualizer.h"
#include <SDL.h>
#include "GarbageCollector.h"
#include <kazmath.h>
#include "GPURenderProgram.h"
#include "RenderTexture.h"

Visualizer::Visualizer(kmScalar scale, bool dumpSVG) : _scale(scale), _useDefaultShaders(true)
{
	unsigned outWidth, outHeight;
	
	if (GLGlobals::getInstance()->getRenderTarget() == nullptr)
	{
		outWidth = GLGlobals::getInstance()->getWinWidth();
		outHeight = GLGlobals::getInstance()->getWinHeight();
	}
	else
	{
		outWidth = GLGlobals::getInstance()->getRenderTarget()->getWidth();
		outHeight = GLGlobals::getInstance()->getRenderTarget()->getHeight();
	}

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glClearColor(1, 1, 1, 1.0f);
	glPointSize(4.0f);

	glGenBuffers(3, _glBuffers);
	
	_texture.push_back(new Texture2D());
	_texture.front()->retain();

	kmScalar rect[] = {
		0,                                  0, 0, 1, 0, 0,
		0,                 outHeight / _scale, 0, 1, 0, 1,
		outWidth / _scale,                  0, 0, 1, 1, 0,
		outWidth / _scale, outHeight / _scale, 0, 1, 1, 1
	};
	GLGlobals::getInstance()->setArrayBuffer(_rectBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);

	kmMat4OrthographicProjection(&_projection, 0, outWidth / scale, 0, outHeight / scale, -1, 1);

	kmMat4Identity(&_transform);

	if (dumpSVG)
	{
		_svgDoc.reset(new svg::Document(svg::Layout(
			svg::Dimensions(outWidth, outHeight),
			svg::Layout::BottomLeft, _scale
		)));
	}
}


Visualizer::~Visualizer()
{
	for (auto &t : _texture)
		t->release();
	glDeleteBuffers(3, _glBuffers);
}

void Visualizer::bindImage(const Image & image, int unit)
{
	while (unit >= _texture.size())
	{
		_texture.push_back(new Texture2D());
		_texture.back()->retain();
	}
	GLGlobals::getInstance()->bindTexture(_texture[unit], unit);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	switch (image.format())
	{
	case Image::Format::RGBA:_texture[unit]->bufferData(GL_RGBA, image.width(), image.height(), const_cast<Image&>(image).data()); break;
	case Image::Format::RGB:_texture[unit]->bufferData(GL_RGB, image.width(), image.height(), const_cast<Image&>(image).data()); break;
	case Image::Format::I:_texture[unit]->bufferData(GL_LUMINANCE, image.width(), image.height(), const_cast<Image&>(image).data()); break;
	}
}

void Visualizer::renderImage(const Image & image, int unit)
{
	bindImage(image, unit);
	renderTextureUnit(image.width(), image.height(), unit);
}

void Visualizer::renderTextureUnit(float width, float height, int textureUnit)
{
	kmScalar rect[] = {
		0,          0, 0, 1, 0, 0,
		0,     height, 0, 1, 0, 1,
		width,      0, 0, 1, 1, 0,
		width, height, 0, 1, 1, 1
	};
	GLGlobals::getInstance()->setArrayBuffer(_rectBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_DYNAMIC_DRAW);

	drawRect(textureUnit);
}

void Visualizer::renderTexture(float width, float height, Texture2D *texture, int textureUnit)
{
	kmScalar rect[] = {
		0,          0, 0, 1, 0, 0,
		0,     height, 0, 1, 0, 1,
		width,      0, 0, 1, 1, 0,
		width, height, 0, 1, 1, 1
	};
	GLGlobals::getInstance()->setArrayBuffer(_rectBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_DYNAMIC_DRAW);

	GLGlobals::getInstance()->bindTexture(texture, textureUnit);

	drawRect(textureUnit);
}

void Visualizer::useDefaultShaders(bool use)
{
	_useDefaultShaders = use;
}

void Visualizer::setTranslate(Vec2 off)
{
	kmMat4Translation(&_transform, off.x, off.y, 0);
	_translate = off;
}

void Visualizer::display(bool clear, bool clearBackBuffer)
{
	GLGlobals::getInstance()->swapBuffers();
	if (clear)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		if (_svgDoc)
		{
			_svgDoc.reset(new svg::Document(
				svg::Layout(
					svg::Dimensions(GLGlobals::getInstance()->getWinWidth(), GLGlobals::getInstance()->getWinHeight()),
					svg::Layout::BottomLeft, _scale
				)
			));
		}
		_arrayData.clear();
		if (clearBackBuffer)
		{
			GLGlobals::getInstance()->swapBuffers();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		}
	}
	GarbageCollector::sharedGarbageCollector()->cleanUp();
}

void Visualizer::display(const std::string &svgDumpPath, bool clear, bool clearBackBuffer)
{
	if (_svgDoc)
	{
		std::ofstream fileOut(svgDumpPath);
		fileOut << _svgDoc->toString();
	}
	display(clear, clearBackBuffer);
}

Image Visualizer::screenShot()
{
	unsigned width, height;
	if (GLGlobals::getInstance()->getRenderTarget() == nullptr)
	{
		width = GLGlobals::getInstance()->getWinWidth();
		height = GLGlobals::getInstance()->getWinHeight();
	}
	else
	{
		width = GLGlobals::getInstance()->getRenderTarget()->getWidth();
		height = GLGlobals::getInstance()->getRenderTarget()->getHeight();
	}
	Image img;
	img.alloc(width, height, Image::Format::RGBA);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glReadBuffer(GL_FRONT);
	glFinish();
	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, img.data());
	return img;
}

Vec2 Visualizer::project(Vec3 point) {
	unsigned width, height;
	if (GLGlobals::getInstance()->getRenderTarget() == nullptr)
	{
		width = GLGlobals::getInstance()->getWinWidth();
		height = GLGlobals::getInstance()->getWinHeight();
	}
	else
	{
		width = GLGlobals::getInstance()->getRenderTarget()->getWidth();
		height = GLGlobals::getInstance()->getRenderTarget()->getHeight();
	}
	Vec3 out;
	kmVec3Transform(&out, &point, &_transform);
	kmVec3Transform(&out, &out, &_projection);
	return Vec2((out.x + 1) * width / 2 , (out.y + 1) * height / 2);
}

Vec2 Visualizer::unproject(Vec2 point) {
	unsigned width, height;
	if (GLGlobals::getInstance()->getRenderTarget() == nullptr)
	{
		width = GLGlobals::getInstance()->getWinWidth();
		height = GLGlobals::getInstance()->getWinHeight();
	}
	else
	{
		width = GLGlobals::getInstance()->getRenderTarget()->getWidth();
		height = GLGlobals::getInstance()->getRenderTarget()->getHeight();
	}
	Vec3 out, in(point.x / width * 2 - 1, 1 - point.y / height * 2, 0);
	kmMat4 inv;
	kmMat4Multiply(&inv, &_projection, &_transform);
	kmMat4Inverse(&inv, &inv);
	kmVec3Transform(&out, &in, &inv);
	return Vec2(out.x, out.y);
}


void Visualizer::drawRect(int unit)
{
	if (_useDefaultShaders)
		GPUProgramCache::getInstance()->getRenderProgram(SHADERNAME_POSITION_TEXTURE)->use();

	auto shader = dynamic_cast<GPURenderProgram*>(GPURenderProgram::getInUseGPUProgram());
	if (shader)
	{
		shader->setUniform(shader->getPVLocation(), _projection);
		shader->setUniform(shader->getMLocation(), _transform);
		shader->setUniform(shader->getTextureLocation(), unit);
	}


	GLGlobals::getInstance()->bindTexture(_texture[0], GL_TEXTURE0 + unit);
	GLGlobals::getInstance()->setArrayBuffer(_rectBuffer);

	GLGlobals::getInstance()->resetVertexAttributeEnabled();
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_Position, true);
	GLGlobals::getInstance()->setVertexAttributeEnabled(VertexAttribute_TextureCoordinates, true);
	GLGlobals::getInstance()->applyVertexAttributeChanges();

	glVertexAttribPointer(VertexAttribute_Position, 4, getGLType<kmScalar>(), 0, sizeof(kmScalar) * 6, 0);
	glVertexAttribPointer(VertexAttribute_TextureCoordinates, 2, getGLType<kmScalar>(), 0, sizeof(kmScalar) * 6, reinterpret_cast<void*>(sizeof(kmScalar) * 4));

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
