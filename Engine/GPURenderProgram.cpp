#include "CommonHeaders.h"
#include "GPURenderProgram.h"
#include <cstdlib>
#include <iostream>

using namespace std;	

GPURenderProgram::GPURenderProgram()
{

}

GPURenderProgram::GPURenderProgram(const char* vertexShader, const char* fragmentShader)
{
	if (!compile(vertexShader, fragmentShader))
		throw std::exception("GPU render program failed to compile");
}

GPURenderProgram::~GPURenderProgram()
{
}

bool GPURenderProgram::compile(const char * vertexShader, const char * fragmentShader)
{
	int status = 0;
	status += attachShader(ShaderType::Vertex, vertexShader) ? 1 : 0;
	status += attachShader(ShaderType::Framgent, fragmentShader) ? 2 : 0;

	if (status != 3)
	{
		if ((status & 1) == 0)
			Log("Error Compiling Vertex Shader\n");
		if ((status & 2) == 0)
			Log("Error Compiling Fragment Shader\n");

	}
	else
	{
		glBindAttribLocation(_programID, VertexAttribute_Position, VertexAttribute_Position_Name);
		glBindAttribLocation(_programID, VertexAttribute_Normal, VertexAttribute_Normal_Name);
		glBindAttribLocation(_programID, VertexAttribute_Color, VertexAttribute_Color_Name);
		glBindAttribLocation(_programID, VertexAttribute_TextureCoordinates, VertexAttribute_TextureCoordinates_Name);
		status += linkProgram() ? 4 : 0;
	}
	if (status != 7)
		return false;
	use();
	_PVLocation = getUniformID(Uniform_ProjectionView_Name);
	_MLocation = getUniformID(Uniform_Model_Name);
	_TextureLocation = getUniformID(Uniform_Texture0_name);
	return true;
}
